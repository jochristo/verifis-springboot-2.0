package org.verifis.core.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.verifis.api.error.ApiErrorCode;

/**
 * Internal API utilities.
 * @author ic
 */
public class Internal
{
    
    /**
     * Gets a list of API error codes and their description.
     * @return 
     */
    public static List<ApplicationErrorCode> getApiErrorCodes()
    {
        List<ApplicationErrorCode> data = new ArrayList();
        ApiErrorCode[] apiErrorCodes = ApiErrorCode.values();
        List<ApiErrorCode> list = Arrays.asList(apiErrorCodes);
        if(list.isEmpty() == false){
            list.stream().forEachOrdered((item)->
            {
                ApplicationErrorCode code = new ApplicationErrorCode();
                code.setStatus(item.getHttpStatus().name());
                code.setValue(item.getHttpStatus().value());
                code.setCode(item.getAppCode());                
                code.setTitle(item.getTitle());                
                data.add(code);
            });
        }
        return data;
    }
    
    /**
     * Validate input against predefined semantic version format;
     * @param input
     * @return 
     */
    public static boolean isSemanticVersion(String input)
    {        
        final String format = "^(([1-9]+)\\.([0-9]+)(?:\\.)?([0-9]*)(\\.|-|\\+)?([0-9A-Za-z-.]*)?)$";
        final Pattern pattern = Pattern.compile(format);
        final Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }    
    
}
