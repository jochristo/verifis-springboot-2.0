package org.verifis.core.utility;

import java.util.UUID;

/**
 *
 * @author ic
 */
public class ApplicationErrorCode
{        
    private final String id = UUID.randomUUID().toString();    
    
    private String status;   
    
    private Integer value; 
    
    private String code;    
    
    private String title;    
    
    private String description;

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
