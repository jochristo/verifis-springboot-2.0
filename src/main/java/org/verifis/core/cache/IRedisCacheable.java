package org.verifis.core.cache;

/**
 *  Defines base methods for Serializable objects.
 * @author ic
 */
public interface IRedisCacheable
{
    public Object set(String key,Object object);        
        
    public Object get(String key);    
    
    public boolean isExpired(String key);    
    
    public boolean setExpired(String key);      
    
    // prefix key with given key prefix
    
    public Object set(String keyPrefix, String key, Object object);    
    
    public Object get(String keyPrefix, String key);                  
    
    public boolean isExpired(String keyPrefix, String key);    
    
    public boolean setExpired(String keyPrefix, String key);     
}
