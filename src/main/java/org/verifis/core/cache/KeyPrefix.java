package org.verifis.core.cache;

/**
 *
 * @author ic
 */
@Deprecated
public interface KeyPrefix
{
    public final static String NEW_VERIFIS = "new_vid:";
    
    public final static String VERIFIS_TOKEN = "verifis_token:";
    
    public final static String UNENCRYPTED_VERIFIS_TOKEN = "unencrypted_verifis_token:";
    
    public final static String ASSOCIATION_REQUEST_ID = "association_request_id:";
    
    public final static String ASSOCIATION_REQUEST_ID_EXISTENCE = "request_id:";
    
    public final static String TRANSFER_REQUEST_ID = "transfer_request_id:";
    
    public final static String TRANSFER_REQUEST_ID_EXISTENCE = "transfer_id:";
    
    // example --> logistics:building#23
    
}
