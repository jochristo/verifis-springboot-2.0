package org.verifis.core.cache.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.verifis.api.Constants;
import org.verifis.api.model.serializable.UnencryptedTokenItem;
import org.verifis.core.cache.AbstractRedisCacheable;
import org.verifis.api.Constants.Cache.KeyPrefix;

/**
 *
 * @author ic
 */
@Component
public class UnencryptedTokenItemCacheable extends AbstractRedisCacheable<UnencryptedTokenItem>
{
    private static final Logger logger = LoggerFactory.getLogger(UnencryptedTokenItemCacheable.class); 
    private final int _keyExpiry = Constants.Cache.TOKEN_EXPIRY_SECONDS;      
    private final int _existsKeyExpiry = Constants.Cache.TOKEN_EXPIRY_SECONDS;  
    private final String _keyPrefix = KeyPrefix.UNENCRYPTED_VERIFIS_TOKEN;

    public UnencryptedTokenItemCacheable() {        
        this.keyPrefix = _keyPrefix;
        this.prefixedKeyExpiry = _existsKeyExpiry;
        this.keyExpiry = _keyExpiry;
        logger.info("Initialized UnencryptedTokenItemCacheable component: {key prefix: " + keyPrefix + ", key expiry: "+keyExpiry + " , exists key expiry: "+ prefixedKeyExpiry + "}");
    }          

    
}
