package org.verifis.core.cache.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.verifis.api.Constants;
import org.verifis.api.Constants.Cache.KeyPrefix;
import org.verifis.api.model.serializable.TransferItem;
import org.verifis.core.cache.AbstractRedisCacheable;

/**
 *
 * @author ic
 */
@Component
public class TransferItemCacheable extends AbstractRedisCacheable<TransferItem>
{
    private static final Logger logger = LoggerFactory.getLogger(TransferItemCacheable.class);     
    private final int _keyExpiry = Constants.Cache.TRANSFER_EXPIRY_SECONDS;      
    private final int _existsKeyExpiry = Constants.Cache.TRANSFER_ID_EXISTS_EXPIRY_SECONDS;   
    private final String _keyPrefix = KeyPrefix.TRANSFER_REQUEST_ID;

    public TransferItemCacheable() {
        this.keyPrefix = _keyPrefix;
        this.prefixedKeyExpiry = _existsKeyExpiry;
        this.keyExpiry = _keyExpiry;
        logger.info("Initialized TransferItemCacheable component: {key prefix: " + keyPrefix + ", key expiry: "+keyExpiry + " , exists key expiry: "+ prefixedKeyExpiry + "}");      
    }
    
}
