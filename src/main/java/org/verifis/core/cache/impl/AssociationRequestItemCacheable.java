package org.verifis.core.cache.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.verifis.api.Constants;
import org.verifis.api.model.serializable.AssociationRequestItem;
import org.verifis.core.cache.AbstractRedisCacheable;
import org.verifis.api.Constants.Cache.KeyPrefix;


/**
 *
 * @author ic
 */
@Component
public class AssociationRequestItemCacheable extends AbstractRedisCacheable<AssociationRequestItem>
{
    private static final Logger logger = LoggerFactory.getLogger(AssociationRequestItemCacheable.class);     
    private final int _keyExpiry = Constants.Cache.ASSOCIATION_REQUEST_EXPIRY_SECONDS;      
    private final int _existsKeyExpiry = Constants.Cache.REQUEST_ID_EXIST_EXPIRY_SECONDS;   
    private final String _keyPrefix = KeyPrefix.ASSOCIATION_REQUEST_ID;

    public AssociationRequestItemCacheable() {
        this.keyPrefix = _keyPrefix;
        this.prefixedKeyExpiry = _existsKeyExpiry;
        this.keyExpiry = _keyExpiry;
        logger.info("Initialized AssociationRequestItemCacheable component: {key prefix: " + keyPrefix + ", key expiry: "+keyExpiry + " , exists key expiry: "+ prefixedKeyExpiry + "}");
    }    
}
