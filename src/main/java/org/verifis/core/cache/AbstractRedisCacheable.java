package org.verifis.core.cache;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.verifis.data.redis.service.IRedisService;

/**
 *
 * @author ic
 * @param <T>
 */
public abstract class AbstractRedisCacheable<T extends Serializable> implements IRedisCacheable
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractRedisCacheable.class);    
    private String _key = null;     
    protected int prefixedKeyExpiry;         
    protected int keyExpiry;    
    protected String keyPrefix = null;
    
    @Autowired
    protected IRedisService redis;    
   
    @Override
    public T set(String key, Object object) {
        _key = keyPrefix + key;
        logger.info("Adding redis key: " + _key);
        return (T) redis.added(_key, object, keyExpiry);
    }      

    @Override
    public T get(String key) {
        _key = keyPrefix + key;
        logger.info("Accessing redis key: " + _key);        
        return (T) redis.get(_key);
    }

    @Override
    public boolean isExpired(String key) {
        _key = keyPrefix + key;
        logger.info("Checking redis key existence: " + _key);          
        return !redis.hasKey(_key);
    }

    @Override
    public boolean setExpired(String key) {
        _key = keyPrefix + key;
        logger.info("Deleting redis key: " + _key);           
        redis.delete(_key);
        logger.info("Done.");     
        return isExpired(_key);    
    }

    @Override
    public Object set(String keyPrefix, String key, Object object) {
        _key = keyPrefix + key;
        logger.info("Adding redis key: " + _key);
        return (T) redis.added(_key, object, this.prefixedKeyExpiry);
    }

    
    @Override
    public Object get(String keyPrefix, String key) {
        _key = keyPrefix + key;
        logger.info("Accessing redis key: " + _key);        
        return redis.get(_key);
    }

    
    @Override
    public boolean isExpired(String keyPrefix, String key) {
        _key = keyPrefix + key;
        logger.info("Checking redis key existence: " + _key);          
        return !redis.hasKey(_key);
    }

    
    @Override
    public boolean setExpired(String keyPrefix, String key) {
        _key = keyPrefix + key;
        logger.info("Deleting redis key: " + _key);           
        redis.delete(_key);
        logger.info("Done.");     
        return isExpired(keyPrefix,key);
    }
    
}
