package org.verifis.core.threading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import static org.verifis.api.Constants.Cache.TOKEN_SCHEDULER_DELAY_MILLISECONDS;
import org.verifis.api.domain.Verifis;
import org.verifis.api.model.enumeration.ActivationStatus;
import org.verifis.api.service.IVerifisService;

/**
 *
 * @author ic
 */
@Component
public class VerifisTaskSchedulerImpl implements IVerifisTaskScheduler
{    
    private static final Logger logger = LoggerFactory.getLogger(VerifisTaskSchedulerImpl.class);      
    private String vid = null;
    
    @Autowired
    private IVerifisService verifisService;    
      
    @Override
    public void initialize(String vid)
    {
        logger.info("ScheduledVerifisTaskImpl running and setting vid... " + vid);
        this.vid = vid;
    }    
    
    @Scheduled(initialDelay = TOKEN_SCHEDULER_DELAY_MILLISECONDS, fixedDelay = TOKEN_SCHEDULER_DELAY_MILLISECONDS)
    public void terminateToken(){
        Verifis verifis = verifisService.find(this.vid);
        if(verifis != null){
            if(verifis.getStatus() == ActivationStatus.INACTIVE){
                logger.info("Verifis installation found active, terminating token...");                
                verifis.setExpired(true);
                verifisService.update(verifis.getId(), verifis);
                logger.info("Terminating token... done.");                
                this.vid = null;
            }
        }
    }    
    
}
