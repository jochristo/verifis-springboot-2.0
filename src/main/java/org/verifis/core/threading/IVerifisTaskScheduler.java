package org.verifis.core.threading;

/**
 * Defines scheduling task operations on Verifis tokens.
 * @author ic
 */
public interface IVerifisTaskScheduler
{
    public void initialize(String vid);    
    
}
