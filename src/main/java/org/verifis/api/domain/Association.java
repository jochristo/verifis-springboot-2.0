package org.verifis.api.domain;

import org.verifis.api.model.enumeration.AssociationStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.Date;
import java.util.UUID;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.util.DigestUtils;
import org.verifis.core.utility.Utilities;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "assocId",
    "service",
    "url",
    "logo",
    "description",
    "assocDate",
    "lastAuthDate",
    "status"
})
@Entity("association")
@Document(collection = "association")
@CompoundIndexes({@CompoundIndex(name = "vid_assocId_unique_index", unique = true, def ="{'vid' : 1, 'assocId': 1}")})
public class Association
{
    @Id    
    @JsonIgnore
    private String id;      
    
    @JsonProperty("assocId")    
    @Indexed(unique=true)
    private String assocId;
    
    @JsonProperty("service")
    @Field
    private String service;
    
    @JsonProperty("url")
    @Field
    private String url;
    
    @JsonProperty("logo")
    @Field
    private String logo;
    
    @JsonProperty("description")
    @Field
    private String description;    
    
    @JsonProperty("assocDate")
    @Field
    private Integer assocDate;
    
    @JsonProperty("lastAuthDate")
    @Field
    private Integer lastAuthDate;
    
    @JsonProperty("status")             
    @Field
    private AssociationStatus status;
    
    @JsonIgnore
    @Field
    private String vid;
    
    @Field
    @JsonIgnore
    private Date createdAt;
    
    @Field
    @JsonIgnore
    private Date updatedAt;    
        
    @Field
    @JsonIgnore
    private boolean deleted;    
    

    public Association()
    {
        this.assocId = DigestUtils.md5DigestAsHex(UUID.randomUUID().toString().getBytes());
        this.setAssocDate(Utilities.currentEpochPlus(0));
        this.setLastAuthDate(this.getAssocDate());
        this.createdAt = new Date();
        this.deleted = false;           
    }
      
    
    @JsonProperty("assocId")
    public String getAssocId() {
        return assocId;
    }

    @JsonProperty("assocId")
    public void setAssocId(String assocId) {
        this.assocId = assocId;
    }
    
    @JsonProperty("service")
    public String getService() {
        return service;
    }

    @JsonProperty("service")
    public void setService(String service) {
        this.service = service;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("logo")
    public String getLogo() {
        return logo;
    }

    @JsonProperty("logo")
    public void setLogo(String logo) {
        this.logo = logo;
    }

    @JsonProperty("assocDate")
    public Integer getAssocDate() {
        return assocDate;
    }

    @JsonProperty("assocDate")
    public void setAssocDate(Integer assocDate) {
        this.assocDate = assocDate;
    }

    @JsonProperty("lastAuthDate")
    public Integer getLastAuthDate() {
        return lastAuthDate;
    }

    @JsonProperty("lastAuthDate")
    public void setLastAuthDate(Integer lastAuthDate) {
        this.lastAuthDate = lastAuthDate;
    }

    @JsonProperty("status")
    public AssociationStatus getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(AssociationStatus status) {
        this.status = status;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    

}
