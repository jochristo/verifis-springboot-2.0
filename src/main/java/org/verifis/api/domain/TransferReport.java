package org.verifis.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.verifis.api.model.enumeration.OperationStatus;

/**
 *
 * @author ic
 */
@Entity("transferReport")
@Document(collection = "transferReport")
@CompoundIndexes({@CompoundIndex(name = "vid_transferId_unique_index",unique = true,def ="{'vid' : 1, 'transferId': 1}")})
public class TransferReport
{    
    @Id    
    @JsonIgnore
    private String id;      

    @Field(value="transferId")
    @Indexed(unique=true)
    private String transferId;    
    
    @Field        
    @JsonProperty("vid")
    private String vid;    
    
    @Field         
    @JsonProperty("recipientDeviceId")
    private String recipientDeviceId; 

    @Field         
    @JsonProperty("forwardingDeviceId")
    private String forwardingDeviceId;
    
    @Field         
    @JsonProperty("createdAt")
    private Date createdAt;
    
    @Field
    private OperationStatus status;   

    public TransferReport() {        
        this.status = OperationStatus.FAILURE; // default on creation
        this.createdAt = new Date();
    }
        
    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getRecipientDeviceId() {
        return recipientDeviceId;
    }

    public void setRecipientDeviceId(String recipientDeviceId) {
        this.recipientDeviceId = recipientDeviceId;
    }

    public String getForwardingDeviceId() {
        return forwardingDeviceId;
    }

    public void setForwardingDeviceId(String forwardingDeviceId) {
        this.forwardingDeviceId = forwardingDeviceId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public OperationStatus getStatus() {
        return status;
    }

    public void setStatus(OperationStatus status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
        
}
