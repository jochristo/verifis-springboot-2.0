package org.verifis.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import static org.apache.commons.codec.digest.DigestUtils.md5Hex;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.verifis.api.model.enumeration.ActivationStatus;
import org.verifis.api.model.enumeration.InstallationType;
import org.verifis.api.model.enumeration.MobilePlatform;

/**
 *
 * @author ic
 */
@Entity("verifis")
@Document(collection = "verifis")
@CompoundIndexes({@CompoundIndex(name = "vid_deviceId_unique_index",unique = true,def ="{'vid' : 1, 'deviceId': 1}")})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Verifis implements Serializable
{            
    @Id    
    @JsonIgnore    
    private String id;    
    
    @Field    
    @Indexed(unique=true)
    @JsonProperty("vid")
    private String vid;    
    
    @Field         
    @JsonProperty("deviceId")
    private String deviceId;    
    
    @Field     
    @JsonProperty("accessPin")
    private String accessPin;
    
    @Field
    @JsonProperty("name")
    private String name;    
    
    @Field
    @JsonProperty("description")
    private String description;         
    
    @Field 
    @JsonProperty("platform")
    private MobilePlatform platform;
    
    @Field 
    @JsonProperty("version")
    private String version;       
    
    @Field 
    @JsonProperty("status")
    private ActivationStatus status = ActivationStatus.INACTIVE; // default
    
    @Field
    @JsonProperty("isExpired")
    private boolean expired = false;
    
    @Field
    @JsonProperty("createdAt")
    private Date createdAt;    
    
    @Field 
    @JsonProperty("validUntil")
    private Integer validUntil;        
            
    @Field
    @JsonProperty("updatedAt")
    private Date updatedAt;
    
    @Field
    @JsonProperty("deleted")    
    private boolean deleted;
    
    @Field
    @JsonProperty("type")    
    private InstallationType type;
    

    public Verifis() {
        this.vid = md5Hex(UUID.randomUUID().toString());
        this.createdAt = new Date();
        this.deleted = false;
        this.type = InstallationType.NEW;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Integer validUntil) {
        this.validUntil = validUntil;
    }

    public String getAccessPin() {
        return accessPin;
    }

    public void setAccessPin(String accessPin) {
        this.accessPin = accessPin;
    }

    public MobilePlatform getPlatform() {
        return platform;
    }

    public void setPlatform(MobilePlatform platform) {
        this.platform = platform;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }    

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }        

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.vid);
        hash = 67 * hash + Objects.hashCode(this.deviceId);
        hash = 67 * hash + Objects.hashCode(this.accessPin);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Verifis other = (Verifis) obj;
        if (!Objects.equals(this.vid, other.vid)) {
            return false;
        }
        return Objects.equals(this.deviceId, other.deviceId);
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public ActivationStatus getStatus() {
        return status;
    }

    public void setStatus(ActivationStatus status) {
        this.status = status;
    }

    public InstallationType getType() {
        return type;
    }

    public void setType(InstallationType type) {
        this.type = type;
    }






    
    
    
    
}
