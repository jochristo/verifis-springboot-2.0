package org.verifis.api.domain;

import java.util.Date;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ic
 */
@Entity("transaction")
@Document(collection = "transaction")
public class Transaction
{
    
    private ObjectId id;
    
    private String collectionName;
    
    private String description;
    
    private final Date createdAt;

    public Transaction()
    {
        this.createdAt = new Date();
    }
    
    
    
    
}
