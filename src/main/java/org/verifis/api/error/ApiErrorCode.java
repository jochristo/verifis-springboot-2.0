package org.verifis.api.error;

import org.springframework.http.HttpStatus;

/**
 * Defines specific API error codes and their description.
 * @author ic
 */
public enum ApiErrorCode
{
    
    // GENERAL API ERROR CODES
    //INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "500", "INTERNAL_SERVER_ERROR"),
    
    //CONSTRAINT_VIOLATION(HttpStatus.UNPROCESSABLE_ENTITY, "1000", "CONSTRAINT_VIOLATION"),
    
    //DATA_INTEGRITY_VIOLATION(HttpStatus.UNPROCESSABLE_ENTITY, "1001", "DATA_INTEGRITY_VIOLATION"),

    //RESOURCE_NOT_FOUND(HttpStatus.NOT_FOUND, "404", "RESOURCE_NOT_FOUND"),
    
    MISSING_REQUEST_PARAMETER(HttpStatus.BAD_REQUEST, "2001", "MISSING_REQUEST_PARAMETER"),
    
    //INVALID_PARAMETER_VALUE(HttpStatus.UNPROCESSABLE_ENTITY, "2002", "INVALID_PARAMETER_VALUE"),
    
    //UNAUTHORIZED(HttpStatus.UNAUTHORIZED,"401", "UNAUTHORIZED"),

    //INVALID_LOGIN_CREDENTIALS(HttpStatus.UNAUTHORIZED,"401", "INVALID_LOGIN_CREDENTIALS"),
    
    
    INVALID_FORMAT(HttpStatus.BAD_REQUEST, "400", "INVALID_FORMAT"), // Specific error occured causing field validation failure
    
    SERVICE_UNAVAILABLE(HttpStatus.SERVICE_UNAVAILABLE, "503", "SERVICE_UNAVAILABLE"),
    
    BAD_REQUEST(HttpStatus.BAD_REQUEST, "400", "BAD_REQUEST"),
    
    
    // application-specific errors
    DEVICE_ID_MISMATCH(HttpStatus.FORBIDDEN, "403", "DEVICE_ID_MISMATCH"), //the value of deviceId not matches active installation's deviceId.
    
    VERIFIS_ID_NOT_FOUND(HttpStatus.NOT_FOUND, "404", "VERIFIS_ID_NOT_FOUND"),
    
    VERIFIS_ID_NOT_ACTIVE(HttpStatus.LOCKED, "423", "VERIFIS_ID_NOT_ACTIVE"),
    
    INVALID_TRANSFER_PIN(HttpStatus.UNAUTHORIZED, "401", "INVALID_TRANSFER_PIN"),
    
    TOKEN_UNAVAILABLE(HttpStatus.NOT_FOUND, "404", "TOKEN_UNAVAILABLE"),
    
    INVALID_TOKEN(HttpStatus.UNPROCESSABLE_ENTITY, "422", "INVALID_TOKEN"),
    
    TRANSFER_ID_NOT_FOUND(HttpStatus.NOT_FOUND, "404", "TRANSFER_ID_NOT_FOUND"),
    
    EXPIRED_TRANSFER_ID(HttpStatus.GONE, "410", "EXPIRED_TRANSFER_ID"),
    
    INVALID_VERIFIS_CODE(HttpStatus.UNAUTHORIZED, "401", "INVALID_VERIFIS_CODE"),

    INVALID_INSTALLATION(HttpStatus.NOT_FOUND, "404", "INVALID_INSTALLATION"), // INVALID PAIR OF VID & DEVICE ID
    
    FINAL_REPORT_NOT_SUBMITTED(HttpStatus.NOT_FOUND, "404", "FINAL_REPORT_NOT_SUBMITTED"),    
    
    REQUEST_ID_NOT_FOUND(HttpStatus.NOT_FOUND, "404", "REQUEST_ID_NOT_FOUND"),
    
    EXPIRED_ASSOCIATION_REQUEST_ID(HttpStatus.GONE, "410", "EXPIRED_ASSOCIATION_REQUEST_ID"),
    
    ASSOCIATION_ID_VERIFIS_ID_MISMATCH(HttpStatus.NOT_FOUND, "404", "ASSOCIATION_ID_VERIFIS_ID_MISMATCH"), // ASSOCIATION ID NOT FOUND OR ASSOCIATION/VID MISMATCH
    
    INVALID_ACCESS_TOKEN(HttpStatus.UNAUTHORIZED, "401", "INVALID_ACCESS_TOKEN"),
    
    ACCESS_TOKEN_MISMATCH(HttpStatus.BAD_REQUEST, "400", "ACCESS_TOKEN_MISMATCH"),
    
    ;    
    
    private final HttpStatus httpStatus;
    private final String appCode;
    private final String title;    
    
    private final int statusCode;
    public int getStatusCode()
    {
        return this.httpStatus.value();
    }
    
    public String getStatusAsString()
    {
        return String.valueOf(this.httpStatus.value());
    }    

    private ApiErrorCode(HttpStatus httpStatus, String appCode, String title)
    {
        this.httpStatus = httpStatus;
        this.appCode = appCode;
        this.title = title;
        this.statusCode = httpStatus.value();
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getAppCode() {
        return appCode;
    }

    public String getTitle() {
        return title;
    }    
    
}
