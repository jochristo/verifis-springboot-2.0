package org.verifis.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.verifis.api.annotation.VerifisCodeAttribute;
import org.verifis.api.exception.InvalidVerifisCodeFormatException;

/**
 *
 * @author ic
 */
public class VerifisCodeValidator implements ConstraintValidator<VerifisCodeAttribute, String>
{   
    protected String pattern = null;
    protected String message = null;
    
    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {        
        if(t != null){
            if(!t.matches(this.pattern)){
            throw new InvalidVerifisCodeFormatException(message);
        }
        }
    
        return true;
    }

    @Override
    public void initialize(VerifisCodeAttribute a) {
        this.message = a.message();
        this.pattern = a.pattern();
    }
    
}
