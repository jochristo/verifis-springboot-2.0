package org.verifis.api.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.verifis.api.model.enumeration.MobilePlatform;

/**
 *
 * @author ic
 */
public class MobilePlatformValidator implements Validator
{

    @Override
    public boolean supports(Class<?> type) {
        return MobilePlatform.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        if(o == null){
            errors.reject("reject empty");
        }
    }
    
}
