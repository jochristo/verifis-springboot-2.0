package org.verifis.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.verifis.api.annotation.EnumAttribute;
import org.verifis.api.model.enumeration.MobilePlatform;

/**
 *
 * @author ic
 */
public class EnumerationValidator implements ConstraintValidator<EnumAttribute, MobilePlatform>
{
    @Override
    public void initialize(EnumAttribute a) {
    
    }

    @Override
    public boolean isValid(MobilePlatform t, ConstraintValidatorContext cvc)
    {        
        if(t == null){
            return false;
        }
        if(t == MobilePlatform.NONE){
            return false;
        }
        return true;
    }





    
}
