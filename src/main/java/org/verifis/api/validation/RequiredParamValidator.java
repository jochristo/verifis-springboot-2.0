package org.verifis.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.verifis.api.annotation.RequiredParam;

/**
 *
 * @author ic
 */
public class RequiredParamValidator implements ConstraintValidator<RequiredParam, Object>
{
    
    @Override
    public void initialize(RequiredParam a) {
        //throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public boolean isValid(Object t, ConstraintValidatorContext cvc) {
        return t != null;
    }
        
}
