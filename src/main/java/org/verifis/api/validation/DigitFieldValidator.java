package org.verifis.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.verifis.api.model.enumeration.DigitFieldType;
import org.verifis.api.annotation.DigitAttribute;
import org.verifis.api.exception.BadFormatException;

/**
 *
 * @author ic
 */
public class DigitFieldValidator implements ConstraintValidator<DigitAttribute, String>
{
    protected String pattern = null;
    protected String message = null;
    protected DigitFieldType type = null;
    
    @Override
    public void initialize(DigitAttribute a) {
        this.message = a.message();
        this.pattern = a.pattern();
        this.type = a.type();
    }

    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {
        if(t != null){
            if(!t.matches(this.pattern))
            {   
                throw new BadFormatException(this.type, message);    
            }
        }    
        return true;
    }
    
    
}
