package org.verifis.api.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.verifis.api.Constants;
import org.verifis.api.exception.InvalidVerifisCodeException;
import org.verifis.api.model.bind.ActiveTransferParameter;

/**
 *
 * @author ic
 */
public class ActiveTransferParameterValidator implements Validator
{  
    protected String pattern = "^\\d{6}$";
    
    @Override
    public boolean supports(Class<?> type) {
        return ActiveTransferParameter.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ActiveTransferParameter parameter = (ActiveTransferParameter) o;        
        String code = parameter.getCode();
        if(code != null){
            if(!code.matches(pattern)){
                throw new InvalidVerifisCodeException(Constants.FieldValidationMessage.INVALID_VERIFIS_CODE);
            }
        }
    }
    
}
