package org.verifis.api.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import org.verifis.api.model.enumeration.DigitFieldType;
import org.verifis.api.validation.DigitFieldValidator;

@Documented
@Constraint(validatedBy = DigitFieldValidator.class)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DigitAttribute
{
    String message() default "BAD FORMAT";
    
    String pattern() default "^\\d{4,}$";       
    
    DigitFieldType type() default DigitFieldType.ACCESS_TOKEN;
     
    Class<?>[] groups() default {};
     
    Class<? extends Payload>[] payload() default {};      
    
}
