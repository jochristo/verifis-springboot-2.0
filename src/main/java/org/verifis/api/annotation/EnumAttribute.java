package org.verifis.api.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import org.verifis.api.validation.EnumerationValidator;

@Documented
@Constraint(validatedBy = EnumerationValidator.class)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface EnumAttribute {
    
    Class<? extends Enum<?>>enumClazz();

    String message() default "Field value is not valid";

    Class<?>[]groups() default {};

    Class<? extends Payload>[]payload() default {};    
    
}
