package org.verifis.api.annotation;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import org.verifis.api.validation.RequiredParamValidator;

/**
 *
 * @author ic
 */
@Documented
@Constraint(validatedBy = RequiredParamValidator.class)
@Retention(RUNTIME)
@Target({FIELD, PARAMETER})
public @interface RequiredParam {
    String message() default "{RequiredParam.message}";
    String field() default "{RequiredParam.field}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};    
}
