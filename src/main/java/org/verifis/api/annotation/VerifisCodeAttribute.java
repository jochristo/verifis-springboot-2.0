package org.verifis.api.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import org.verifis.api.Constants;
import org.verifis.api.validation.VerifisCodeValidator;

@Documented
@Constraint(validatedBy = VerifisCodeValidator.class)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Deprecated
public @interface VerifisCodeAttribute {
    
    String message() default Constants.FieldValidationMessage.INVALID_VERIFIS_CODE;
    
    String pattern() default Constants.FieldPattern.VERIFIS_CODE;
     
    Class<?>[] groups() default {};
     
    Class<? extends Payload>[] payload() default {};    
}
