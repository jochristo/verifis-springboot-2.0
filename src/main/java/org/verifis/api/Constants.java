package org.verifis.api;

import org.springframework.http.MediaType;

/**
 * Defines API constants.
 * @author ic
 */
public interface Constants
{
    public final static String APPLICATION_JSON_UTF8_VALUE = MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8";    
    
    public final static MediaType APPLICATION_JSON_UTF8 = MediaType.valueOf(APPLICATION_JSON_UTF8_VALUE);     
    
    /**
     * Defines static field validation patterns.
     */
    public interface FieldPattern
    {
        public final static String PLATFORM = "^((ios)|(android))$";
        
        public final static String VERSION = "^(?<=^[Vv]|^)(?:(?<major>(?:0|[1-9](?:(?:0|[1-9])+)*))[.](?<minor>(?:0|[1-9](?:(?:0|[1-9])+)*))[.](?<patch>(?:0|[1-9](?:(?:0|[1-9])+)*))(?:-(?<prerelease>(?:(?:(?:[A-Za-z]|-)(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)?|(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)(?:[A-Za-z]|-)(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)?)|(?:0|[1-9](?:(?:0|[1-9])+)*))(?:[.](?:(?:(?:[A-Za-z]|-)(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)?|(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)(?:[A-Za-z]|-)(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)?)|(?:0|[1-9](?:(?:0|[1-9])+)*)))*))?(?:[+](?<build>(?:(?:(?:[A-Za-z]|-)(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)?|(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)(?:[A-Za-z]|-)(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)?)|(?:(?:0|[1-9])+))(?:[.](?:(?:(?:[A-Za-z]|-)(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)?|(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)(?:[A-Za-z]|-)(?:(?:(?:0|[1-9])|(?:[A-Za-z]|-))+)?)|(?:(?:0|[1-9])+)))*))?)$";
        
        public final static String VERIFIS_CODE = "^\\d{6}$";
        
        public final static String PIN = "^[1-9]\\d{3}$";        
        
    }
    
    /**
     * Defines static field validation error messages.
     */
    public interface FieldValidationMessage
    {
        public final static String INVALID_PLATFORM = "INVALID_FORMAT: The format of 'platform' parameter is invalid. Valid values are: ios or android.";
        
        public final static String INVALID_VERSION = "INVALID_FORMAT: The format of 'ver' parameter is invalid. Please use semantic versioning format.";                
        
        public final static String INVALID_TRANSFER_PIN = "INVALID_FORMAT: The format of 'transferPin' parameter is invalid. Please use 4-digit code.";            
        
        public final static String INVALID_VERIFIS_CODE = "INVALID_FORMAT: The format of 'code' parameter is invalid. Please use 6-digit code.";            
        
        public final static String INVALID_ACCESS_PIN = "INVALID_FORMAT: The format of 'accessToken' parameter is invalid. Allowed rangeis 1000-9999";            
    }    
    
    public interface DataViolationMessage
    {
        public final static String DEVICE_ID_MISMATCH = "Verifis ID found and active but the deviceId does not match";
        
        public final static String INVALID_VERSION = "INVALID_FORMAT: The format of 'ver' parameter is invalid. Please use semantic versioning format.";            
    }
    
    public interface RequiredFieldMessage
    {
        public final static String VERIFIS_CODE_REQUIRED = "code is required";
        
        public final static String DEVICE_ID_REQUIRED = "deviceId is required";
        
        public final static String VERIFIS_ID_REQUIRED = "vid is required";
        
        public final static String MOBILE_PLATFORM_REQUIRED = "platform is required";
        
        public final static String CLIENT_VERSION_REQUIRED = "ver is required";
        
        public final static String TRANSFER_PIN_REQUIRED = "transferPin is required";
        
        public final static String ACCESS_TOKEN_REQUIRED = "accessToken is required";
        
        public final static String TRANSFER_ID_REQUIRED = "transferId is required";
        
        public final static String REQUEST_ID_REQUIRED = "requestId is required";
        
        public final static String TOKEN_FILE_REQUIRED = "token file is required";
    }
    
    public interface Cache
    {
        public final static int TOKEN_EXPIRY_SECONDS = 90;
        
        public final static long TOKEN_SCHEDULER_DELAY_MILLISECONDS = 90000;
        
        public final static int ASSOCIATION_REQUEST_EXPIRY_SECONDS = 30; //86400000;
        
        public final static int REQUEST_ID_EXIST_EXPIRY_SECONDS = 60; //86400000;
        
        public final static int TRANSFER_EXPIRY_SECONDS = 120; // store store transfer item to cache , its expired info, etc.
        
        public final static int TRANSFER_ID_EXISTS_EXPIRY_SECONDS = 180; // store transfer id to cache to check its existence
        
        
        
        public interface KeyPrefix
        {
            public final static String NEW_VERIFIS = "new_vid:";

            public final static String VERIFIS_TOKEN = "verifis_token:";

            public final static String UNENCRYPTED_VERIFIS_TOKEN = "verifis:token:id#";

            public final static String ASSOCIATION_REQUEST_ID = "association:request:id#";

            public final static String ASSOCIATION_REQUEST_ID_EXISTENCE = "association:request:exists:id#";

            public final static String TRANSFER_REQUEST_ID = "transfer:request:id#";

            public final static String TRANSFER_REQUEST_ID_EXISTENCE = "transfer:request:exists:id#";

            /*
            EXAMPLES:
                1) verifis:token:id#5a8c1ad0b61c02477ee542a2008105a5
                2) association:request:exists:id#e68ee68838cc5869f37d28e2532e159a
                3) association:request:id#e68ee68838cc5869f37d28e2532e159a
                4) transfer:request:exists:id#eedc2486f35122d2d6be624ce9673069
                5) transfer:request:exists:id#bc45c22abeaeab14d1241fa1686d7226
            */

        }        
    }    
    
}
