package org.verifis.api.exception;

import org.verifis.api.model.serializable.TransferObject;
import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class TransferIdExpiredException extends RestApiException
{
    protected String transferId;
    protected Integer expires;
    private TransferObject transferral;
    public TransferIdExpiredException(String details) {
        super(ApiErrorCode.EXPIRED_TRANSFER_ID, details);
    }
    
    public TransferIdExpiredException(String details, String tranferId) {
        super(ApiErrorCode.EXPIRED_TRANSFER_ID, details);
        this.transferId = transferId;
        this.expires = 0;
        this.transferral = new TransferObject();
        transferral.setTransferId(this.transferId);
        transferral.setExpires(0);
    }    

    public String getTransferId() {
        return transferId;
    }

    public Integer getExpires() {
        return expires;
    }

    public TransferObject getTransferral() {
        return transferral;
    }
    
    
    
}
