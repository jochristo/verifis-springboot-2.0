package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class InvalidTransferPinException extends RestApiException
{
    
    public InvalidTransferPinException(String details) {
        super(ApiErrorCode.INVALID_TRANSFER_PIN,details);
    }
    
}
