package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;
import org.verifis.api.model.enumeration.DigitFieldType;

/**
 *
 * @author ic
 */
public class BadFormatException extends RestApiException
{
    private final String parameter;
    private final String error;
    private final DigitFieldType fieldType;    

    public BadFormatException(DigitFieldType fieldType, String details) {
        super(ApiErrorCode.INVALID_FORMAT,details);
        this.error = details; 
        this.fieldType = fieldType;
        this.parameter = this.fieldType.getDescription();
    }    
    
    public String getParameter() {
        return parameter;
    }

    public String getError() {
        return error;
    }

    public DigitFieldType getFieldType() {
        return fieldType;
    }
}
