package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class VerifisDeviceMismatchException extends RestApiException
{    
    public VerifisDeviceMismatchException(String details) {
        super(ApiErrorCode.DEVICE_ID_MISMATCH, details);
    }
    
}
