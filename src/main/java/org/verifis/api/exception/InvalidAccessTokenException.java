package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class InvalidAccessTokenException extends RestApiException
{
    
    public InvalidAccessTokenException(String details) {
        super(ApiErrorCode.INVALID_ACCESS_TOKEN,details);
    }
    
}
