package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class VerifisIdNotActiveException extends RestApiException
{
    private final String parameter;
    
    public VerifisIdNotActiveException(String details) {
        super(ApiErrorCode.VERIFIS_ID_NOT_ACTIVE,details);
        this.parameter = "vid";
    }

    public String getParameter() {
        return parameter;
    }
    
    
    
}
