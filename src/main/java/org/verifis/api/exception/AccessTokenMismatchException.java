package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class AccessTokenMismatchException extends RestApiException
{
    private String parameter = "accessToken";
    private String error;
    
    public AccessTokenMismatchException(String details) {
        super(ApiErrorCode.ACCESS_TOKEN_MISMATCH,details);
        this.error = details;
    }

    public String getParameter() {
        return parameter;
    }

    public String getError() {
        return error;
    }
    
    
    
}
