package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;
import org.verifis.api.model.enumeration.DigitFieldType;

/**
 * 
 * @author ic
 */
@Deprecated
public class InvalidPinFormatException extends RestApiException
{
    private String parameter;
    private String error;
    private DigitFieldType pinType;
    
    /*
    public InvalidPinFormatException(String details) {
        super(ApiErrorCode.INVALID_FORMAT,details);
        this.error = details;        
    }
    */

    public InvalidPinFormatException(DigitFieldType pinType, String details) {
        super(ApiErrorCode.INVALID_FORMAT,details);
        this.error = details; 
        this.pinType = pinType;
        this.parameter = this.pinType.getDescription();
    }    
    
    public String getParameter() {
        return parameter;
    }

    public String getError() {
        return error;
    }

    public DigitFieldType getPinType() {
        return pinType;
    }
    
    
}
