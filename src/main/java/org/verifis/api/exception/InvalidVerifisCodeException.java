package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class InvalidVerifisCodeException extends RestApiException
{
    
    public InvalidVerifisCodeException(String details) {
        super(ApiErrorCode.INVALID_VERIFIS_CODE,details);
    }
    
    
}
