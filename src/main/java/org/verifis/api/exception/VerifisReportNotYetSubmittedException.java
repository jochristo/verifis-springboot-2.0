package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class VerifisReportNotYetSubmittedException extends RestApiException
{
    
    public VerifisReportNotYetSubmittedException(String details) {
        super(ApiErrorCode.FINAL_REPORT_NOT_SUBMITTED,details);
    }
    
}
