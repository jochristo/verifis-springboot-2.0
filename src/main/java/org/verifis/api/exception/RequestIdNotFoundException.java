package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class RequestIdNotFoundException extends RestApiException
{    
    public RequestIdNotFoundException(String details) {
        super(ApiErrorCode.REQUEST_ID_NOT_FOUND,details);
    }
    
}
