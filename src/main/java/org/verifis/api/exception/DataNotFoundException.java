package org.verifis.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author Admin
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class DataNotFoundException extends RestApiException
{
    
    public DataNotFoundException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
    }

    
}
