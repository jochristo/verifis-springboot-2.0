package org.verifis.api.exception;

import org.springframework.http.HttpStatus;
import org.verifis.api.error.ApiErrorCode;

/**
 * Base API Exception class.
 * @author ic
 */
public class RestApiException extends RuntimeException
{
    private static final long serialVersionUID = -7498621923421702089L;
    protected String appCode;
    protected String details;
    protected HttpStatus httpStatus;    
    protected String title;  
    protected ApiErrorCode apiErrorCode;    

    public RestApiException(ApiErrorCode apiErrorCode, String details )
    {
        super(details);
        this.apiErrorCode = apiErrorCode;
        this.httpStatus = apiErrorCode.getHttpStatus();
        this.appCode = apiErrorCode.getAppCode();
        this.details = details;  
        this.title = apiErrorCode.getTitle();
    }

    public RestApiException(String details) {
        this.details = details;
    }   
    
    public String getDetails() {
        return details;
    }

    public String getTitle() {
        return title;
    }

    public String getAppCode() {
        return appCode;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public ApiErrorCode getApiErrorCode() {
        return apiErrorCode;
    }
    
    
    
    
}
