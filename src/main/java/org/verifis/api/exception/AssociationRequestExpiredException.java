package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;
import org.verifis.api.model.serializable.AssociationRequestObject;

/**
 *
 * @author ic
 */
public class AssociationRequestExpiredException extends RestApiException
{
    private AssociationRequestObject object;
    
    public AssociationRequestExpiredException(String details) {
        super(ApiErrorCode.EXPIRED_ASSOCIATION_REQUEST_ID,details);
    }

    public AssociationRequestExpiredException(AssociationRequestObject object, String details) {
        super(details);
        this.object = object;
    }

    public AssociationRequestObject getObject() {
        return object;
    }
    
    
    
}
