package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class TokenUnavailableException extends RestApiException
{
    
    public TokenUnavailableException(String details) {
        super(ApiErrorCode.TOKEN_UNAVAILABLE,details);
    }
    
}
