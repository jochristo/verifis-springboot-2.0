package org.verifis.api.exception;

import java.util.List;
import org.verifis.api.model.ErrorResponse;

/**
 *
 * @author ic
 */
public class FieldConstraintValidationException extends RuntimeException
{
    private final List<ErrorResponse> errors;    
        
    public FieldConstraintValidationException(List<ErrorResponse> errors) {
        
        super("Field(s) contain invalid or no data. Found: " + errors.size());
        this.errors = errors;
    }    
    
    public List<ErrorResponse> getErrors() {
        return errors;
    }
    
    
    
}
