package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
@Deprecated
public class InvalidVerifisCodeFormatException extends RestApiException
{
    private String parameter = "code";
    private String error;
    
    public InvalidVerifisCodeFormatException(String details) {
        super(ApiErrorCode.INVALID_FORMAT,details);
        this.error = details;
    }

    public String getParameter() {
        return parameter;
    }

    public String getError() {
        return error;
    }
    
    
}
