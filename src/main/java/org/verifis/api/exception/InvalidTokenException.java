package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class InvalidTokenException extends RestApiException
{
    
    public InvalidTokenException(String details) {
        super(ApiErrorCode.INVALID_TOKEN,details);
    }
    
}
