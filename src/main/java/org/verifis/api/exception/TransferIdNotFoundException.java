package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class TransferIdNotFoundException extends RestApiException
{    
    public TransferIdNotFoundException(String details) {
        super(ApiErrorCode.TRANSFER_ID_NOT_FOUND, details);
    }
}
