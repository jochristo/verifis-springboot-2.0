package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class AssociationVerifisMismatchException extends RestApiException
{
    
    public AssociationVerifisMismatchException(String details) {
        super(ApiErrorCode.ASSOCIATION_ID_VERIFIS_ID_MISMATCH,details);
    }
    
}
