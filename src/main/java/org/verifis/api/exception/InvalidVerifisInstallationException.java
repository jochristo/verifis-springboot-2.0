package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class InvalidVerifisInstallationException extends RestApiException
{
    
    public InvalidVerifisInstallationException(String details) {
        super(ApiErrorCode.INVALID_INSTALLATION,details);
    }
    
}
