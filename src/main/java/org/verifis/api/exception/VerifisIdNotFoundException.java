package org.verifis.api.exception;

import org.verifis.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class VerifisIdNotFoundException extends RestApiException
{    
    public VerifisIdNotFoundException(String details) {
        super(ApiErrorCode.VERIFIS_ID_NOT_FOUND, details);
    }
}
