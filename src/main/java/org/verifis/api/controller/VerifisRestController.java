package org.verifis.api.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.verifis.api.Constants;
import org.verifis.api.EndPoints;
import org.verifis.api.domain.Association;
import org.verifis.api.model.serializable.AssociationRequestObject;
import org.verifis.api.model.Associations;
import org.verifis.api.model.bind.BootRequestParameter;
import org.verifis.api.model.BootResponse;
import org.verifis.api.model.serializable.TransferObject;
import org.verifis.api.model.TokenReport;
import org.verifis.api.model.serializable.VerifisToken;
import org.verifis.api.model.bind.ActiveTransferParameter;
import org.verifis.api.model.bind.AssociationRequestParameter;
import org.verifis.api.model.bind.DeviceIdParameter;
import org.verifis.api.model.bind.TransferIdParameter;
import org.verifis.api.model.bind.TransferPinParameter;
import org.verifis.api.model.bind.TransferSessionParameter;
import org.verifis.api.model.bind.VerifisCodeParameter;
import org.verifis.api.model.bind.VerifisTokenParameter;
import org.verifis.api.model.enumeration.MobilePlatform;
import org.verifis.api.model.bind.converter.MobilePlatformPropertyBindingConverter;
import org.verifis.api.handler.IFieldValidationHandler;
import org.verifis.api.service.IAssociationService;
import org.verifis.api.service.IVerifisService;
import org.verifis.api.service.ITransferService;

/**
 * Main Verifis app controller.
 * @author ic
 */
@RestController
public class VerifisRestController
{    
    @Autowired IFieldValidationHandler fieldValidation;           
    @Autowired IVerifisService verifisService;        
    @Autowired ITransferService transferralService;        
    @Autowired IAssociationService associationService;    
      
    @InitBinder
    protected void initBinder(final WebDataBinder webdataBinder) {
        webdataBinder.registerCustomEditor(MobilePlatform.class, new MobilePlatformPropertyBindingConverter());
    }    
    
    /**
     * Endpoint to check current Verifis ID is active on the server and Verifis application update is mandatory prior to engaging with the application flow.
     * @param request boot request parameter object
     * @param result the binding result
     * @return {@link BootResponse}
     */
    @RequestMapping(value = EndPoints.BOOT, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody
    public BootResponse boot(@ModelAttribute @Valid BootRequestParameter request, BindingResult result)
    {     
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        return verifisService.initializeVerifis(request);
    }
    

    
    /**
     * Endpoint to verify if a transfer id is valid and references an active transfer.
     * @param request indicates an active transfer
     * @param result
     * @return {@link TransferObject}
     */
    //@RequestMapping(value = EndPoints.TRANSFER_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody        
    public TransferObject verifyTrasferStatus(@ModelAttribute @Valid TransferIdParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        return transferralService.verify(request.getTransferId());
    } 
    
    /**
     * Endpoint to extend the expiration date of an active transfer.
     * @param transferId
     * @param request
     * @param result
     * @return {@link TransferObject}
     */
    //@RequestMapping(value = EndPoints.TRANSFER_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.PUT)
    @ResponseBody    
    public TransferObject extendActiveTrasfer(@PathVariable String transferId, @ModelAttribute @Valid ActiveTransferParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        return new TransferObject();
    }     
    
    /**
     * Endpoint to delete an active transfer.
     * @param transferId
     * @param request
     * @param result
     * @return {@link TransferObject}
     */
    //@RequestMapping(value = EndPoints.TRANSFER_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.DELETE)
    @ResponseBody      
    public ResponseEntity deleteActiveTrasfer(@PathVariable String transferId, @ModelAttribute @Valid ActiveTransferParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        return ResponseEntity.ok(null);
    } 

    /**
     * Endpoint to verify Verifis Installation.
     * @param deviceId Device's Unique Identifier to which Verifis is installed
     * @param vid the Verifis ID
     * @return {@link ResponseEntity}
     */
    //@RequestMapping(value = EndPoints.TOKEN_BY_VID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody     
    public ResponseEntity verifyVerifisInstall(@RequestParam String deviceId, @PathVariable String vid)
    {
        return ResponseEntity.ok(null);
    }
    
    /**
     * Endpoint to reset Verifis Installation.
     * @param vid the Verifis ID
     * @param request
     * @param result
     * @return {@link ResponseEntity}
     */
    //@RequestMapping(value = EndPoints.TOKEN_BY_VID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.DELETE)
    @ResponseBody     
    public ResponseEntity resetVerifisInstall(@PathVariable String vid, @Valid @ModelAttribute ActiveTransferParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }         
        return ResponseEntity.ok(null);
    }    
    
    /**
     * Endpoint to create a new transfer.
     * @param request transfer session request parameter object
     * @param result the binding result
     * @return {@link TransferObject}
     */
    //@RequestMapping(value = EndPoints.TRANSFER, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)
    @ResponseBody    
    public TransferObject createTrasfer(@ModelAttribute @Valid TransferSessionParameter request, BindingResult result)
    {     
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        return new TransferObject();
    }     
    
    /**
     * Endpoint to get encrypted token file.
     * @param transferId
     * @param request
     * @param result
     * @return {@link HttpEntity}
     */
    //@RequestMapping(value = EndPoints.TOKEN_BY_TRANSFER_ID,  method=RequestMethod.GET)
    @ResponseBody     
    public HttpEntity<byte[]> fetchToken(@PathVariable String transferId, @ModelAttribute @Valid TransferPinParameter request, BindingResult result)
    {        
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        // to-do
        String fileName = "encrypted.file";
        byte[] documentBody = fileName.getBytes();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName.replace(" ", "_"));
        header.setContentLength(documentBody.length);
        return new HttpEntity<>(documentBody, header);
    }    
    
    /**
     * Endpoint to get encrypted token file.
     * @param transferId
     * @param deviceId
     * @param file
     * @return {@link HttpEntity}
     */
    //@RequestMapping(value = EndPoints.TOKEN_BY_TRANSFER_ID, method=RequestMethod.POST)
    @ResponseBody     
    public HttpEntity<byte[]> uploadToken(@PathVariable String transferId, @RequestParam String deviceId, @RequestPart MultipartFile file)        
    {                          
        String filename = file.getOriginalFilename();
        // process uploaded file, check its validity...
        // TO-DO
        
        //byte[] documentBody = new byte[1024];        
        //HttpHeaders header = new HttpHeaders();
        //header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        //header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename.replace(" ", "_"));
        //header.setContentLength(documentBody.length);        
        //return new HttpEntity<>(documentBody, header);
        return ResponseEntity.ok(null);
    }      
   
    /**
     * Endpoint to check if encrypted token file is available.
     * @param transferId
     * @return 
     */
    //@RequestMapping(value = EndPoints.TOKEN_BY_TRANSFER_ID, method=RequestMethod.HEAD)
    public ResponseEntity checkTokenAvailability(@PathVariable String transferId)
    {
        // TO-DO
        return ResponseEntity.ok(null);
    }    
    
    /**
     * Endpoint to get the status of the report.
     * @param transferId
     * @return 
     */
    //@RequestMapping(value = EndPoints.REPORT_BY_TRANSFER_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody 
    public TokenReport getReportStatus(@PathVariable String transferId)
    {
        // TO-DO
        return new TokenReport();
    }    
    
    /**
     * Endpoint to create a final report of the current transfer.
     * @param transferId
     * @param deviceId
     * @return 
     */
    //@RequestMapping(value = EndPoints.REPORT_BY_TRANSFER_ID, method=RequestMethod.POST)    
    public ResponseEntity createTransferReport(@PathVariable String transferId, @RequestParam String deviceId)
    {
        // TO-DO
        return ResponseEntity.ok(null);
    }    
    
    /**
     * Endpoint to create association request.
     * @param request
     * @param result
     * @return 
     */
    //@RequestMapping(value = EndPoints.ASSOCIATION_REQUEST, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)
    @ResponseBody 
    public AssociationRequestObject createAssociationRequest(@ModelAttribute @Valid AssociationRequestParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        // TO-DO
        return new AssociationRequestObject();
    }     
    
    /**
     * Endpoint to verify if an association request exists/is active.
     * @param requestId
     * @return 
     */
    //@RequestMapping(value = EndPoints.ASSOCIATION_REQUEST_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody 
    public AssociationRequestObject isActiveAssociationRequest(@PathVariable String requestId)
    {
     
        // TO-DO
        return new AssociationRequestObject();
    }     
    
    /**
     * Endpoint to extend active association request expiration.
     * @param requestId
     * @param request
     * @param result
     * @return 
     */
    //@RequestMapping(value = EndPoints.ASSOCIATION_REQUEST_BY_ID, method=RequestMethod.PUT)    
    public AssociationRequestObject extendAssociationRequestExpiration(@PathVariable String requestId, @ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        // TO-DO
        return new AssociationRequestObject();
    }     
    
    /**
     * Endpoint to destroys an active association request by its requestId.
     * @param requestId
     * @param request
     * @param result
     * @return 
     */
    //@RequestMapping(value = EndPoints.ASSOCIATION_REQUEST_BY_ID, method=RequestMethod.DELETE)    
    public ResponseEntity deleteAssociationRequest(@PathVariable String requestId, @ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        // TO-DO
        return ResponseEntity.ok(null);
    }      
    
    /**
     * Endpoint to get the data of an association request.
     * @param requestId
     * @return 
     */
    //@RequestMapping(value = EndPoints.DATA_OF_ASSOCIATION_REQUEST_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody 
    public Association getAssociationRequestData(@PathVariable String requestId)
    {
     
        // TO-DO
        return new Association();
    }     
    
    /**
     * Endpoint to accept an association request.
     * @param requestId
     * @param request
     * @param result
     * @return 
     */
    //@RequestMapping(value = EndPoints.ASSOCIATION_REQUEST_BY_ID_ACCEPT, method=RequestMethod.POST)
    public ResponseEntity acceptAssociationRequest(@PathVariable String requestId, @ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {     
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        // TO-DO
        return ResponseEntity.ok(null);
    }     
    
    /**
     * Endpoint to reject an association request.
     * @param requestId
     * @param request
     * @param result
     * @return 
     */
    //@RequestMapping(value = EndPoints.ASSOCIATION_REQUEST_BY_ID_REJECT, method=RequestMethod.POST)
    public ResponseEntity rejectAssociationRequest(@PathVariable String requestId, @ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {     
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        
        // TO-DO
        return ResponseEntity.ok(null);
    }      
    
    /**
     * Endpoint to gets all associations for given VID.
     * @param vid
     * @param request
     * @param result
     * @return 
     */
    //@RequestMapping(value = EndPoints.ASSOCIATIONS_BY_VID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    @ResponseBody 
    public Associations getAssociationsByVid(@PathVariable String vid, @ModelAttribute @Valid AssociationRequestParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        // TO-DO
        return new Associations();
    }    
    
    /**
     * Endpoint to get information about an association.
     * @param vid
     * @param assocId
     * @param request
     * @param result
     * @return 
     */
    //@RequestMapping(value = EndPoints.ASSOCIATIONS_BY_VID_AND_ASSOC_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    @ResponseBody 
    public Association getAssociationById(@PathVariable String vid, @PathVariable String assocId, @ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        // TO-DO
        return new Association();
    }     
    
    /**
     * Endpoint to delete an association.
     * @param vid
     * @param assocId
     * @param request
     * @param result
     * @return 
     */
    //@RequestMapping(value = EndPoints.ASSOCIATIONS_BY_VID_AND_ASSOC_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.DELETE)  
    @ResponseBody 
    public ResponseEntity deleteAssociation(@PathVariable String vid, @PathVariable String assocId, @ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        // TO-DO
        return ResponseEntity.ok(null);
    }      
    
    /**
     * Endpoint to initialize a new Verifis Token.
     * @param request
     * @param result
     * @return 
     */
    //@RequestMapping(value = EndPoints.TOKEN, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)  
    @ResponseBody     
    public VerifisToken createVerifisAccountToken(@ModelAttribute @Valid DeviceIdParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        return new VerifisToken();
    }
    
    /**
     * Endpoint to download the un-encrypted Verifis token file.
     * @param vid
     * @param request
     * @param result
     * @return {@link HttpEntity}
     */
    //@RequestMapping(value = EndPoints.TOKEN_DOWNLOAD_BY_VID,  method=RequestMethod.GET)    
    public HttpEntity<byte[]> downloadUnencryptedVerifisToken(@PathVariable String vid, @ModelAttribute @Valid VerifisTokenParameter request, BindingResult result)
    {        
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        // to-do
        String fileName = "unencrypted.file";
        byte[] documentBody = fileName.getBytes();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName.replace(" ", "_"));
        header.setContentLength(documentBody.length);
        return new HttpEntity<>(documentBody, header);
    }    
    
    /**
     * Endpoint to get the status of the new Verifis token report
     * @param request
     * @param result
     * @return 
     */
    //@RequestMapping(value = EndPoints.TOKEN_REPORT_BY_VID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    @ResponseBody     
    public TokenReport getVerifisTokenReport(@ModelAttribute @Valid DeviceIdParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        return new TokenReport();
    }    
    
    /**
     * Endpoint to create a final report of the new token initialization.
     * @param vid
     * @param request
     * @param result
     * @return 
     */
    //@RequestMapping(value = EndPoints.TOKEN_REPORT_BY_VID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)  
    @ResponseBody 
    public ResponseEntity createTokenReport(@PathVariable String vid, @ModelAttribute @Valid DeviceIdParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        // TO-DO
        return ResponseEntity.ok(null);
    }     
    
}
