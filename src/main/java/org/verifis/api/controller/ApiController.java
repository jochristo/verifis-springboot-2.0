package org.verifis.api.controller;

import java.io.IOException;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.verifis.api.Constants;
import org.verifis.api.EndPoints;
import org.verifis.api.component.LogoComponent;
import org.verifis.api.domain.Association;
import org.verifis.api.model.serializable.TransferObject;
import org.verifis.api.domain.Verifis;
import org.verifis.api.handler.IFieldValidationHandler;
import org.verifis.api.model.AssociationProvider;
import org.verifis.api.model.AssociationRequestData;
import org.verifis.api.model.serializable.AssociationRequestObject;
import org.verifis.api.model.Associations;
import org.verifis.api.model.TokenReport;
import org.verifis.api.model.serializable.VerifisToken;
import org.verifis.api.model.bind.ActiveTransferParameter;
import org.verifis.api.model.bind.AssociationRequestParameter;
import org.verifis.api.model.bind.AssociationsRequestParameter;
import org.verifis.api.model.bind.DeviceIdParameter;
import org.verifis.api.model.bind.TransferIdParameter;
import org.verifis.api.model.bind.TransferPinParameter;
import org.verifis.api.model.bind.TransferSessionParameter;
import org.verifis.api.model.bind.VerifisCodeParameter;
import org.verifis.api.model.bind.VerifisTokenParameter;
import org.verifis.api.service.IAssociationRequestService;
import org.verifis.api.service.IAssociationService;
import org.verifis.api.service.IVerifisService;
import org.verifis.core.utility.ApplicationErrorCode;
import org.verifis.core.utility.Internal;
import org.verifis.api.service.IVerifisVerifier;
import org.verifis.api.service.ITransferHandler;

/**
 * Internal API controller.
 * @author ic
 */
@RestController
public class ApiController
{
    private static final Logger logger = LoggerFactory.getLogger(ApiController.class);    
    
    private final static String internal = ""; ///internal/";
    
    @Autowired private IFieldValidationHandler fieldValidation;          
    @Autowired private IVerifisService verifisService;
    @Autowired private IAssociationRequestService requestService;
    @Autowired private IAssociationService associationService;
    @Autowired private IVerifisVerifier verifisVerification;    
    @Autowired private ITransferHandler transferComponent;
    
    @InitBinder
    protected void initBinder(final WebDataBinder webdataBinder)
    {
        //webdataBinder.setValidator(new VerifisCodeValidator());
        //webdataBinder.addValidators(new VerifisCodeValidator());
    }     
    
    /**
     * Endpoint to get API error codes.
     * @return 
     */
    @RequestMapping(value = EndPoints.API_ERROR_CODES, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody
    public List<ApplicationErrorCode> getApplicationErrorDescriptions()
    {        
        return Internal.getApiErrorCodes();
    }    
    
    /**
     * REturns all Verifis accounts stored: FOR INTERNAL USE - temporary
     * @return 
     */
    @RequestMapping(value = internal + EndPoints.TOKEN, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    @ResponseBody     
    public List<Verifis> getAllVerifis()
    {        
        return verifisService.get();
    }     
       
     
    /**
     * 
     * @param vid
     * @param request
     * @param result
     * @return 
     */
    @RequestMapping(value = internal + EndPoints.TOKEN_BY_VID, method=RequestMethod.GET)
    public ResponseEntity verifyVerifisInstallation(@PathVariable String vid, @ModelAttribute @Valid DeviceIdParameter request, BindingResult result)
    {        
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }         
        return verifisService.verifyInstallation(vid, request.getDeviceId());
    }
    
    /**
     * 
     * @param vid
     * @param request
     * @param result
     * @return 
     */
    @RequestMapping(value = internal + EndPoints.TOKEN_BY_VID, method=RequestMethod.DELETE)
    public ResponseEntity resetVerifisInstallation(@PathVariable String vid, @ModelAttribute @Valid ActiveTransferParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){                        
            fieldValidation.resolveErrors(result.getFieldErrors());
        }         
        return verifisService.resetInstallation(vid, request.getDeviceId());
    }    
    
    @RequestMapping(value = internal + EndPoints.ASSOCIATION_REQUEST, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)
    @ResponseBody 
    public AssociationRequestObject createAssociationRequest(@ModelAttribute @Valid AssociationRequestParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }                
        // verify vid exists
        verifisVerification.getExplicitly(request.getVid());
        
        return requestService.create(request.getVid());
    }     
      
    @RequestMapping(value = internal + EndPoints.ASSOCIATION_REQUEST_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody 
    public AssociationRequestObject verifyAssociationRequest(@PathVariable String requestId)
    {
        return requestService.verify(requestId);
    }     
    
    @RequestMapping(value = internal + EndPoints.ASSOCIATION_REQUEST_BY_ID, method=RequestMethod.PUT)    
    public AssociationRequestObject extendAssociationRequest(@PathVariable String requestId, @ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        return requestService.extend(requestId);
    }     
    
    @RequestMapping(value = internal + EndPoints.ASSOCIATION_REQUEST_BY_ID, method=RequestMethod.DELETE)    
    public ResponseEntity deleteAssociationRequest(@PathVariable String requestId, @ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        return requestService.delete(requestId);
    }      
    
    @RequestMapping(value = internal + EndPoints.DATA_OF_ASSOCIATION_REQUEST_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody 
    public AssociationRequestData getAssociationRequestData(@PathVariable String requestId)
    {
        return requestService.get(requestId);
    }     
    
    @RequestMapping(value = internal + EndPoints.ASSOCIATION_REQUEST_BY_ID_ACCEPT, method=RequestMethod.POST)
    public ResponseEntity acceptAssociationRequest(@PathVariable String requestId, @ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {     
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        
        return requestService.accept(requestId, request.getCode());
    }     

    @RequestMapping(value = internal + EndPoints.ASSOCIATION_REQUEST_BY_ID_REJECT, method=RequestMethod.POST)
    public ResponseEntity rejectAssociationRequest(@PathVariable String requestId, @ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {     
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        
        return requestService.reject(requestId, request.getCode());
    }    
    
    @RequestMapping(value = internal + EndPoints.ASSOCIATIONS_BY_VID, method=RequestMethod.GET)
    public Associations getAssociationsByVid(@PathVariable String vid, @ModelAttribute @Valid AssociationsRequestParameter request, BindingResult result)
    {     
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }                
        return associationService.find(vid, request);
    }      
    
    @RequestMapping(value = internal + EndPoints.ASSOCIATIONS_BY_VID_AND_ASSOC_ID, method=RequestMethod.GET)
    public Association getAssociation(@PathVariable String vid, @PathVariable String assocId,@ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {     
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }                
        return associationService.find(vid, assocId);
    } 
    
    @RequestMapping(value = internal + EndPoints.ASSOCIATIONS_BY_VID_AND_ASSOC_ID, method=RequestMethod.DELETE)
    public void deleteAssociation(@PathVariable String vid, @PathVariable String assocId,@ModelAttribute @Valid VerifisCodeParameter request, BindingResult result)
    {     
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }                
        associationService.delete(vid, assocId);
    }    
    
    
    /**
     * Endpoint to initialize a new Verifis Token.
     * Status 'OK' is returned when a new Verifis ID is successfully generated on the server and returned as a JSON object. 
     * After the Verifis mobile application fetches its Verifis ID, it should call the endpoint /token/{vid}/download in order to fetch the unencrypted token file. 
     * Then, mobile application should deploy the unencrypten token file, as well as encrypting it with user's PIN code. 
     * Then, mobile application should submit a report with the outcome of that process with a call to /token/{vid}/report. 
     * If device refused to download the token file as well as to properly deploy it within the 'validUntil' period, the VID will automatically expires. 
     * Also, if value of query parameter 'deviceId' references to an other active Verifis device, server should immediately deactivate the other device.
     * @param request
     * @param result
     * @return 
     */
    @RequestMapping(value = internal + EndPoints.TOKEN, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)  
    @ResponseBody     
    public VerifisToken initializeNewVerifisToken(@ModelAttribute @Valid DeviceIdParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }                        
        return verifisService.initializeToken(request);
    }  
    
    /**
     * Endpoint to download the un-encrypted Verifis token file.
     * @param vid
     * @param request
     * @param result
     * @return {@link HttpEntity}
     */
    @RequestMapping(value = internal + EndPoints.TOKEN_DOWNLOAD_BY_VID,  method=RequestMethod.GET)    
    public HttpEntity<byte[]> downloadUnencryptedVerifisToken(@PathVariable String vid, @ModelAttribute @Valid VerifisTokenParameter request, BindingResult result)
    {        
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        return verifisService.getUnencryptedToken(vid, request);
    }    
    
    /**
     * Endpoint to get the status of the new Verifis token report
     * @param vid
     * @return 
     */
    @RequestMapping(value = internal + EndPoints.TOKEN_REPORT_BY_VID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    @ResponseBody     
    public TokenReport getVerifisTokenReport(@PathVariable String vid)
    {        
        return verifisService.getTokenReport(vid);
    }    
    
    /**
     * Endpoint to create a final report of the new token initialization.
     * @param vid
     * @param request
     * @param result
     * @return 
     */
    @RequestMapping(value = internal + EndPoints.TOKEN_REPORT_BY_VID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)  
    @ResponseBody 
    public ResponseEntity createTokenReport(@PathVariable String vid, @ModelAttribute @Valid DeviceIdParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        
        // use turnary operator
        return verifisService.createTokenReport(vid, request.getDeviceId()) == true 
            ? ResponseEntity.ok().build()
            : ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }     
    
    
    // TRANSFER METHODS - START
    
    @RequestMapping(value = internal + EndPoints.TRANSFER, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)
    @ResponseBody    
    public TransferObject createTrasfer(@ModelAttribute @Valid TransferSessionParameter request, BindingResult result)
    {     
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        return transferComponent.create(request);
    }      
    
    @RequestMapping(value = internal + EndPoints.TRANSFER_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody        
    public TransferObject verifyTrasferStatus(@ModelAttribute @Valid TransferIdParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        return transferComponent.verify(request.getTransferId());
    } 
    
    @RequestMapping(value = internal + EndPoints.TRANSFER_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.PUT)
    @ResponseBody    
    public TransferObject extendActiveTrasfer(@PathVariable String transferId, @ModelAttribute @Valid ActiveTransferParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        return transferComponent.extend(transferId, request);
    }     
    
    @RequestMapping(value = internal + EndPoints.TRANSFER_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.DELETE)
    @ResponseBody      
    public ResponseEntity deleteActiveTrasfer(@PathVariable String transferId, @ModelAttribute @Valid ActiveTransferParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        return transferComponent.delete(transferId, request);
    } 

   
    
    @RequestMapping(value = internal + EndPoints.TOKEN_BY_TRANSFER_ID,  method=RequestMethod.GET)
    @ResponseBody     
    public HttpEntity<byte[]> fetchToken(@PathVariable String transferId, @ModelAttribute @Valid TransferPinParameter request, BindingResult result)
    {        
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }
        
        return transferComponent.download(request.getTransferPin(), transferId);
    }    
    
    @RequestMapping(value = internal + EndPoints.TOKEN_BY_TRANSFER_ID, method=RequestMethod.POST)
    @ResponseBody     
    public HttpEntity<byte[]> uploadToken(@PathVariable String transferId, @ModelAttribute @Valid DeviceIdParameter request, BindingResult result, //@RequestPart MultipartFile file, 
            HttpEntity<byte[]> bytes) throws IOException                    
    {                     
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        byte[] data = bytes.getBody();
        return transferComponent.upload(request.getDeviceId(), transferId, data);
    }      
   
    @RequestMapping(value = internal + EndPoints.TOKEN_BY_TRANSFER_ID, method=RequestMethod.HEAD)
    public HttpEntity checkTokenAvailability(@PathVariable String transferId)
    {
        return transferComponent.checkTokenAvailability(transferId);
    }    
    
    @RequestMapping(value = internal + EndPoints.REPORT_BY_TRANSFER_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody 
    public TokenReport getReportStatus(@PathVariable String transferId)
    {
        return transferComponent.getStatus(transferId);
    }    
    
    //@RequestMapping(value = internal + EndPoints.REPORT_BY_TRANSFER_ID, method=RequestMethod.POST)    
    @Deprecated
    public HttpEntity createTransferReport(@PathVariable String transferId, @RequestParam String deviceId)
    {
        return transferComponent.createReport(deviceId, transferId);
    }      
    
    @RequestMapping(value = internal + EndPoints.REPORT_BY_TRANSFER_ID, method=RequestMethod.POST)    
    public TokenReport createTransferTokenReport(@PathVariable String transferId, @ModelAttribute @Valid DeviceIdParameter request, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidation.resolveErrors(result.getFieldErrors());
        }        
        return transferComponent.createTokenReport(request.getDeviceId(), transferId);
    }      
    
    // TRANSFER METHODS - END
        
      
    @Autowired
    private LogoComponent logoComponent;    
 
    
    @RequestMapping(value = internal + "/logo/{filename:.+}", method=RequestMethod.GET)
    @ResponseBody 
    public HttpEntity<byte[]> getLogo(@PathVariable String filename) throws IOException
    {

        //if(logoComponent.existsResourceFile(filename) == false){
        //    throw new ResourceNotFoundException("file not found: " + filename);
        //}        
                
        byte[] data = logoComponent.getData(filename);
        if(data == null){
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
        
        byte[] documentBody = data;
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename.replace(" ", "_"));
        header.setContentLength(documentBody.length);
        return new HttpEntity<>(documentBody, header); 
    } 
    
    @RequestMapping(value = internal + "/providers", produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
    @ResponseBody     
    public AssociationProvider providers(){
        return logoComponent.getRandomProvider();
    }

    
}
