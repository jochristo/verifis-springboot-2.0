package org.verifis.api;

/**
 * Defines Verifis API endpoints.
 * @author ic
 */
public interface EndPoints
{
    public static final String CONTEXT_PATH = "";///verifis"; 
    
    public static final String API_PATH = CONTEXT_PATH + "";//"/api/v1";
    
    public static final String SPRING_BOOT_ERRORS = "/error";	          
    
    public static final String API_ERROR_CODES = "/codes";	    

    public static final String BOOT = API_PATH + "/boot"; //[GET]

    public static final String TRANSFER = API_PATH + "/transfer"; //[POST]
    
    public static final String TRANSFER_BY_ID = API_PATH + "/transfer/{transferId}"; //[GET,PUT,DELETE]
    
    public static final String TOKEN = API_PATH + "/token"; //[POST]  
    
    public static final String TOKEN_DOWNLOAD_BY_VID = API_PATH + "/token/{vid}/download"; //[GET]  
    
    public static final String TOKEN_REPORT_BY_VID = API_PATH + "/token/{vid}/report"; //[GET,POST]      
    
    public static final String TOKEN_BY_VID = API_PATH + "/token/{vid}"; //[GET,DELETE]       
    
    public static final String TOKEN_BY_TRANSFER_ID = API_PATH + "/transfer/{transferId}/token"; //[POST,DELETE,HEAD]           

    public static final String REPORT_BY_TRANSFER_ID = API_PATH + "/transfer/{transferId}/report"; //[POST,DELETE]           
    
    public static final String ASSOCIATION_REQUEST = API_PATH + "/association"; //[POST]
    
    public static final String ASSOCIATION_REQUEST_BY_ID = API_PATH + "/association/{requestId}"; //[GET,PUT,DELETE]
    
    public static final String DATA_OF_ASSOCIATION_REQUEST_BY_ID = API_PATH + "/association/{requestId}/data"; //[GET]
    
    public static final String ASSOCIATION_REQUEST_BY_ID_ACCEPT = API_PATH + "/association/{requestId}/accept"; //[POST]
    
    public static final String ASSOCIATION_REQUEST_BY_ID_REJECT = API_PATH + "/association/{requestId}/reject"; //[POST]
    
    public static final String ASSOCIATIONS_BY_VID = API_PATH + "/associations/{vid}"; //[GET]
    
    public static final String ASSOCIATIONS_BY_VID_AND_ASSOC_ID = API_PATH + "/associations/{vid}/{assocId}"; //[GET,DELETE]
    
}
