/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.verifis.api.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.verifis.api.model.AssociationProvider;
import org.verifis.core.utility.Utilities;

/**
 *
 * @author ic
 */
@Component
public class LogoComponent
{
    @Value(value = "classpath:logo/trophy1.png")
    private Resource trophy1;
    
    @Value(value = "classpath:logo/trophy2.png")
    private Resource trophy2;

    @Value(value = "classpath:logo/threetrophies.jpg")
    private Resource threetrophies;

    @Value(value = "classpath:logo/ceramic1.jpg")
    private Resource ceramic1;

    @Value(value = "classpath:logo/ceramic2.jpg")
    private Resource ceramic2;  

    protected HashSet<String> getFilenames()
    {
        HashSet<String> resources = new HashSet<>();
        resources.add(trophy1.getFilename());
        resources.add(trophy2.getFilename());
        resources.add(ceramic1.getFilename());
        resources.add(ceramic2.getFilename());
        resources.add(threetrophies.getFilename());
        return resources;
        
    }   
    
    public byte[] getData(String filename) throws IOException{
        if(existsResourceFile(filename))
        {
            List<Resource> resources = this.getResources().stream().filter(item->item.getFilename().equals(filename)).collect(Collectors.toList());
            if(resources != null){
                return Utilities.toByteArray(resources.get(0).getInputStream());
            }
        }
        return null;
    }
    
    protected HashSet<Resource> getResources(){
        HashSet<Resource> resources = new HashSet<>();
        resources.add(trophy1);
        resources.add(trophy2);
        resources.add(ceramic1);
        resources.add(ceramic2);
        resources.add(threetrophies);
        return resources;
    }
    
    public String randomFilename()
    {                
        int start = 0;
        int limit = getFilenames().size() - 1;
        int randomIndex = ThreadLocalRandom.current().nextInt(start, limit);
        ArrayList<Resource> list = new ArrayList(this.getResources());        
        return list.get(randomIndex).getFilename();
    }
    
    public boolean existsResourceFile(String filename){
        Set set = this.getFilenames();
        return set.contains(filename);
    }
    
    protected List<AssociationProvider> providers()
    {
        List<AssociationProvider> list = new ArrayList();
        AssociationProvider provider1 = new AssociationProvider();
        provider1.setService("Solutions4Mobiles");                       
        provider1.setUrl("http://www.Solutions4Mobiles.com");
        provider1.setLogo(randomFilename());        
        list.add(provider1);
        
        AssociationProvider provider2 = new AssociationProvider();        
        provider2.setService("AMAZON");                       
        provider2.setUrl("http://www.amazon.com");
        provider2.setLogo(randomFilename());         
        list.add(provider2);
        
        AssociationProvider provider3 = new AssociationProvider();
        provider3.setService("e-Bay");                       
        provider3.setUrl("http://www.ebay.com");
        provider3.setLogo(randomFilename());              
        list.add(provider3);
        
        AssociationProvider provider4 = new AssociationProvider();        
        provider4.setService("PAYPAL");                       
        provider4.setUrl("http://www.paypal.com");
        provider4.setLogo(randomFilename());         
        list.add(provider4);        
        
        AssociationProvider provider5 = new AssociationProvider();        
        provider5.setService("linkedin");                       
        provider5.setUrl("http://www.linkedin.com");
        provider5.setLogo(randomFilename());         
        list.add(provider5);
        
        AssociationProvider provider6 = new AssociationProvider();
        provider6.setService("Verified By Visa");                       
        provider6.setUrl("http://www.visa.com");
        provider6.setLogo(randomFilename());         
        list.add(provider6);
                
        AssociationProvider provider7 = new AssociationProvider();
        provider7.setService("Mastercard SecureCode");                       
        provider7.setUrl("http://www.mastercard.com");
        provider7.setLogo(randomFilename());         
        list.add(provider7);
        
        AssociationProvider provider8 = new AssociationProvider();
        provider8.setService("Google");                       
        provider8.setUrl("http://www.google.com");
        provider8.setLogo(randomFilename());         
        list.add(provider8);
        
        AssociationProvider provider9 = new AssociationProvider();
        provider9.setService("facebook");                       
        provider9.setUrl("http://www.facebook.com");
        provider9.setLogo(randomFilename());         
        list.add(provider9);
        
        AssociationProvider provider10 = new AssociationProvider();
        provider10.setService("aliexpress");                       
        provider10.setUrl("http://www.aliexpress.com");
        provider10.setLogo(randomFilename());         
        list.add(provider10);        
        
        return list;
    }
    
    public AssociationProvider getRandomProvider()
    {
        List<AssociationProvider> data = this.providers();
        int start = 0;
        int limit = data.size() - 1;
        int randomIndex = ThreadLocalRandom.current().nextInt(start, limit);       
        return data.get(randomIndex);
    }
}
