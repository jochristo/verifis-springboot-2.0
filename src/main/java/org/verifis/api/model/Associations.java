package org.verifis.api.model;

import org.verifis.api.domain.Association;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "totalAssocs",
    "limit",
    "start",
    "associations"
})
public class Associations {

    @JsonProperty("totalAssocs")
    private Integer totalAssocs;
    
    @JsonProperty("limit")
    private Integer limit = -1;
    
    @JsonProperty("start")
    private Integer start = -1;
    
    @JsonProperty("associations")
    private List<Association> associations = null;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("totalAssocs")
    public Integer getTotalAssocs() {
        return totalAssocs;
    }

    @JsonProperty("totalAssocs")
    public void setTotalAssocs(Integer totalAssocs) {
        this.totalAssocs = totalAssocs;
    }

    @JsonProperty("limit")
    public Integer getLimit() {
        return limit;
    }

    @JsonProperty("limit")
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @JsonProperty("start")
    public Integer getStart() {
        return start;
    }

    @JsonProperty("start")
    public void setStart(Integer start) {
        this.start = start;
    }

    @JsonProperty("associations")
    public List<Association> getAssociations() {
        return associations;
    }

    @JsonProperty("associations")
    public void setAssociations(List<Association> associations) {
        this.associations = associations;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
