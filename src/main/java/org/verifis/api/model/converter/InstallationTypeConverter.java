package org.verifis.api.model.converter;

import org.springframework.core.convert.converter.Converter;
import org.verifis.api.model.enumeration.InstallationType;

/**
 *
 * @author ic
 */
public class InstallationTypeConverter implements Converter<InstallationType, String>
{

    @Override
    public String convert(InstallationType s) {
        return s.getDescription();
    }
    
}
