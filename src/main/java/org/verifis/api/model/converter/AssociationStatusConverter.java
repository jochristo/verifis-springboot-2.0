package org.verifis.api.model.converter;

import org.springframework.core.convert.converter.Converter;
import org.verifis.api.model.enumeration.AssociationStatus;

/**
 *
 * @author ic
 */
public class AssociationStatusConverter implements Converter<AssociationStatus,String>
{
    @Override
    public String convert(AssociationStatus s) {
        return s.getDescription();
    }
    
}
