package org.verifis.api.model.converter;

import org.springframework.core.convert.converter.Converter;
import org.verifis.api.model.enumeration.OperationStatus;

/**
 *
 * @author ic
 */
public class VerifisStatusConverter implements Converter<OperationStatus,String>
{

    @Override
    public String convert(OperationStatus s) {
        return s.getDescription();
    }
    
}
