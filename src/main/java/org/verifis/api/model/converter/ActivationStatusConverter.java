package org.verifis.api.model.converter;

import org.springframework.core.convert.converter.Converter;
import org.verifis.api.model.enumeration.ActivationStatus;

/**
 *
 * @author ic
 */
public class ActivationStatusConverter implements Converter<ActivationStatus,String>
{

    @Override
    public String convert(ActivationStatus s) {
        return s.getDescription();
    }
    
}
