package org.verifis.api.model.converter;

import org.springframework.core.convert.converter.Converter;
import org.verifis.api.model.enumeration.MobilePlatform;

/**
 * MobilePlatform to string description converter.
 * @author ic
 */
public class MobilePlatformConverter implements Converter<MobilePlatform,String>
{

    @Override
    public String convert(MobilePlatform s) {
        return s.getDescription();
    }
    
}
