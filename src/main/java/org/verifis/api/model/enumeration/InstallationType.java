package org.verifis.api.model.enumeration;

/**
 * Designates the type of the Verifis installation.
 * Possible values: NEW, TRANSFER
 * @author ic
 */
public enum InstallationType
{
    NEW(0,"NEW"),    
    
    TRANSFER(1,"TRANSFER")        
    ;
    
    private InstallationType(int value, String description){
        this.value = value;
        this.description = description;
    }
    
    private final int value;
    private final String description;

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }       
}
