package org.verifis.api.model.enumeration;

/**
 *
 * @author Admin
 */
public enum DigitFieldType
{
    TRANSFER_PIN(0,"transferPin"),    
    
    ACCESS_TOKEN(1,"accessToken"),
    
    VERIFIS_CODE(2,"code"),
    ;
    
    private DigitFieldType(int value, String description){
        this.value = value;
        this.description = description;
    }
    
    private final int value;
    private final String description;

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }     
    
   
}
