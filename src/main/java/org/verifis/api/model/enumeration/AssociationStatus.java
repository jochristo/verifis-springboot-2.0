package org.verifis.api.model.enumeration;

/**
 *
 * @author ic
 */
public enum AssociationStatus
{
    ACTIVE(0,"ACTIVE"),    
    
    INACTIVE(1,"INACTIVE"),
    
    EXPIRED(2,"EXPIRED"),
    ;
    
    private AssociationStatus(int value, String description){
        this.value = value;
        this.description = description;
    }
    
    private final int value;
    private final String description;

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }    
    
}
