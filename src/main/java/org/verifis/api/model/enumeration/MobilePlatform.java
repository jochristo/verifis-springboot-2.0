package org.verifis.api.model.enumeration;

/**
 * Mobile platform enumeration class.
 * @author ic
 */
public enum MobilePlatform
{
    IOS(0,"ios"),
    
    ANDROID(1,"android"),
    
    NONE(2,"none"),
    ;
    
    private MobilePlatform(int value, String description){
        this.value = value;
        this.description = description;
    }
    
    private final int value;
    private final String description;

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
    
    
            
}
