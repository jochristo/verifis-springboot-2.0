package org.verifis.api.model.enumeration;

/**
 * Designates the status of a Verifis token/transfer report.
 * @author ic
 */
public enum OperationStatus
{
    SUCCESS(0,"SUCCESS"),    
    
    FAILURE(1,"FAILURE")        
    ;
    
    private OperationStatus(int value, String description){
        this.value = value;
        this.description = description;
    }
    
    private final int value;
    private final String description;

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }     
}
