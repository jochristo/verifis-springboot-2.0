package org.verifis.api.model.serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import org.verifis.api.domain.Verifis;

/**
 * Represents an un-encrypted token file attributes object.
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UnencryptedTokenItem implements Serializable
{       
    @JsonProperty("accessToken")
    private String accessToken;        
    
    @JsonProperty("deviceId")
    private String deviceId;
    
    @JsonProperty("vid")
    private String vid;
    
    @JsonIgnore
    private byte[] data;
    
    @JsonProperty("verifis")
    private Verifis verifis;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }
    
    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }    

    public UnencryptedTokenItem() {
    }    
    
    public UnencryptedTokenItem(String accessToken, String deviceId, String vid) {
        this.accessToken = accessToken;
        this.deviceId = deviceId;
        this.vid = vid;
    }        

    public UnencryptedTokenItem(String accessToken, Verifis verifis) {
        this.accessToken = accessToken;
        this.verifis = verifis;
        this.vid = verifis.getVid();
        this.deviceId = verifis.getDeviceId();
    }
    
    

    public Verifis getVerifis() {
        return verifis;
    }

    public void setVerifis(Verifis verifis) {
        this.verifis = verifis;
    }
            
}
