package org.verifis.api.model.serializable;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;

/**
 * Verifis token DTO
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "vid",
    "validUntil",
    "accessPin"
})
public class VerifisToken implements Serializable
{

    @JsonProperty("vid")
    private String vid;
    @JsonProperty("validUntil")
    private Integer validUntil;
    @JsonProperty("accessPin")
    private String accessPin;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("vid")
    public String getVid() {
        return vid;
    }

    @JsonProperty("vid")
    public void setVid(String vid) {
        this.vid = vid;
    }

    @JsonProperty("validUntil")
    public Integer getValidUntil() {
        return validUntil;
    }

    @JsonProperty("validUntil")
    public void setValidUntil(Integer validUntil) {
        this.validUntil = validUntil;
    }

    @JsonProperty("accessPin")
    public String getAccessPin() {
        return accessPin;
    }

    @JsonProperty("accessPin")
    public void setAccessPin(String accessPin) {
        this.accessPin = accessPin;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
