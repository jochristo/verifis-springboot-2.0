package org.verifis.api.model.serializable;

import java.io.Serializable;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.util.DigestUtils;
import org.verifis.api.Constants;
import org.verifis.api.model.enumeration.OperationStatus;
import org.verifis.core.utility.Utilities;

/**
 * Represents a transfer item stored in cache.
 * @author ic
 */
public class TransferItem implements Serializable
{
    @Id        
    private String transferId;        
    
    private String vid;    
    
    private String recipientDeviceId;   
    
    private String forwardingDeviceId;
    
    private Integer expires;      
       
    private byte[] token;
    
    private String transferPin; //temporary
    
    private OperationStatus status;

    public TransferItem()
    {
        this.transferId = DigestUtils.md5DigestAsHex(UUID.randomUUID().toString().getBytes());       
        this.expires = Utilities.currentEpochPlus(Constants.Cache.TRANSFER_EXPIRY_SECONDS);
        this.status = OperationStatus.FAILURE; // default until successfully changed
    }    
    
    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getRecipientDeviceId() {
        return recipientDeviceId;
    }

    public void setRecipientDeviceId(String deviceId) {
        this.recipientDeviceId = deviceId;
    }

    public Integer getExpires() {
        return expires;
    }

    public void setExpires(Integer expires) {
        this.expires = expires;
    }

    public byte[] getToken() {
        return token;
    }

    public void setToken(byte[] token) {
        this.token = token;
    }

    public String getTransferPin() {
        return transferPin;
    }

    public void setTransferPin(String transferPin) {
        this.transferPin = transferPin;
    }
    
    public OperationStatus getStatus() {
        return status;
    }

    public void setStatus(OperationStatus status) {
        this.status = status;
    }    

    public String getForwardingDeviceId() {
        return forwardingDeviceId;
    }

    public void setForwardingDeviceId(String forwardingDeviceId) {
        this.forwardingDeviceId = forwardingDeviceId;
    }
    
    
}
