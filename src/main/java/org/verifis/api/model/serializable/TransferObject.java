package org.verifis.api.model.serializable;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Id;

/**
 * Represents a transfer response item.
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "transferId",
    "expires"
})
@Entity("transfer")
public class TransferObject implements Serializable
{

    @Id    
    @JsonProperty("transferId")    
    private String transferId;
    
    @JsonProperty("expires")
    private Integer expires;
    
    @JsonProperty("deleted")  
    @JsonIgnore
    private boolean deleted;    
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("transferId")
    public String getTransferId() {
        return transferId;
    }

    @JsonProperty("transferId")
    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    @JsonProperty("expires")
    public Integer getExpires() {
        return expires;
    }

    @JsonProperty("expires")
    public void setExpires(Integer expires) {
        this.expires = expires;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
