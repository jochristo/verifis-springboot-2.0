package org.verifis.api.model.serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 * Represents an association request attributes reference stored in in-memory cache.
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssociationRequestAttributes implements Serializable
{
    @JsonProperty("associationRequest")
    private AssociationRequestObject associationRequest;
    
    @JsonProperty("vid")
    private String vid;     

    public AssociationRequestObject getAssociationRequest() {
        return associationRequest;
    }

    public void setAssociationRequest(AssociationRequestObject associationRequest) {
        this.associationRequest = associationRequest;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public AssociationRequestAttributes(AssociationRequestObject associationRequest, String vid) {
        this.associationRequest = associationRequest;
        this.vid = vid;
    }

    public AssociationRequestAttributes() {
    }
    
    
    
}
