package org.verifis.api.model.bind;

import org.verifis.api.model.enumeration.MobilePlatform;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.verifis.api.Constants.FieldPattern;
import org.verifis.api.Constants.FieldValidationMessage;
import org.verifis.api.Constants.RequiredFieldMessage;
import org.verifis.api.annotation.EnumAttribute;


/**
 *
 * @author ic
 */
public class BootRequestParameter
{
    @NotNull(message = RequiredFieldMessage.VERIFIS_ID_REQUIRED)
    private String vid;
    
    @NotNull(message = RequiredFieldMessage.MOBILE_PLATFORM_REQUIRED)
    @EnumAttribute(enumClazz = MobilePlatform.class, message = FieldValidationMessage.INVALID_PLATFORM)
    private MobilePlatform platform;
    
    @NotNull(message = RequiredFieldMessage.CLIENT_VERSION_REQUIRED)
    @Pattern(regexp = FieldPattern.VERSION, message = FieldValidationMessage.INVALID_VERSION)
    private String ver;
    
    @NotNull(message = RequiredFieldMessage.DEVICE_ID_REQUIRED)
    private String deviceId;

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public MobilePlatform getPlatform() {
        return platform;
    }

    public void setPlatform(MobilePlatform platform) {
        this.platform = platform;
    }
    
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    
    
}
