package org.verifis.api.model.bind;

import javax.validation.constraints.NotNull;
import org.verifis.api.Constants;

/**
 *
 * @author ic
 */
public class RequestIdParameter {
 
    @NotNull(message = Constants.RequiredFieldMessage.REQUEST_ID_REQUIRED)
    private String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
    
    
}
