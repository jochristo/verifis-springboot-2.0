package org.verifis.api.model.bind;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import org.verifis.api.Constants;
import org.verifis.api.Constants.RequiredFieldMessage;
import org.verifis.api.annotation.DigitAttribute;
import org.verifis.api.model.enumeration.DigitFieldType;

/**
 *
 * @author ic
 */
public class VerifisCodeParameter {
 
    @JsonProperty("code")
    @NotNull(message = RequiredFieldMessage.VERIFIS_CODE_REQUIRED)
    @DigitAttribute(type = DigitFieldType.VERIFIS_CODE, pattern = Constants.FieldPattern.VERIFIS_CODE, message = Constants.FieldValidationMessage.INVALID_VERIFIS_CODE )
    private String code;        
        
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }    
    
}
