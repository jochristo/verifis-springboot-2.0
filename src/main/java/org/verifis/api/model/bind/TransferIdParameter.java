package org.verifis.api.model.bind;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import org.verifis.api.Constants.RequiredFieldMessage;

/**
 *
 * @author ic
 */
public class TransferIdParameter {
 
    @JsonProperty("transferId")
    @NotNull(message = RequiredFieldMessage.TRANSFER_ID_REQUIRED)
    private String transferId;    

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }
    
    
}
