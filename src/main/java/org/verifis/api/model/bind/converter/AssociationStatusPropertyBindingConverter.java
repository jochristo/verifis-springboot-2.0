package org.verifis.api.model.bind.converter;

import java.beans.PropertyEditorSupport;
import org.verifis.api.model.enumeration.AssociationStatus;

/**
 *
 * @author ic
 */
public class AssociationStatusPropertyBindingConverter extends PropertyEditorSupport
{
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        String lowerCase = text.toUpperCase();
        AssociationStatus as = AssociationStatus.valueOf(lowerCase);
        setValue(as);
    }    
}
