package org.verifis.api.model.bind;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import javax.validation.constraints.NotNull;
import org.verifis.api.Constants;
import org.verifis.api.Constants.FieldPattern;
import org.verifis.api.Constants.RequiredFieldMessage;
import org.verifis.api.annotation.DigitAttribute;
import org.verifis.api.model.enumeration.DigitFieldType;
/**
 *
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "code",
    "deviceId"
})
public class ActiveTransferParameter
{
    @JsonProperty("code")
    @NotNull(message = RequiredFieldMessage.VERIFIS_CODE_REQUIRED)        
    @DigitAttribute(type = DigitFieldType.VERIFIS_CODE, pattern = FieldPattern.VERIFIS_CODE, message = Constants.FieldValidationMessage.INVALID_VERIFIS_CODE )
    private String code;   
    
    @JsonProperty("deviceId")
    @NotNull(message = RequiredFieldMessage.DEVICE_ID_REQUIRED)
    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    } 

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    
}
