package org.verifis.api.model.bind;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import org.verifis.api.Constants;
import org.verifis.api.Constants.FieldPattern;
import org.verifis.api.annotation.DigitAttribute;
import org.verifis.api.model.enumeration.DigitFieldType;

/**
 *
 * @author ic
 */
public class AssociationsRequestParameter
{    
    @JsonProperty("code")
    @NotNull(message = Constants.RequiredFieldMessage.VERIFIS_CODE_REQUIRED)    
    @DigitAttribute(type = DigitFieldType.VERIFIS_CODE, pattern = FieldPattern.VERIFIS_CODE, message = Constants.FieldValidationMessage.INVALID_VERIFIS_CODE )
    private String code;      
        
    private String filter;    
        
    private Integer start;
    
    private Integer limit;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }    
    
}
