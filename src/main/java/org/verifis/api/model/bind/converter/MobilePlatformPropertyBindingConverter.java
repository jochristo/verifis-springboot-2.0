package org.verifis.api.model.bind.converter;

import java.beans.PropertyEditorSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.verifis.api.model.enumeration.MobilePlatform;

/**
 * Converts given binding property to its enumeration equivalent;
 * @author ic
 */
public class MobilePlatformPropertyBindingConverter extends PropertyEditorSupport
{
    private static final Logger logger = LoggerFactory.getLogger(MobilePlatformPropertyBindingConverter.class);     

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        String upperCase = text.toUpperCase();
        MobilePlatform mp = MobilePlatform.NONE;
        try{
            mp = MobilePlatform.valueOf(upperCase);
            
        }
        catch (IllegalArgumentException ex){
            //logger.info("IllegalArgumentException occured during enum transform... "+ ex.getMessage());                             
        }
        finally{
            setValue(mp);
        }        
    }  

    
    
    
}
