package org.verifis.api.model.bind;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import javax.validation.constraints.NotNull;
import org.verifis.api.Constants;
import org.verifis.api.Constants.FieldPattern;
import org.verifis.api.Constants.RequiredFieldMessage;
import org.verifis.api.model.enumeration.DigitFieldType;
import org.verifis.api.annotation.DigitAttribute;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "vid",
    "code",
    "deviceId",
    "transferPin"
})
public class TransferSessionParameter {

    @JsonProperty("vid")
    @NotNull(message = RequiredFieldMessage.VERIFIS_ID_REQUIRED)
    private String vid;
    
    @JsonProperty("code")
    @NotNull(message = RequiredFieldMessage.VERIFIS_CODE_REQUIRED)    
    @DigitAttribute(type = DigitFieldType.VERIFIS_CODE, pattern = FieldPattern.VERIFIS_CODE, message = Constants.FieldValidationMessage.INVALID_VERIFIS_CODE )
    private String code;
    
    @JsonProperty("deviceId")
    @NotNull(message = RequiredFieldMessage.DEVICE_ID_REQUIRED)
    private String deviceId;
    
    @JsonProperty("transferPin")
    @NotNull(message = RequiredFieldMessage.TRANSFER_PIN_REQUIRED)    
    @DigitAttribute(type = DigitFieldType.TRANSFER_PIN, pattern = FieldPattern.PIN, message = Constants.FieldValidationMessage.INVALID_TRANSFER_PIN)
    private String transferPin;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("vid")
    public String getVid() {
        return vid;
    }

    @JsonProperty("vid")
    public void setVid(String vid) {
        this.vid = vid;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("deviceId")
    public String getDeviceId() {
        return deviceId;
    }

    @JsonProperty("deviceId")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("transferPin")
    public String getTransferPin() {
        return transferPin;
    }

    @JsonProperty("transferPin")
    public void setTransferPin(String transferPin) {
        this.transferPin = transferPin;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
