package org.verifis.api.model.bind;

import javax.validation.constraints.NotNull;
import org.verifis.api.Constants.RequiredFieldMessage;

/**
 *
 * @author ic
 */
public class DeviceIdParameter {
    
    @NotNull(message = RequiredFieldMessage.DEVICE_ID_REQUIRED)
    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
    
    
}
