package org.verifis.api.model.bind;

import javax.validation.constraints.NotNull;
import org.verifis.api.Constants;
import org.verifis.api.Constants.RequiredFieldMessage;
import org.verifis.api.model.enumeration.DigitFieldType;
import org.verifis.api.annotation.DigitAttribute;

/**
 *
 * @author ic
 */
public class VerifisTokenParameter
{    
    @NotNull(message = RequiredFieldMessage.ACCESS_TOKEN_REQUIRED)    
    @DigitAttribute(type = DigitFieldType.ACCESS_TOKEN, pattern = Constants.FieldPattern.PIN, message = Constants.FieldValidationMessage.INVALID_ACCESS_PIN)
    private String accessToken;
    
    @NotNull(message = RequiredFieldMessage.DEVICE_ID_REQUIRED)
    private String deviceId;

    /**
     * Get the value of deviceId
     *
     * @return the value of deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * Set the value of deviceId
     *
     * @param deviceId new value of deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Get the value of accessToken
     *
     * @return the value of accessToken
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * Set the value of accessToken
     *
     * @param accessToken new value of accessToken
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    
    
}