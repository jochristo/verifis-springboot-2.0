package org.verifis.api.model.bind;

import javax.validation.constraints.NotNull;
import org.verifis.api.Constants.FieldPattern;
import org.verifis.api.Constants.FieldValidationMessage;
import org.verifis.api.Constants.RequiredFieldMessage;
import org.verifis.api.model.enumeration.DigitFieldType;
import org.verifis.api.annotation.DigitAttribute;

/**
 *
 * @author ic
 */
public class TransferPinParameter {
    
    @NotNull(message = RequiredFieldMessage.TRANSFER_PIN_REQUIRED)    
    @DigitAttribute(type = DigitFieldType.TRANSFER_PIN, pattern = FieldPattern.PIN, message = FieldValidationMessage.INVALID_TRANSFER_PIN)
    private String transferPin;

    public String getTransferPin() {
        return transferPin;
    }

    public void setTransferPin(String transferPin) {
        this.transferPin = transferPin;
    }

    
}
