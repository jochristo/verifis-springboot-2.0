package org.verifis.api.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "platform",
    "lastVer",
    "forceUpdate",
    "storeUrl"
})
public class BootResponse {

    @JsonProperty("platform")
    private String platform;
    @JsonProperty("lastVer")
    private String lastVer;
    @JsonProperty("forceUpdate")
    private Boolean forceUpdate;
    @JsonProperty("storeUrl")
    private String storeUrl;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("platform")
    public String getPlatform() {
        return platform;
    }

    @JsonProperty("platform")
    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @JsonProperty("lastVer")
    public String getLastVer() {
        return lastVer;
    }

    @JsonProperty("lastVer")
    public void setLastVer(String lastVer) {
        this.lastVer = lastVer;
    }

    @JsonProperty("forceUpdate")
    public Boolean getForceUpdate() {
        return forceUpdate;
    }

    @JsonProperty("forceUpdate")
    public void setForceUpdate(Boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    @JsonProperty("storeUrl")
    public String getStoreUrl() {
        return storeUrl;
    }

    @JsonProperty("storeUrl")
    public void setStoreUrl(String storeUrl) {
        this.storeUrl = storeUrl;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
