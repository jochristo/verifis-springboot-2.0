package org.verifis.api.advisor;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.verifis.api.model.serializable.TransferObject;
import org.verifis.api.exception.AccessTokenMismatchException;
import org.verifis.api.model.ErrorResponse;
import org.verifis.api.exception.AssociationRequestExpiredException;
import org.verifis.api.exception.AssociationVerifisMismatchException;
import org.verifis.api.exception.BadFormatException;
import org.verifis.api.exception.DataNotFoundException;
import org.verifis.api.exception.FieldConstraintValidationException;
import org.verifis.api.exception.FieldValidationException;
import org.verifis.api.exception.InvalidAccessTokenException;
import org.verifis.api.exception.InvalidPinFormatException;
import org.verifis.api.exception.InvalidTokenException;
import org.verifis.api.exception.InvalidTransferPinException;
import org.verifis.api.exception.InvalidVerifisCodeException;
import org.verifis.api.exception.InvalidVerifisCodeFormatException;
import org.verifis.api.exception.InvalidVerifisInstallationException;
import org.verifis.api.exception.MissingParameterException;
import org.verifis.api.exception.RequestIdNotFoundException;
import org.verifis.api.exception.RestApiException;
import org.verifis.api.exception.TokenUnavailableException;
import org.verifis.api.exception.TransferIdExpiredException;
import org.verifis.api.exception.TransferIdNotFoundException;
import org.verifis.api.exception.VerifisDeviceMismatchException;
import org.verifis.api.exception.VerifisIdNotActiveException;
import org.verifis.api.exception.VerifisIdNotFoundException;
import org.verifis.api.exception.VerifisReportNotYetSubmittedException;

/**
 * Rest controller exception advisory handler.
 * @author ic
 */
@ControllerAdvice(annotations = {RestController.class})
public class RestApiExceptionControllerAdviceImpl implements IRestApiExceptionControllerAdvice
{
    private static final Logger logger = LoggerFactory.getLogger(RestApiExceptionControllerAdviceImpl.class);          
     
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({FieldConstraintValidationException.class})
    @ResponseBody    
    public List<ErrorResponse> handleFieldConstraintValidationException(FieldConstraintValidationException ex)
    {
        logger.error("FieldConstraintValidationException occured: ", ex);        
        return ex.getErrors();
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({FieldValidationException.class})
    @ResponseBody    
    @Override
    public ErrorResponse handleFieldValidationException(FieldValidationException ex)
    {
        logger.error("FieldValidationException occured: ", ex);        
        return ex.getErrorResponse();
    }    
 
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler({VerifisDeviceMismatchException.class})
    @ResponseBody    
    @Override
    public ResponseEntity handleForbiddenException(VerifisDeviceMismatchException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);       
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); 
    }     
    
    @ResponseStatus(HttpStatus.LOCKED)
    @ExceptionHandler({VerifisIdNotActiveException.class})
    @ResponseBody    
    @Override
    public ResponseEntity handleLockedException(RestApiException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);      
        return ResponseEntity.status(HttpStatus.LOCKED).build(); 
    }       
    
    @ResponseStatus(HttpStatus.GONE)
    @ExceptionHandler({TransferIdExpiredException.class})
    @ResponseBody    
    @Override
    public TransferObject handleTransferIdExpiredException(TransferIdExpiredException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ex.getTransferral();    }    
    
    @ResponseStatus(HttpStatus.GONE)
    @ExceptionHandler({AssociationRequestExpiredException.class})
    @ResponseBody    
    @Override
    public ResponseEntity handleAssociationRequestExpiredException(AssociationRequestExpiredException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ResponseEntity.status(HttpStatus.GONE).body(ex.getObject());
    }         
    
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({InvalidVerifisCodeException.class, InvalidTransferPinException.class, InvalidAccessTokenException.class})
    @ResponseBody    
    @Override
    public ResponseEntity handleUnauthorizedException(RestApiException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);       
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();        
    }     
    
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({InvalidTokenException.class})
    @ResponseBody    
    @Override
    public ResponseEntity handleUnprocessableEntityException(RestApiException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
    }       
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({AccessTokenMismatchException.class})
    @ResponseBody    
    @Override
    public ErrorResponse handleAccessTokenMismatchException(AccessTokenMismatchException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        ErrorResponse response = new ErrorResponse();
        response.setError(ex.getError());
        response.setParameter(ex.getParameter());
        return response;
    }       

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(VerifisIdNotFoundException.class)
    @ResponseBody    
    @Override
    public ResponseEntity handleVerifisIdNotFoundException(VerifisIdNotFoundException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(TransferIdNotFoundException.class)
    @ResponseBody    
    @Override
    public ResponseEntity handleTransferIdNotFoundException(TransferIdNotFoundException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(InvalidVerifisInstallationException.class)
    @ResponseBody    
    @Override
    public ResponseEntity handleInvalidVerifisInstallationException(InvalidVerifisInstallationException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(TokenUnavailableException.class)
    @ResponseBody    
    @Override
    public ResponseEntity handleTokenUnavailableException(TokenUnavailableException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(VerifisReportNotYetSubmittedException.class)
    @ResponseBody    
    @Override
    public ResponseEntity handleVerifisReportNotYetSubmittedException(VerifisReportNotYetSubmittedException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(RequestIdNotFoundException.class)
    @ResponseBody    
    @Override
    public ResponseEntity handleRequestIdNotFoundException(RequestIdNotFoundException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(AssociationVerifisMismatchException.class)
    @ResponseBody    
    @Override
    public ResponseEntity handleAssociationVerifisMismatchException(AssociationVerifisMismatchException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MissingParameterException.class})
    @ResponseBody    
    @Override
    public ErrorResponse handleMissingParameterException(MissingParameterException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ex.getErrorResponse();
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({InvalidVerifisCodeFormatException.class})
    @ResponseBody    
    @Override
    @Deprecated
    public ErrorResponse handleInvalidVerifisCodeFormatException(InvalidVerifisCodeFormatException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        ErrorResponse response = new ErrorResponse();
        response.setError(ex.getError());
        response.setParameter(ex.getParameter());
        return response;
    }      
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({InvalidPinFormatException.class})
    @ResponseBody    
    @Override
    @Deprecated
    public ErrorResponse handleInvalidPinFormatException(InvalidPinFormatException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        ErrorResponse response = new ErrorResponse();
        response.setError(ex.getError());
        response.setParameter(ex.getParameter());
        return response;
    }     
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({BadFormatException.class})
    @ResponseBody    
    @Override
    public ErrorResponse handleBadFormatException(BadFormatException ex)
    {        
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        ErrorResponse response = new ErrorResponse();
        response.setError(ex.getError());
        response.setParameter(ex.getParameter());
        return response;
    }        
    
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DataNotFoundException.class)
    @ResponseBody    
    @Override
    public ResponseEntity handleDataNotFoundException(DataNotFoundException ex) {
        String type  = ex.getClass().getSimpleName();        
        logger.error(type + " occured: ", ex);        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }    
    
}
