package org.verifis.api.advisor;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(annotations = {Service.class})
public class ServiceControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(ServiceControllerAdvice.class);

    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseBody
    public ResponseEntity handle(ConstraintViolationException ex) {

        Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
        violations.stream().map((violation) -> {
            logger.error("ConstraintViolationException occured: Invalid " + violation.getPropertyPath().toString());
            return violation;
        }).forEachOrdered((violation) -> {        
            logger.error("Details: " + violation.getMessage().substring(0, 1).toUpperCase() + violation.getMessage().substring(1));
        });
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }

    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    public ResponseEntity handle(MethodArgumentNotValidException ex)
    {
        logger.error("MethodArgumentNotValidException occured: " + ex.getMessage());
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }

    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler({DataIntegrityViolationException.class})
    @ResponseBody
    public ResponseEntity handle(DataIntegrityViolationException ex)
    {        
        logger.error("DataIntegrityViolationException occured: " + ex.getMessage());
        logger.error("Most specific cause: " + ex.getMostSpecificCause().getMessage());
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }

}
