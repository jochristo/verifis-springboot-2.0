package org.verifis.api.advisor;

import org.springframework.http.ResponseEntity;
import org.verifis.api.model.serializable.TransferObject;
import org.verifis.api.exception.AccessTokenMismatchException;
import org.verifis.api.exception.AssociationRequestExpiredException;
import org.verifis.api.exception.AssociationVerifisMismatchException;
import org.verifis.api.exception.BadFormatException;
import org.verifis.api.exception.DataNotFoundException;
import org.verifis.api.exception.FieldValidationException;
import org.verifis.api.exception.InvalidPinFormatException;
import org.verifis.api.exception.InvalidVerifisCodeFormatException;
import org.verifis.api.exception.InvalidVerifisInstallationException;
import org.verifis.api.exception.MissingParameterException;
import org.verifis.api.exception.RequestIdNotFoundException;
import org.verifis.api.exception.RestApiException;
import org.verifis.api.exception.TokenUnavailableException;
import org.verifis.api.exception.TransferIdExpiredException;
import org.verifis.api.exception.TransferIdNotFoundException;
import org.verifis.api.exception.VerifisDeviceMismatchException;
import org.verifis.api.exception.VerifisIdNotFoundException;
import org.verifis.api.exception.VerifisReportNotYetSubmittedException;
import org.verifis.api.model.ErrorResponse;

/**
 * Defines the exception handling methods and its return type of the RESTful Verifis API.
 * @author ic
 */
public interface IRestApiExceptionControllerAdvice
{
    public ErrorResponse handleFieldValidationException(FieldValidationException ex);        
    
    public ResponseEntity handleForbiddenException(VerifisDeviceMismatchException ex);    
    
    public ResponseEntity handleLockedException(RestApiException ex);    
    
    public TransferObject handleTransferIdExpiredException(TransferIdExpiredException ex);    
    
    public ResponseEntity handleAssociationRequestExpiredException(AssociationRequestExpiredException ex);    
    
    public ResponseEntity handleUnauthorizedException(RestApiException ex);    
    
    public ResponseEntity handleUnprocessableEntityException(RestApiException ex);    
    
    public ResponseEntity handleVerifisIdNotFoundException(VerifisIdNotFoundException ex);    
    
    public ResponseEntity handleTransferIdNotFoundException(TransferIdNotFoundException ex);    
    
    public ResponseEntity handleInvalidVerifisInstallationException(InvalidVerifisInstallationException ex);    
    
    public ResponseEntity handleTokenUnavailableException(TokenUnavailableException ex);    
    
    public ResponseEntity handleVerifisReportNotYetSubmittedException(VerifisReportNotYetSubmittedException ex);    
    
    public ResponseEntity handleRequestIdNotFoundException(RequestIdNotFoundException ex);    
    
    public ResponseEntity handleAssociationVerifisMismatchException(AssociationVerifisMismatchException ex);     
    
    public ErrorResponse handleAccessTokenMismatchException(AccessTokenMismatchException ex);
    
    public ErrorResponse handleMissingParameterException(MissingParameterException ex);
    
    @Deprecated
    public ErrorResponse handleInvalidVerifisCodeFormatException(InvalidVerifisCodeFormatException ex);
     
    @Deprecated
    public ErrorResponse handleInvalidPinFormatException(InvalidPinFormatException ex);
    
    public ErrorResponse handleBadFormatException(BadFormatException ex);
    
    public ResponseEntity handleDataNotFoundException(DataNotFoundException ex);
    
}
