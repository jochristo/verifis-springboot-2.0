package org.verifis.api.handler;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import org.verifis.api.EndPoints;
import org.verifis.api.exception.ResourceNotFoundException;

/**
 * Handler of spring boot errors.
 * @author ic
 */
@RestController
public class SpringBootErrorController implements ErrorController
{   
    @Autowired
    private ErrorAttributes errorAttributes;    

    private static final Logger logger = LoggerFactory.getLogger(SpringBootErrorController.class);    
        
    @Override
    public String getErrorPath()
    {
        return EndPoints.SPRING_BOOT_ERRORS;
    }
    
    /**
     * Return request errors.
     *
     * @param request The HTTP Request processed
     * @param includeStackTrace
     * @return	Map
     */
    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace)
    {
        //RequestAttributes requestAttributes = new ServletRequestAttributes(request);        
        //return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
        ServletWebRequest webRequest = new ServletWebRequest(request);
        return errorAttributes.getErrorAttributes(webRequest, includeStackTrace);        
    }   
    
    /**
     * Default application error request mapping: Returns always 503 service unavailable
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = EndPoints.SPRING_BOOT_ERRORS, method={RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT,RequestMethod.HEAD})
    public ResponseEntity getError(HttpServletRequest request, HttpServletResponse response)
    {
        Map<String, Object> attributes = getErrorAttributes(request, false);
        String title = (String) request.getAttribute("title");
        String details = (String) request.getAttribute("details");        
        String status = String.valueOf(attributes.get("status"));
        String message = String.valueOf(attributes.get("message"));       
        
        switch (status) {

            case "403":                                
                details = message;
                message = HttpStatus.FORBIDDEN.getReasonPhrase();
                logger.error("Application exception occured: Status: " + status + " ( " + message + " )");                
                break;

            case "404":                
                details = message;
                message = HttpStatus.NOT_FOUND.getReasonPhrase();
                logger.error("Application exception occured: Status: " + status + " ( " + message + " )");                
                throw new ResourceNotFoundException("Resource not found");                

            case "405":                
                details = message;
                message = HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase();
                logger.error("Application exception occured: Status: " + status + " ( " + message + " )");                
                break;

            case "500":
                details = message;
                message = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
                logger.error("Application exception occured: Status: " + status + " ( " + message + " )");                
                break;

            default:
                details = message;                
                logger.error("Application exception occured: Status: " + status + " ( " + message + " )");                
                break;

        }       
        
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }
  
    
}
