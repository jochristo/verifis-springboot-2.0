package org.verifis.api.handler;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.verifis.api.model.ErrorResponse;
import org.verifis.api.exception.FieldValidationException;

/**
 *
 * @author ic
 */
@Component
public class FieldValidationHandlerImpl implements IFieldValidationHandler
{
    private List<FieldError> objectErrors;
    private List<ErrorResponse> errorResponses;
    
    @Override
    public void resolveErrors(final List<FieldError> objectErrors)
    {
        this.objectErrors = objectErrors;
        this.errorResponses = new ArrayList();
        
        objectErrors.stream().forEachOrdered((item)->{
            ErrorResponse er = new ErrorResponse();
            er.setParameter(item.getField());
            er.setError(item.getDefaultMessage());
            this.errorResponses.add(er);
        });
        
        if(errorResponses.isEmpty() == false){
            throw new FieldValidationException(errorResponses.get(0)); // get first validation error
        }
    }

    @Override
    public void resolveErrors(FieldError fieldError) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public void resolveErrors(final List<FieldError> fieldErrors, final Object target)
    {
        // read object's custom annotations: VerifisCode, VerifisTransferPin, etc.
        
        this.objectErrors = objectErrors;
        this.errorResponses = new ArrayList();
        
        objectErrors.stream().forEachOrdered((item)->{
            ErrorResponse er = new ErrorResponse();
            er.setParameter(item.getField());
            er.setError(item.getDefaultMessage());
            this.errorResponses.add(er);
        });
        
        if(errorResponses.isEmpty() == false){
            throw new FieldValidationException(errorResponses.get(0)); // get first validation error
        }
    }
    
}
