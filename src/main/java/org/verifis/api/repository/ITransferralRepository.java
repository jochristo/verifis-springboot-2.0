package org.verifis.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.verifis.api.model.serializable.TransferObject;

/**
 *
 * @author ic
 */
@Repository
public interface ITransferralRepository extends MongoRepository<TransferObject, String>,  QuerydslPredicateExecutor<TransferObject>
{
    
}
