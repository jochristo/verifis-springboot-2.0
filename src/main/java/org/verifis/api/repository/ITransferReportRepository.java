package org.verifis.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.verifis.api.domain.TransferReport;

/**
 *
 * @author ic
 */
@Repository
public interface ITransferReportRepository extends MongoRepository<TransferReport, String>,  QuerydslPredicateExecutor<TransferReport>
{
    @Query(value = "{ 'transferId' : ?0, 'status' : 'SUCCESS' }")    
    public TransferReport findSuccessful(String transferId);    
    
    @Query(value = "{ 'transferId' : ?0, 'status' : 'SUCCESS' }")
    public TransferReport isSuccessful(String transferId);
    
    @Query(value = "{ 'transferId' : ?0, 'recipientDeviceId' : ?1 }")    
    public TransferReport find(String transferId, String recipientDeviceId);      
    
    
    
}
