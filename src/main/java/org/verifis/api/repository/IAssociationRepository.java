package org.verifis.api.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.verifis.api.domain.Association;
import org.verifis.api.model.enumeration.AssociationStatus;

/**
 *
 * @author ic
 */
@Repository
public interface IAssociationRepository extends MongoRepository<Association,String>, QuerydslPredicateExecutor<Association>
{
    @Override
    @Query(value = "{ 'deleted' : false, 'status' : 'ACTIVE' }")
    public List<Association> findAll();            
           
    //Override
    @Query(value = "{ 'vid' : ?0, 'deleted' : false, 'status' : 'ACTIVE' }", count = true)
    public long count(String vid);              
        
    //@Override
    @Query(value = "{ 'id' : ?0, 'status' : 'ACTIVE', 'deleted' : false }")
    public Association findOne(String id);    
        
    @Query(value = "{ 'assocId' : ?0, 'status' : 'ACTIVE', 'deleted' : false }")
    public Association findByAssocId(String assocId);     
    
    @Query(value = "{ 'status' : ?0 , 'deleted' : false }")
    public List<Association> findByStatus(AssociationStatus status);
    
    @Query(value = "{ 'vid' : ?0, 'deleted' : false , 'status' : 'ACTIVE' }")
    public List<Association> findByVid(String vid);
    
    @Query(value = "{ 'vid' : ?0, 'deleted' : false , 'status' : 'ACTIVE' }")
    public List<Association> findByVid(String vid, Pageable pageable);       
        
    @Query(value = "{ 'vid' : ?0, 'status' : ?1 , 'deleted' : false }")
    public List<Association> findByVidAndStatus(String vid, AssociationStatus status);    
    
    @Query(value = "{ 'vid' : ?0, 'assocId' : ?1, 'status' : 'ACTIVE', 'deleted' : false }")
    public Association findByVidAndAssocId(String vid, String assocId);    
    
    // for test pursposes: find with deleted and inactive flag
    @Query(value = "{ 'vid' : ?0 }")
    public List<Association> findByVidIncludeDeletedAndInactive(String vid);    
}
