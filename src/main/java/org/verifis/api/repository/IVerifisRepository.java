package org.verifis.api.repository;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.verifis.api.domain.Verifis;

/**
 *
 * @author ic
 */
@Repository
public interface IVerifisRepository extends MongoRepository<Verifis,String>, QuerydslPredicateExecutor<Verifis>
{
    @Override
    @Query(value = "{ 'deleted' : false }")
    public List<Verifis> findAll();    
    
    //@Override
    @Query(value = "{ 'id' : ?0, 'deleted' : false }")
    public Verifis findOne(String id);    
    
    @Query(value = "{ 'vid' : ?0 , 'deviceId' : ?1, 'deleted' : false }")
    public Verifis findByVidAndDeviceId(String vid,String deviceId);
    
    @Query(value = "{ 'vid' : ?0, 'deleted' : false }")
    public Verifis findByVid(String vid);
    
    @Query(value = "{ 'deviceId' : ?0, 'deleted' : false }")
    public List<Verifis> findAllByDeviceId(String deviceId);
    
    @Query(value = "{ 'vid' : ?0, 'deleted' : false , 'status' : 'ACTIVE' }")
    public Verifis findActiveByVid(String vid);
    
    @Query(value = "{ 'vid' : ?0 , 'deviceId' : ?1, 'deleted' : false, 'status' : 'ACTIVE' }")
    public Verifis findActiveByVidAndDeviceId(String vid,String deviceId);    
    
    @Query(value = "{ 'deviceId' : ?0, 'deleted' : false, 'status' : 'ACTIVE' }")
    public List<Verifis> findAllActiveByDeviceId(String deviceId);    
    
    // for test pursposes: find with deleted and inactive flag
    @Query(value = "{ 'vid' : ?0 }")
    public Verifis findByVidIncludeDeletedAndInactive(String vid);
    
    @Query(value = "{ 'deviceId' : ?0 }")
    public List<Verifis> findAllByDeviceIdIncludeDeletedAndInactive(String deviceId);    
     
}
