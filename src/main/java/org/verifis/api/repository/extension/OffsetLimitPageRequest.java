package org.verifis.api.repository.extension;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * Provides limit, offset, sort, and paging capability.
 * @author ic
 */
public class OffsetLimitPageRequest extends PageRequest
{
    private final int limit;
    private final int offset;   
    private final Sort sort;

    /*
    public OffsetLimitPageRequest(int limit, int offset, int page, int size) {
        super(page, size);
        this.limit = limit;
        this.offset = offset;
    }            
    */
    
    public OffsetLimitPageRequest(int limit, int offset, Sort sort,int page, int size) {
        super(page, size);
        this.limit = limit;
        this.offset = offset;
        this.sort = sort;
    }
    
    
    
    @Override
    public int getPageNumber() {
        return 0;
    }
    @Override
    public int getPageSize() {
        return limit;
    }
    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public Sort getSort() {
        return sort;
    }
    
    

}
