package org.verifis.api.service;

import org.springframework.http.ResponseEntity;
import org.verifis.api.model.AssociationRequestData;
import org.verifis.api.model.serializable.AssociationRequestObject;

/**
 * Defines methods to handle association requests.
 * @author ic
 */
public interface IAssociationRequestService
{
    public AssociationRequestObject create(String vid);
    
    public AssociationRequestObject verify(String requestId);
    
    public AssociationRequestObject extend(String requestId);
    
    public ResponseEntity delete(String requestId);
    
    public AssociationRequestData get(String requestId);
    
    public ResponseEntity accept(String requestId, String code);
    
    public ResponseEntity reject(String requestId, String code);    
    
}
