package org.verifis.api.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.verifis.api.model.BootResponse;
import org.verifis.api.model.TokenReport;
import org.verifis.api.model.serializable.VerifisToken;
import org.verifis.api.model.bind.BootRequestParameter;
import org.verifis.api.model.bind.DeviceIdParameter;
import org.verifis.api.model.bind.VerifisTokenParameter;

/**
 *
 * @author ic
 */
public interface IVerifisExtended
{ 
    public BootResponse initializeVerifis(BootRequestParameter request);
    
    public ResponseEntity verifyInstallation(String vid, String deviceId);
    
    public ResponseEntity resetInstallation(String vid, String deviceId);
    
    public VerifisToken initializeToken(DeviceIdParameter request);
    
    public HttpEntity<byte[]> getUnencryptedToken(String vid, VerifisTokenParameter request);
    
    public TokenReport getTokenReport(String vid);
    
    public boolean createTokenReport(String vid, String deviceId);
    
    
}
