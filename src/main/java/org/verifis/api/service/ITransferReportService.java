package org.verifis.api.service;

import org.verifis.api.domain.TransferReport;
import org.verifis.data.mongo.service.IMongoDbService;

/**
 *
 * @author ic
 */
public interface ITransferReportService extends IMongoDbService<TransferReport>
{     
    public TransferReport findSuccessful(String transferId);        
 
    public boolean isSuccessful(String transferId);    
 
    public TransferReport find(String transferId, String recipientDeviceId);    
    
}
