package org.verifis.api.service;

import java.util.List;
import org.verifis.api.domain.Verifis;
import org.verifis.data.mongo.service.IMongoDbService;

/**
 *
 * @author ic
 */
public interface IVerifisService extends IMongoDbService<Verifis>, IVerifisExtended
{   
    public Verifis find(String vid);    
        
    public List<Verifis> findAllByDeviceId(String deviceId);
    
    public List<Verifis> findAllByDeviceIdOrderedByCreation(String deviceId);
    
    public Verifis find(String vid,String deviceId);
    
    public boolean isVidAndDeviceIdMatch(String vid,String deviceId);           
    
    public Verifis findByVidExplicitly(String vid);    
    
    public Verifis findByVidAndDeviceIdExplicitly(String vid,String deviceId);        
    
    public List<Verifis> findAllByDeviceIdExplicitly(String deviceId);    
    
    public Verifis findByVidIncludeDeletedAndInactive(String vid);
    
    public List<Verifis> findAllByDeviceIdIncludeDeletedAndInactive(String deviceId);
}
