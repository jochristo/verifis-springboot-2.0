package org.verifis.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.verifis.api.model.serializable.TransferObject;
import org.verifis.api.domain.Verifis;
import org.verifis.api.exception.TransferIdExpiredException;
import org.verifis.api.exception.TransferIdNotFoundException;
import org.verifis.api.repository.ITransferralRepository;
import org.verifis.data.mongo.querydsl.PredicateOperation;
import org.verifis.data.mongo.querydsl.EntityPredicateBuilder;
import org.verifis.core.utility.Utilities;
import org.verifis.api.service.ITransferService;

/**
 *
 * @author ic
 */
@Service
public class TransferServiceImpl implements ITransferService
{
    @Autowired
    private ITransferralRepository repository;    
    
    //@Autowired 
    //private ICached<TransferItem> cachable;        
    
    @Override
    public List<TransferObject> get() {
        
        
        return repository.findAll();
    }

    @Override
    public TransferObject findOne(String id) {
        //return repository.findOne(id);
        return repository.findById(id).get();
    }

    @Override
    public TransferObject save(TransferObject object) {
        return repository.save(object);
    }

    @Override
    public TransferObject update(String id, TransferObject object) {
        //to do
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public void delete(TransferObject object) {
        repository.delete(object);
    }

    @Override
    public void insert(TransferObject object) {
        repository.insert(object);
    }

    @Override
    public TransferObject findOne(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public TransferObject findOne(String key, Object object) {
        EntityPredicateBuilder<TransferObject> predicate = new EntityPredicateBuilder(TransferObject.class);
        predicate.with(key, PredicateOperation.EQUALS, object);
        return repository.findOne(predicate.build()).get(); 
    }

    @Override
    public List<TransferObject> get(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public List<TransferObject> find(Map<Object, Object> requestMap) {
        if(requestMap.isEmpty() == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Verifis> predicate = new EntityPredicateBuilder(Verifis.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){                    
                    predicate.with((String) key, PredicateOperation.EQUALS, value);
                }                                
            }));     
            List<TransferObject> list = new ArrayList();
            Iterable<TransferObject> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null; 
    }

    @Override
    public List<TransferObject> find(Map<Object, Object> requestMap, PredicateOperation operation) {
        if(requestMap.isEmpty() == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<TransferObject> predicate = new EntityPredicateBuilder(TransferObject.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){                    
                    predicate.with((String) key, operation, value);
                }                                
            }));     
            List<TransferObject> list = new ArrayList();
            Iterable<TransferObject> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null; 
    }

    @Override
    public List<TransferObject> get(Pageable pageable) {
        return repository.findAll(pageable).getContent();
    }

    @Override
    public List<TransferObject> find(Map<Object, Object> requestMap, Pageable pageable) {
        if(requestMap.isEmpty() == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<TransferObject> predicate = new EntityPredicateBuilder(TransferObject.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){                    
                    predicate.with((String) key, PredicateOperation.EQUALS, value);
                }                                
            }));     
            List<TransferObject> list = new ArrayList();
            Iterable<TransferObject> iterable = repository.findAll(predicate.build(), pageable);
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null; 
    }

    @Override
    public TransferObject verify(String transferId)
    {        
        TransferObject transferral = findOne("transferId", transferId);
        if(transferral == null){
            throw new TransferIdNotFoundException("transferId does not exist: " + transferId);            
        }
        if(transferral.getExpires() == 0){
            throw new TransferIdExpiredException("transferId has expired: " + transferId);
        }
        return transferral;
    }

    @Override
    public TransferObject extend(String transferId) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
    
    
}
