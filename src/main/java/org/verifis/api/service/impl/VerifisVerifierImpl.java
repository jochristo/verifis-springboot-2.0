package org.verifis.api.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.verifis.api.domain.Verifis;
import org.verifis.api.exception.InvalidVerifisInstallationException;
import org.verifis.api.exception.VerifisDeviceMismatchException;
import org.verifis.api.exception.VerifisIdNotActiveException;
import org.verifis.api.exception.VerifisIdNotFoundException;
import org.verifis.api.exception.VerifisReportNotYetSubmittedException;
import org.verifis.api.model.enumeration.ActivationStatus;
import org.verifis.api.service.IVerifisService;
import org.verifis.api.service.IVerifisVerifier;

/**
 * Defines methods to verify Verifis installation such as current installation status, expiry, activation, report submission, etc.
 * @author ic
 */
@Service
public class VerifisVerifierImpl implements IVerifisVerifier
{
    @Autowired
    private IVerifisService verifisService;

    @Override
    public Verifis getExplicitly(String vid) {
        Verifis verifis = verifisService.find(vid);                
        if (verifis == null) {
            throw new VerifisIdNotFoundException("Verifis ID not found");
        }
        return verifis;
    }

    @Deprecated
    @Override
    public boolean existsInstallation(Verifis verifis, String deviceId) {
        if (!verifis.getDeviceId().equals(deviceId)) {
            throw new VerifisDeviceMismatchException("Verifis ID found and active but the deviceId does not match");
        }
        return true;
    }

    @Override
    public boolean existsInstallation(String vid, String deviceId) {
        Verifis verifis = getExplicitly(vid);  
        if (!verifis.getDeviceId().equals(deviceId)) {
            throw new VerifisDeviceMismatchException("Verifis ID found and active but the deviceId does not match");
        }
        return true;
    }    

    @Override
    public Verifis exists(String vid, String deviceId) {
        Verifis verifis = getExplicitly(vid);  
        if (!verifis.getDeviceId().equals(deviceId)) {
            throw new VerifisDeviceMismatchException("Verifis ID found and active but the deviceId does not match");
        }
        return verifis;
    }        
    
    @Override
    public boolean isActive(Verifis verifis) {
        if(verifis.getStatus() == ActivationStatus.INACTIVE){
            throw new VerifisIdNotActiveException("Verifis ID is not active");
        }
        return true;
    }

    @Override
    public boolean deactivate(String deviceId) {
        List<Verifis> list = verifisService.findAllByDeviceId(deviceId);
        list.stream().forEachOrdered((Verifis element)->{
            if(element.getStatus() == ActivationStatus.ACTIVE){            
                element.setStatus(ActivationStatus.INACTIVE);
                verifisService.update(element.getId(), element);
            }
        });
        return true;
    }
    
    @Override
    public boolean deactivate(String vid, String deviceId) {
        //List<Verifis> list = verifisService.findByDeviceId(deviceId);
        List<Verifis> list = verifisService.findAllByDeviceIdOrderedByCreation(deviceId);
        list.stream().forEachOrdered((Verifis element)->{
            if(element.getVid().equals(vid) == false && element.getStatus() == ActivationStatus.ACTIVE){            
                element.setStatus(ActivationStatus.INACTIVE);
                verifisService.update(element.getId(), element);
            }
        });
        return true;
    }    
    
    @Override
    public boolean isTokenReportSubmitted(Verifis verifis) {
        if(verifis.getStatus() == ActivationStatus.INACTIVE){
            throw new VerifisReportNotYetSubmittedException("A final report is still not submitted by the new Verifis Device");
        }
        return true;
    }    

    @Override
    public boolean isExpired(String vid) {
        Verifis verifis = this.getExplicitly(vid);
        return verifis.isExpired();
    }

    @Override
    public boolean isValidInstallation(String vid, String deviceId) {
        Verifis verifis = verifisService.find(vid, deviceId);
        if (verifis == null) {
            throw new InvalidVerifisInstallationException("The combination of Verifis Id (vid) and device id (deviceId) is not indicating a valid installation");
        }
        return true;
    }

    
}
