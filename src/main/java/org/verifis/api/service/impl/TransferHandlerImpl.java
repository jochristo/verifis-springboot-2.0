package org.verifis.api.service.impl;

import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.verifis.api.Constants;
import org.verifis.api.domain.TransferReport;
import org.verifis.api.domain.Verifis;
import org.verifis.api.exception.InvalidTransferPinException;
import org.verifis.api.exception.MissingParameterException;
import org.verifis.api.exception.TokenUnavailableException;
import org.verifis.api.exception.TransferIdExpiredException;
import org.verifis.api.exception.TransferIdNotFoundException;
import org.verifis.api.exception.VerifisDeviceMismatchException;
import org.verifis.api.exception.VerifisReportNotYetSubmittedException;
import org.verifis.api.model.TokenReport;
import org.verifis.api.model.serializable.TransferItem;
import org.verifis.api.model.serializable.TransferObject;
import org.verifis.api.model.bind.ActiveTransferParameter;
import org.verifis.api.model.bind.TransferSessionParameter;
import org.verifis.api.model.enumeration.InstallationType;
import org.verifis.api.model.enumeration.OperationStatus;
import org.verifis.api.service.ITransferReportService;
import org.verifis.api.service.IVerifisService;
import org.verifis.api.service.IVerifisVerifier;
import org.verifis.api.Constants.Cache.KeyPrefix;
import org.verifis.core.cache.impl.TransferItemCacheable;
import org.verifis.core.utility.Utilities;
import org.verifis.api.service.ITransferHandler;

/**
 *
 * @author ic
 */
@Service
public class TransferHandlerImpl implements ITransferHandler
{
    private static final Logger logger = LoggerFactory.getLogger(TransferHandlerImpl.class);         
    @Autowired private ITransferReportService transferReportService;    
    @Autowired private IVerifisService verifisService;    
    @Autowired private IVerifisVerifier verifisVerifier;        
    @Autowired private TransferItemCacheable cachable;

    @Override
    public TransferObject create(TransferSessionParameter parameter)
    {                
        String vid = parameter.getVid();
        String code = parameter.getCode();
        String deviceId = parameter.getDeviceId();
        String transferPin = parameter.getTransferPin();
        
        //To-DO: check code validity...
        
        //To-DO: check transfer pin validity...
        
        if(verifisService.findByVidAndDeviceIdExplicitly(vid, deviceId) == null) // pair not found: search only active installations
        {
            throw new VerifisDeviceMismatchException("deviceId does not match active installation's deviceId");
        }
        
        // cach transfer item
        TransferItem item = new TransferItem();
        item.setForwardingDeviceId(deviceId);        
        item.setVid(vid);
        item.setTransferPin(DigestUtils.md5DigestAsHex(transferPin.getBytes()));
        
        // store item to cache
        cachable.set(item.getTransferId(), item);
        
        //store key in cache to read its existence in following request...
        cachable.set(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, item.getTransferId(), item.getTransferId());
        
        //deliver object
        TransferObject to =  new TransferObject();
        to.setExpires(item.getExpires());
        to.setTransferId(item.getTransferId());
        return to;
    }

    @Override
    public TransferObject verify(String transferId) {
        //check existence
        String existsKey = KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,transferId);
        if(object == null){
            throw new TransferIdNotFoundException("transferId not found: " + transferId);
        }
        TransferItem item = cachable.get(transferId);
        if(item == null){
            throw new TransferIdExpiredException("transferId has expired: "+ transferId);
        }
        //deliver object
        TransferObject to =  new TransferObject();
        to.setExpires(item.getExpires());
        to.setTransferId(item.getTransferId());
        return to;
    }

    @Override
    public TransferObject extend(String transferId, ActiveTransferParameter parameter)
    {
        String code = parameter.getCode();
        String deviceId = parameter.getDeviceId();
        
        //check existence
        String existsKey = KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,transferId);
        if(object == null){
            // 404: not found
            throw new TransferIdNotFoundException("transferId not found: " + transferId);
        }        
        
        TransferItem item = cachable.get(transferId);
        if(item == null){
            // 410: gone
            throw new TransferIdExpiredException("transferId has expired: "+ transferId);
        }
        
        //403: forbidden, deviceId mismatch
        if(item.getForwardingDeviceId().equals(deviceId) == false){
            throw new VerifisDeviceMismatchException("deviceId does not much active installation's deviceId");
        }
        
        //extend expiry
        int validUntil = Utilities.currentEpochPlus(Constants.Cache.TRANSFER_EXPIRY_SECONDS);
        item.setExpires(validUntil);        
        
        // store item to cache
        cachable.set(transferId, item);    
        
        // update key existence entry too        
        cachable.set(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, item.getTransferId(), item.getTransferId());       
        logger.info("Checking expiry of transfer id existence key...... :" + cachable.isExpired(existsKey));        
        
        //deliver object
        TransferObject to =  new TransferObject();
        to.setExpires(item.getExpires());
        to.setTransferId(item.getTransferId());
        return to;
    }

    @Override
    public ResponseEntity delete(String transferId, ActiveTransferParameter parameter)
    {
        String code = parameter.getCode();
        String deviceId = parameter.getDeviceId();
        
        //check existence
        String existsKey = KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,transferId);
        if(object == null){
            // 404: not found
            throw new TransferIdNotFoundException("transferId not found: " + transferId);
        }        
        
        TransferItem item = cachable.get(transferId);
        if(item == null){
            // 410: gone
            throw new TransferIdExpiredException("transferId has expired: "+ transferId);
        }
        
        //403: forbidden, deviceId mismatch
        if(item.getForwardingDeviceId().equals(deviceId) == false){
            throw new VerifisDeviceMismatchException("deviceId does not much active installation's deviceId");
        }
        cachable.setExpired(transferId);
        cachable.setExpired(existsKey, transferId);
        return ResponseEntity.ok().build();
    }

    @Override
    public HttpEntity<byte[]> upload(String deviceId, String transferId, @NotNull byte[] data)
    {
        
        if(data ==null){
            throw new MissingParameterException("token file", "token file is required");
        }
        
        //check existence
        String existsKey = KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,transferId);
        if(object == null){
            // 404: not found
            throw new TransferIdNotFoundException("transferId not found: " + transferId);
        }        
        
        TransferItem item = cachable.get(transferId);
        if(item == null){
            // 410: gone
            throw new TransferIdExpiredException("transferId has expired: "+ transferId);
        }
        
        //403: forbidden, deviceId mismatch
        if(item.getForwardingDeviceId().equals(deviceId) == false){
            throw new VerifisDeviceMismatchException("deviceId does not much active installation's deviceId");
        }        
        
        //store token data
        item.setToken(data);
        
        // store item to cache
        cachable.set(transferId, item);         
        
        return ResponseEntity.ok().build();
    }

    @Override
    public HttpEntity<byte[]> download(String transferPin, String transferId)
    {
        //check existence
        String existsKey = KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,transferId);
        if(object == null){
            // 404: not found
            throw new TransferIdNotFoundException("transferId not found: " + transferId);
        }        
        
        TransferItem item = cachable.get(transferId);
        if(item == null){
            // 410: gone
            throw new TransferIdExpiredException("transferId has expired: "+ transferId);
        }
        
        //check transfer pin...
        String transferPinMd5 = DigestUtils.md5DigestAsHex(transferPin.getBytes());
        if(item.getTransferPin().equals(transferPinMd5) == false){
            throw new InvalidTransferPinException("Invalid transfer pin");
        }
        
        // unavailable encrypted token...
        if(item.getToken() == null){
            throw new TokenUnavailableException("Token is not available at the moment");
        }        
        
        String dummyToken = "this is my dummy encrypted token contents: ";                             
        StringBuilder sb = new StringBuilder(dummyToken);
        sb.append("transferId->").append(transferId);
        sb.append("vid->").append(item.getVid());
        sb.append("forwardingDeviceId->").append(item.getForwardingDeviceId());
        String fileName = "encrypted.file";
        //byte[] documentBody = sb.toString().getBytes();
        byte[] documentBody = item.getToken();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName.replace(" ", "_"));
        header.setContentLength(documentBody.length);
        return new HttpEntity<>(documentBody, header);                
    }

    @Override
    public ResponseEntity checkTokenAvailability(String transferId) {
        //check existence
        String existsKey = KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,transferId);
        if(object == null){
            // 404: not found
            throw new TransferIdNotFoundException("transferId not found: " + transferId);
        }        
        
        TransferItem item = cachable.get(transferId);
        if(item == null){
            // 410: gone
            throw new TransferIdExpiredException("transferId has expired: "+ transferId);
        }
        
        // unavailable encrypted token...
        if(item.getToken() == null){
            throw new TokenUnavailableException("Token is not available at the moment");
        }
        
        return ResponseEntity.ok().build();
    }

    @Override
    public TokenReport getStatus(String transferId)
    {
        //load transfer report from persistence store...
        TransferReport transferReport = transferReportService.findSuccessful(transferId);
        if(transferReport == null){
            throw new VerifisReportNotYetSubmittedException("transfer report is not yet submitted by the new Verifis Device");
        }
        
        TokenReport report = new TokenReport();
        report.setDeviceId(transferReport.getRecipientDeviceId());
        report.setStatus(transferReport.getStatus().getDescription());
        report.setVid(transferReport.getVid());
        return report;
    }

    @Override
    @Deprecated
    public HttpEntity createReport(String deviceId, String transferId)
    {
        //check existence
        String existsKey = KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,transferId);
        if(object == null){
            // 404: not found
            throw new TransferIdNotFoundException("transferId not found: " + transferId);
        }        
        
        TransferItem item = cachable.get(transferId);
        if(item == null){
            // 410: gone
            throw new TransferIdExpiredException("transferId has expired: "+ transferId);
        }
        // set new deviceId in trasnfer item in cache
        item.setRecipientDeviceId(deviceId);
        
        // TO-DO: cannot cross check existing deviceId since it is not supplied in the request
        /*
        //403: forbidden, deviceId mismatch
        if(item.getForwardingDeviceId().equals(deviceId) == false){
            throw new VerifisDeviceMismatchException("deviceId does not much active installation's deviceId");
        } 
        */
        
        // token unavailable
        if(item.getToken() == null){
            throw new TokenUnavailableException("Unauthorized access: token not available or not uploaded");
        }
                        
        // wrap all in a try statement to revert status in redis copy should mongo error occur
        try{
            
            //update verifis installation with new deviceId...
            Verifis verifis = verifisService.findByVidAndDeviceIdExplicitly(item.getVid(), item.getForwardingDeviceId());
            if(verifis == null){
                throw new VerifisDeviceMismatchException("Verifis id or deviceId not found or pair mismatch");
            }
            
            //update new device id
            verifis.setDeviceId(deviceId);
            verifis.setType(InstallationType.TRANSFER);
            verifisService.update(verifis.getId(), verifis);
            
            //verify previous installation exists and deactivate...            
            verifisVerifier.deactivate(verifis.getVid(), deviceId);
        
            // set new status
            item.setStatus(OperationStatus.SUCCESS);              
            
            //create transfer record in persistence... TO DO
            TransferReport report = new TransferReport();
            report.setForwardingDeviceId(item.getForwardingDeviceId());
            report.setRecipientDeviceId(item.getRecipientDeviceId());
            report.setStatus(OperationStatus.SUCCESS);
            report.setTransferId(item.getTransferId());
            report.setVid(item.getVid());
            transferReportService.save(report);
            
            //delete from cache...
            cachable.setExpired(transferId);
            cachable.setExpired(existsKey, transferId);            
        }
        catch(Exception ex){
            
            //change transfer status in redis...
            logger.info("Transfer report error... setting status to FAILURE...");   
            item.setStatus(OperationStatus.FAILURE);
            cachable.set(transferId, item);                         
            logger.error("Exception occured: ", ex);                       
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
        
        return ResponseEntity.ok().build();
    }

    @Override
    public TokenReport createTokenReport(String deviceId, String transferId) {
        //check existence
        String existsKey = KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,transferId);
        if(object == null){
            // 404: not found
            throw new TransferIdNotFoundException("transferId not found: " + transferId);
        }        
        
        TransferItem item = cachable.get(transferId);
        if(item == null){
            // 410: gone
            throw new TransferIdExpiredException("transferId has expired: "+ transferId);
        }
        // set new deviceId in trasnfer item in cache
        item.setRecipientDeviceId(deviceId);
        
        // TO-DO: cannot cross check existing deviceId since it is not supplied in the request
        /*
        //403: forbidden, deviceId mismatch
        if(item.getForwardingDeviceId().equals(deviceId) == false){
            throw new VerifisDeviceMismatchException("deviceId does not much active installation's deviceId");
        } 
        */
        
        // token unavailable
        if(item.getToken() == null){
            throw new TokenUnavailableException("Unauthorized access: token not available or not uploaded");
        }
                        
        // wrap all in a try statement to revert status in redis copy should mongo error occur
        try{
            
            //update verifis installation with new deviceId...
            Verifis verifis = verifisService.findByVidAndDeviceIdExplicitly(item.getVid(), item.getForwardingDeviceId());
            if(verifis == null){
                throw new VerifisDeviceMismatchException("Verifis id or deviceId not found or pair mismatch");
            }
            
            //update new device id
            verifis.setDeviceId(deviceId);
            verifis.setType(InstallationType.TRANSFER);
            verifisService.update(verifis.getId(), verifis);
            
            //verify previous installation exists and deactivate...
            //verifisVerifier.deactivate(deviceId);
            verifisVerifier.deactivate(verifis.getVid(), deviceId);
        
            // disable associations data relevant to this vid... TODO
            
            // set new status
            item.setStatus(OperationStatus.SUCCESS);              
            
            //create transfer record in persistence... TO DO
            TransferReport report = new TransferReport();
            report.setForwardingDeviceId(item.getForwardingDeviceId());
            report.setRecipientDeviceId(item.getRecipientDeviceId());
            report.setStatus(OperationStatus.SUCCESS);
            report.setTransferId(item.getTransferId());
            report.setVid(item.getVid());
            transferReportService.save(report);
            
            //delete from cache...
            cachable.setExpired(transferId);
            cachable.setExpired(existsKey, transferId);  
            
            TokenReport tokenReport = new TokenReport();
            tokenReport.setDeviceId(report.getRecipientDeviceId());
            tokenReport.setStatus(report.getStatus().getDescription());
            tokenReport.setVid(report.getVid());
            return tokenReport;            
            
        }
        catch(Exception ex){
            
            //change transfer status in redis...
            logger.info("Transfer report error... setting status to FAILURE...");   
            item.setStatus(OperationStatus.FAILURE);
            cachable.set(transferId, item);                         
            logger.error("Exception occured: ", ex);                       
            TokenReport tokenReport = new TokenReport();
            tokenReport.setDeviceId(deviceId);
            tokenReport.setStatus(OperationStatus.FAILURE.getDescription());    
            if(item != null){
                tokenReport.setVid(item.getVid());
            }
            return tokenReport;
        }
        
    }
    
}
