package org.verifis.api.service.impl;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.verifis.api.domain.TransferReport;
import org.verifis.api.repository.ITransferReportRepository;
import org.verifis.api.service.ITransferReportService;
import org.verifis.data.mongo.querydsl.PredicateOperation;

/**
 *
 * @author ic
 */
@Service
public class TransferReportServiceImpl implements ITransferReportService
{
    private static final Logger logger = LoggerFactory.getLogger(TransferReportServiceImpl.class);     
    
    @Autowired     
    private ITransferReportRepository repository;     

    @Override
    public List<TransferReport> get() {
        return repository.findAll();
    }

    @Override
    public List<TransferReport> get(Pageable pageable) {
        return repository.findAll(pageable).getContent();
    }

    @Override
    public TransferReport findOne(String id) {
        //return repository.findOne(id);
        return repository.findById(id).get();
    }

    @Override
    public TransferReport save(TransferReport object) {
        return repository.save(object);
    }

    @Override
    public TransferReport update(String id, TransferReport object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(TransferReport object) {
        repository.delete(object);
    }

    @Override
    public void insert(TransferReport object) {
        repository.save(object);
    }

    @Override
    public TransferReport findOne(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TransferReport findOne(String key, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TransferReport> get(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TransferReport> find(Map<Object, Object> requestMap) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TransferReport> find(Map<Object, Object> requestMap, Pageable pageable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TransferReport> find(Map<Object, Object> requestMap, PredicateOperation operation) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TransferReport findSuccessful(String transferId) {
        return repository.findSuccessful(transferId);
    }

    @Override
    public boolean isSuccessful(String transferId) {
        return repository.isSuccessful(transferId) != null;
    }

    @Override
    public TransferReport find(String transferId, String recipientDeviceId) {
        return repository.find(transferId, recipientDeviceId);
    }
    
}
