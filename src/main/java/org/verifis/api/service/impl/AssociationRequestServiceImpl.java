package org.verifis.api.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.verifis.api.Constants;
import org.verifis.api.component.LogoComponent;
import org.verifis.api.domain.Association;
import org.verifis.api.exception.AssociationRequestExpiredException;
import org.verifis.api.exception.RequestIdNotFoundException;
import org.verifis.api.model.AssociationProvider;
import org.verifis.api.model.AssociationRequestData;
import org.verifis.api.model.enumeration.AssociationStatus;
import org.verifis.api.model.serializable.AssociationRequestItem;
import org.verifis.api.model.serializable.AssociationRequestObject;
import org.verifis.api.service.IAssociationRequestService;
import org.verifis.api.service.IAssociationService;
import org.verifis.core.cache.impl.AssociationRequestItemCacheable;
import org.verifis.core.utility.Utilities;

/**
 * Component to enable rest endpoints to handle association request operations.
 * @author Admin
 */
@Component
@Qualifier("associationRequestComponent")
public class AssociationRequestServiceImpl implements IAssociationRequestService
{
    private static final Logger logger = LoggerFactory.getLogger(AssociationRequestServiceImpl.class);      
    @Autowired private IAssociationService service;      
    @Autowired private AssociationRequestItemCacheable cachable;
    @Autowired private LogoComponent logoComponent;    
    
    @Override
    public AssociationRequestObject create(String vid)
    {
        AssociationRequestItem item = new AssociationRequestItem();
        item.setVid(vid);
        int validUntil = Utilities.currentEpochPlus(Constants.Cache.ASSOCIATION_REQUEST_EXPIRY_SECONDS); 
        item.setExpires(validUntil);        
        
        // store actual key/object pair
        cachable.set(item.getRequestId(), item);        
        
        // just store key in order to check for its existence
        cachable.set(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId(), item.getRequestId());
        
        AssociationRequestObject aro = new AssociationRequestObject();
        aro.setRequestId(item.getRequestId());
        aro.setExpires(item.getExpires());
        return aro;
    }

    @Override
    public AssociationRequestObject verify(String requestId)
    {
        //check existence
        String existsKey = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,requestId);
        if(object == null){
            throw new RequestIdNotFoundException("requestId not found: " + requestId);
        }
        AssociationRequestItem item = cachable.get(requestId);
        if(item == null){
            AssociationRequestObject aro = new AssociationRequestObject();
            aro.setExpires(0);
            aro.setRequestId(requestId);
            throw new AssociationRequestExpiredException(aro, "requestId has expired: "+ requestId);
        }
        AssociationRequestObject aro = new AssociationRequestObject();
        aro.setRequestId(item.getRequestId());
        aro.setExpires(item.getExpires());
        return aro;
    }

    @Override
    public AssociationRequestObject extend(String requestId)
    {
        //check existence
        String existsKey = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,requestId);
        if(object == null){
            throw new RequestIdNotFoundException("requestId not found: " + requestId);
        }        
        AssociationRequestItem item = cachable.get(requestId);
        if(item == null){
            throw new AssociationRequestExpiredException("requestId has expired: "+ requestId);
        }
        int validUntil = Utilities.currentEpochPlus(Constants.Cache.ASSOCIATION_REQUEST_EXPIRY_SECONDS);
        item.setExpires(validUntil);
        cachable.set(item.getRequestId(), item);        
        
        // update key existence entry too        
        cachable.set(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId(), item.getRequestId());     
        logger.info("Checking expiry of association request id existence key...... :" + cachable.isExpired(existsKey));
                
        AssociationRequestObject aro = new AssociationRequestObject();
        aro.setRequestId(item.getRequestId());
        aro.setExpires(item.getExpires());
        return aro;
    }

    @Override
    public ResponseEntity delete(String requestId)
    {

        //check existence
        String existsKey = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,requestId);
        if(object == null){
            throw new RequestIdNotFoundException("requestId not found: " + requestId);
        }        
        AssociationRequestItem item = cachable.get(requestId);
        if(item == null){
            throw new AssociationRequestExpiredException("requestId has expired: "+ requestId);
        }
        cachable.setExpired(requestId);        
        //TO-DO: delete id_existence key in cache too...
        cachable.setExpired(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, requestId);
        return ResponseEntity.ok().build();
    }

    @Override
    public AssociationRequestData get(String requestId)
    {
        //check existence
        String existsKey = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE;
        Object object = cachable.get(existsKey,requestId);
        if(object == null){
            throw new RequestIdNotFoundException("requestId not found: " + requestId);
        }        
        AssociationRequestItem item = cachable.get(requestId);
        if(item == null){
            throw new RequestIdNotFoundException("requestId does not exist: "+ requestId);
        }
        AssociationRequestData data = new AssociationRequestData();
        
        // temporary: get random association provider
        AssociationProvider provider = logoComponent.getRandomProvider();
        data.setService(provider.getService());                       
        data.setUrl(provider.getUrl());
        data.setLogo(provider.getLogo());         
              
        return data;
    }

    @Override
    public ResponseEntity accept(String requestId, String code)
    {
        AssociationRequestItem item = cachable.get(requestId);
        if(item == null){
            throw new RequestIdNotFoundException("requestId does not exist: "+ requestId);
        }        
        // create association item to persist
        Association association = new Association();
        association.setDescription("description request id:" + item.getRequestId());
        association.setVid(item.getVid()); // set verifis ID
        association.setStatus(AssociationStatus.ACTIVE);        
        
        // temporary: get random association provider
        AssociationProvider provider = logoComponent.getRandomProvider();
        association.setService(provider.getService());                       
        association.setUrl(provider.getUrl());
        association.setLogo(provider.getLogo()); 
        
        service.save(association);
        
        //delete from cache
        //cachable.setExpired(requestId);
        //cachable.setExpired( KeyPrefix.REQUEST_ID, requestId);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity reject(String requestId, String code)
    {
        AssociationRequestItem item = cachable.get(requestId);
        if(item == null){
            throw new RequestIdNotFoundException("requestId does not exist: "+ requestId);
        }        
        Association association = new Association();
        association.setDescription("description request id: item.getRequestId()");
        association.setVid(item.getVid()); // set verifis ID
        association.setStatus(AssociationStatus.INACTIVE); // REJECTED BY CLIENT DEVICE
        
        // temporary: get random association provider
        AssociationProvider provider = logoComponent.getRandomProvider();
        association.setService(provider.getService());                       
        association.setUrl(provider.getUrl());
        association.setLogo(provider.getLogo()); 
        
        service.save(association);
        
        //delete from cache
        //cachable.setExpired(requestId);
        //cachable.setExpired( KeyPrefix.REQUEST_ID, requestId);        
        return ResponseEntity.ok().build();
    }
    
}
