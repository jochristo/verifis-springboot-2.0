package org.verifis.api.service.impl;

import com.querydsl.core.types.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.verifis.api.Constants;
import org.verifis.api.domain.QVerifis;
import org.verifis.api.domain.Verifis;
import org.verifis.api.exception.AccessTokenMismatchException;
import org.verifis.api.exception.VerifisDeviceMismatchException;
import org.verifis.api.exception.VerifisIdNotFoundException;
import org.verifis.api.model.BootResponse;
import org.verifis.api.model.TokenReport;
import org.verifis.api.model.serializable.VerifisToken;
import org.verifis.api.model.bind.BootRequestParameter;
import org.verifis.api.model.bind.DeviceIdParameter;
import org.verifis.api.model.bind.VerifisTokenParameter;
import org.verifis.api.model.serializable.UnencryptedTokenItem;
import org.verifis.api.model.enumeration.ActivationStatus;
import org.verifis.api.model.enumeration.MobilePlatform;
import org.verifis.api.model.enumeration.OperationStatus;
import org.verifis.api.repository.IVerifisRepository;
import org.verifis.api.service.IVerifisService;
import org.verifis.data.mongo.querydsl.EntityPredicateBuilder;
import org.verifis.data.mongo.querydsl.PredicateOperation;
import org.verifis.core.utility.Utilities;
import org.verifis.core.threading.IVerifisTaskScheduler;
import org.verifis.api.service.IVerifisVerifier;
import org.verifis.core.cache.impl.UnencryptedTokenItemCacheable;

/**
 *
 * @author ic
 */
@Service
public class VerifisServiceImpl implements IVerifisService
{
    private static final Logger logger = LoggerFactory.getLogger(VerifisServiceImpl.class);         
    @Autowired private IVerifisRepository repository;            
    @Autowired private IVerifisVerifier verifier;            
    @Autowired private UnencryptedTokenItemCacheable unencryptedCachable;           
    @Autowired private IVerifisTaskScheduler scheduler;

    public IVerifisRepository getRepository() {
        return repository;
    }    
    
    @Override
    public List<Verifis> get() {
        return repository.findAll();
    }

    @Override
    public Verifis findOne(String id) {
        return repository.findOne(id);
    }

    @Override
    public Verifis save(Verifis object) {
        logger.info("Saving Verifis object with id " +object.getVid() );
        return repository.save(object);
    }

    @Override
    public Verifis update(String id, Verifis object)
    {
        // load from persistence
        Verifis verifis  = this.findOne(id);
        if(verifis == null){
            throw new VerifisIdNotFoundException("Verifis id not found: " + id);
        }
        verifis.setAccessPin(object.getAccessPin());
        verifis.setDescription(object.getDescription());
        verifis.setDeviceId(object.getDeviceId());
        verifis.setExpired(object.isExpired());
        verifis.setName(object.getName());
        verifis.setPlatform(object.getPlatform());
        verifis.setUpdatedAt(new Date());
        verifis.setValidUntil(object.getValidUntil());
        verifis.setVersion(object.getVersion());
        verifis.setVid(object.getVid());       
        verifis.setStatus(object.getStatus());
        verifis.setType(object.getType());
        return repository.save(verifis);
    }

    @Override
    public void delete(Verifis object) {
        repository.delete(object);
    }

    @Override
    public void insert(Verifis object) {
        repository.save(object);
    }

    @Override
    public Verifis find(String vid) {
        return repository.findByVid(vid);
    }
        
    @Override
    public Verifis findOne(Object object) {        
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Verifis findOne(String key, Object object) {
        EntityPredicateBuilder<Verifis> predicate = new EntityPredicateBuilder(Verifis.class);
        predicate.with(key, PredicateOperation.EQUALS, object);
        return repository.findOne(predicate.build()).get(); 
    }

    @Override
    public List<Verifis> get(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public List<Verifis> find(Map<Object, Object> requestMap) {
        if(requestMap.isEmpty() == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Verifis> predicate = new EntityPredicateBuilder(Verifis.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){                    
                    predicate.with((String) key, PredicateOperation.EQUALS, value);
                }                                
            }));     
            List<Verifis> list = new ArrayList();
            Iterable<Verifis> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null; 
    }

    @Override
    public List<Verifis> find(Map<Object, Object> requestMap, PredicateOperation operation) {
        if(requestMap.isEmpty() == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Verifis> predicate = new EntityPredicateBuilder(Verifis.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){                    
                    predicate.with((String) key, operation, value);
                }                                
            }));     
            List<Verifis> list = new ArrayList();
            Iterable<Verifis> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null; 
    }

    @Override
    public List<Verifis> get(Pageable pageable) {
        return repository.findAll(pageable).getContent();
    }

    @Override
    public List<Verifis> find(Map<Object, Object> requestMap, Pageable pageable) {
        if(requestMap.isEmpty() == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Verifis> predicate = new EntityPredicateBuilder(Verifis.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){                    
                    predicate.with((String) key, PredicateOperation.EQUALS, value);
                }                                
            }));     
            List<Verifis> list = new ArrayList();
            Iterable<Verifis> iterable = repository.findAll(predicate.build(), pageable);
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null; 
    }

    @Override
    public Verifis find(String vid, String deviceId) {
        return repository.findByVidAndDeviceId(vid, deviceId);
    }

    @Override
    public boolean isVidAndDeviceIdMatch(String vid, String deviceId) {
        return repository.findByVidAndDeviceId(vid, deviceId) != null;
    }

    @Override
    public List<Verifis> findAllByDeviceId(String deviceId) {        
        return repository.findAllByDeviceId(deviceId);
    }    

    @Override
    public List<Verifis> findAllByDeviceIdOrderedByCreation(String deviceId) {
        QVerifis verifis = new QVerifis("verifis");
        Predicate predicate = verifis.deviceId.eq(deviceId).and(verifis.status.eq(ActivationStatus.ACTIVE));        
        Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
        Iterable<Verifis> iterable = repository.findAll(predicate, sort);
        List<Verifis> list = StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
        return list;
    }    
    

    @Override
    public Verifis findByVidExplicitly(String vid) {
        return repository.findActiveByVid(vid);
    }

    @Override
    public Verifis findByVidAndDeviceIdExplicitly(String vid, String deviceId) {
        return repository.findActiveByVidAndDeviceId(vid, deviceId);
    }

    @Override
    public List<Verifis> findAllByDeviceIdExplicitly(String deviceId) {
        return repository.findAllActiveByDeviceId(deviceId);
    }    

    @Override
    public Verifis findByVidIncludeDeletedAndInactive(String vid) {
        return repository.findByVidIncludeDeletedAndInactive(vid);
    }    

    @Override
    public List<Verifis> findAllByDeviceIdIncludeDeletedAndInactive(String deviceId) {
        return repository.findAllByDeviceIdIncludeDeletedAndInactive(deviceId);
    }            
        
    @Override
    public BootResponse initializeVerifis(BootRequestParameter request)
    {                
        Verifis verifis = verifier.exists(request.getVid(), request.getDeviceId());        
        verifier.isActive(verifis);                
        BootResponse br = new BootResponse();
        br.setPlatform(verifis.getPlatform().getDescription());
        br.setLastVer(verifis.getVersion());
        br.setForceUpdate(false); // TEMPORARY
        br.setStoreUrl("/dummy");
        return br;
    }

    @Override
    public VerifisToken initializeToken(DeviceIdParameter request)
    {        
        // generate access pin & validity
        String accessPin = String.valueOf(Utilities.randomPin());
        int validUntil = Utilities.currentEpochPlus(Constants.Cache.TOKEN_EXPIRY_SECONDS);        
        
        // create persistent verifis account
        Verifis verifis = new Verifis();
        verifis.setAccessPin(DigestUtils.sha1Hex(accessPin));
        verifis.setDescription("new verifis for device id: " + request.getDeviceId());
        verifis.setDeviceId(request.getDeviceId());        
        verifis.setStatus(ActivationStatus.INACTIVE);
        verifis.setPlatform(MobilePlatform.IOS); // temporary
        verifis.setValidUntil(validUntil);
        verifis.setVersion("1.0.1");            // dummy
        //repository.save(verifis);
        logger.info("Done.");      
        
        // create timer thread to set this verifis' 'isActive'/isExpired properties in mongo store as soon as it exprires...
        //scheduler.initialize(verifis.getVid());
        
        // create and store token DTO to redis                
        String verifisId = verifis.getVid();
        //UnencryptedVerifisTokenAttributes unencryptedVerifisToken = new UnencryptedVerifisTokenAttributes(accessPin, request.getDeviceId(), verifisId);
        UnencryptedTokenItem unencryptedVerifisToken = new UnencryptedTokenItem(accessPin, verifis);
        unencryptedCachable.set(verifisId, unencryptedVerifisToken);
        
        VerifisToken token = new VerifisToken();
        token.setAccessPin(accessPin);                         
        token.setValidUntil(validUntil);                
        token.setVid(verifisId);                
        //verifisTokenCachable.set(token.getVid(), token);
        
        return token;        
    }
    
    @Override
    public HttpEntity<byte[]> getUnencryptedToken(String vid, VerifisTokenParameter request)
    {        
        // dummy... to do
        String accessPin = request.getAccessToken();
        
        // check verifisId exists in cache or persistence store
        UnencryptedTokenItem unencryptedVerifisToken = unencryptedCachable.get(vid);
        if(unencryptedVerifisToken == null){
            throw new VerifisIdNotFoundException("Verifis id not found");
        }
        
        // check verifisId matches deviceId
        if(!unencryptedVerifisToken.getAccessToken().equals(accessPin)){
            throw new AccessTokenMismatchException("Invalid access token: value is not the same as supplied by the server in POST /token request");
        }         
        
        // case: load from persistence store: TO BE DECIDED UPON
        /*
        Verifis verifis = verifier.getExplicitly(vid);
        String accessPinShaHex = DigestUtils.sha1Hex(accessPin);
        if(accessPinShaHex.equals(verifis.getAccessPin()) == false){
            throw new AccessTokenMismatchException("Invalid access token: value is not the same as supplied by the server in POST /token request");
        }
        */
       
        
        /* THIS CHECK IS REDUNDANT: access token aka pin will be correct at this point
        // check this request's access token(pin) is valid: both redis key values referencing the same vid match
        VerifisToken token = verifisTokenCachable.get( vid);        
        if(token == null){
            throw new InvalidAccessTokenException("Access token is invalid");
        } 
        */
                 
        // deliver data
        // dummy: create temporarily bytes from given vid,deviceid, and accessPin
        StringBuilder sb = new StringBuilder(unencryptedVerifisToken.getVid());
        sb.append(unencryptedVerifisToken.getAccessToken()).append(unencryptedVerifisToken.getDeviceId());                
        String fileName = "unencrypted.file";
        byte[] documentBody = sb.toString().getBytes();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName.replace(" ", "_"));
        header.setContentLength(documentBody.length);
        return new HttpEntity<>(documentBody, header);
    }

    @Override
    public boolean createTokenReport(String vid, String deviceId)
    {
        // check verifisId exists in cache or persistence store
        UnencryptedTokenItem unencryptedVerifisToken = unencryptedCachable.get(vid);
        if(unencryptedVerifisToken == null){
            throw new VerifisIdNotFoundException("Verifis id not found");
        }
        
        // verify current installation
        //Verifis verifis = verifier.getExplicitly(vid);
        Verifis verifis = unencryptedVerifisToken.getVerifis();
        if (!verifis.getDeviceId().equals(deviceId)) {
            throw new VerifisDeviceMismatchException("the value of deviceId does not match the deviceId of active installation");
        }        
                        
        // check for other device/verifis pairs - deactivate if found
        //verifier.deactivate(deviceId);
        verifier.deactivate(vid, deviceId);

        //save to persistence  store
        verifis.setStatus(ActivationStatus.ACTIVE);
        repository.save(verifis);
       
        return true;
    }

    @Override
    public TokenReport getTokenReport(String vid)
    {
        // verify current installation
        Verifis verifis = verifier.getExplicitly(vid);
        verifier.isTokenReportSubmitted(verifis);
        TokenReport tr = new TokenReport();
        tr.setDeviceId(verifis.getDeviceId());
        tr.setStatus(OperationStatus.SUCCESS.getDescription());
        return tr;        
    }

    @Override
    public ResponseEntity verifyInstallation(String vid, String deviceId)
    {
        verifier.isValidInstallation(vid,deviceId);        
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity resetInstallation(String vid, String deviceId)
    {
        Verifis verifis = verifier.exists(vid, deviceId);        
        verifis.setStatus(ActivationStatus.INACTIVE);
        verifis.setExpired(false);
        verifis.setDeleted(true);
        verifis.setUpdatedAt(new Date());
        repository.save(verifis);
        return ResponseEntity.ok().build();
    }
    
    
    
}
