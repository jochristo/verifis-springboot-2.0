package org.verifis.api.service.impl;

import com.querydsl.core.types.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.verifis.api.component.LogoComponent;
import org.verifis.api.domain.Association;
import org.verifis.api.domain.QAssociation;
import org.verifis.api.exception.AssociationVerifisMismatchException;
import org.verifis.api.exception.MissingParameterException;
import org.verifis.api.exception.ResourceNotFoundException;
import org.verifis.api.exception.VerifisIdNotFoundException;
import org.verifis.api.model.Associations;
import org.verifis.api.model.bind.AssociationsRequestParameter;
import org.verifis.api.model.enumeration.AssociationStatus;
import org.verifis.api.repository.IAssociationRepository;
import org.verifis.api.repository.extension.OffsetLimitPageRequest;
import org.verifis.data.mongo.querydsl.PredicateOperation;
import org.verifis.api.service.IAssociationService;
import org.verifis.api.service.IVerifisService;
import org.verifis.core.cache.impl.AssociationRequestItemCacheable;
import org.verifis.core.utility.Utilities;
import org.verifis.data.mongo.querydsl.EntityPredicateBuilder;

/**
 * Provides operational service for Association domain objects.
 * @author ic
 */
@Service
public class AssociationServiceImpl implements IAssociationService
{
    private static final Logger logger = LoggerFactory.getLogger(AssociationServiceImpl.class);     
    
    @Autowired private IAssociationRepository repository;      
    @Autowired private AssociationRequestItemCacheable cachable;            
    @Autowired private IVerifisService verifisService;
    @Autowired private LogoComponent logoComponent;
  
    public IAssociationRepository getRepository() {
        return repository;
    }    
    
    @Override
    public List<Association> get() {        
        return repository.findAll();
    }

    @Override
    public Association findOne(String id) {
        return repository.findOne(id);
    }
    
    @Override
    public Association findSingle(String assocId) {
        return repository.findByAssocId(assocId);
    }    

    @Override
    public Association save(Association object) {
        return repository.save(object);
    }

    @Override
    public Association update(String id, Association object) {
        Association association = this.findOne(id);
        if(association == null){
            throw new ResourceNotFoundException("assocId not found: " + id);
        }
        association.setAssocDate(object.getAssocDate());
        association.setDeleted(object.isDeleted());
        association.setDescription(object.getDescription());
        association.setLogo(object.getLogo());
        association.setService(object.getService());
        association.setStatus(object.getStatus());
        association.setUrl(object.getUrl());
        association.setVid(object.getVid());
        association.setLastAuthDate(object.getLastAuthDate());
        association.setCreatedAt(object.getCreatedAt());
        // update persistence        
        association.setUpdatedAt(new Date());        
        repository.save(association);
        return association;
    }

    @Override
    public void delete(Association object) {
        repository.delete(object);
    }

    @Override
    public void insert(Association object) {
        repository.save(object);
    }

    @Override
    public Association findOne(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Association findOne(String key, Object object) {
        EntityPredicateBuilder<Association> predicate = new EntityPredicateBuilder(Association.class);
        predicate.with(key, PredicateOperation.EQUALS, object);
        return repository.findOne(predicate.build()).get(); 
    }

    @Override
    public List<Association> get(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public List<Association> find(Map<Object, Object> requestMap) {
        if(requestMap.isEmpty() == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Association> predicate = new EntityPredicateBuilder(Association.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){                    
                    predicate.with((String) key, PredicateOperation.EQUALS, value);
                }                                
            }));     
            List<Association> list = new ArrayList();
            Iterable<Association> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null; 
    }

    @Override
    public List<Association> find(Map<Object, Object> requestMap, PredicateOperation operation) {
        if(requestMap.isEmpty() == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Association> predicate = new EntityPredicateBuilder(Association.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){                    
                    predicate.with((String) key, operation, value);
                }                                
            }));     
            List<Association> list = new ArrayList();
            Iterable<Association> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null; 
    }

    @Override
    public List<Association> get(Pageable pageable) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public List<Association> find(Map<Object, Object> requestMap, Pageable pageable) {
        if(requestMap.isEmpty() == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Association> predicate = new EntityPredicateBuilder(Association.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){                    
                    predicate.with((String) key, PredicateOperation.EQUALS, value);
                }                                
            }));     
            List<Association> list = new ArrayList();
            Iterable<Association> iterable = repository.findAll(predicate.build(), pageable);
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null; 
    }
    
    @Override
    public List<Association> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Association> find(AssociationStatus status) {
        return repository.findByStatus(status);
    }

    @Override
    public List<Association> find(String vid) {
        return repository.findByVid(vid);
    }

    @Override
    public List<Association> findByVidIncludeDeletedAndInactive(String vid) {
        return repository.findByVidIncludeDeletedAndInactive(vid);
    }       

    @Override
    public List<Association> find(String vid, AssociationStatus status) {
        return repository.findByVidAndStatus(vid, status);
    }        
    
    @Override
    public Association find(String vid, String assocId) {
        
       if(verifisService.find(vid) == null){
           throw new VerifisIdNotFoundException("vid not found: " + vid);
       }
       
       boolean exists = repository.findByAssocId(assocId) != null;              
       Association association = repository.findByVidAndAssocId(vid, assocId);
       //boolean isPairFound =  repository.findByVidAndAssocId(vid, assocId) != null;
       if(!exists || association == null){ //!isPairFound
           throw new AssociationVerifisMismatchException("assocId and vid pair was not found");
       }

       return association;
    }   
    
    @Override
    public void delete(String vid, String assocId){
        Association association = repository.findByVidAndAssocId(vid, assocId);
        if(association == null){
            throw new AssociationVerifisMismatchException("assocId and vid pair was not found");
        }
        // set delete and status flags
        association.setDeleted(true);
        association.setStatus(AssociationStatus.INACTIVE);
        repository.save(association);
        //this.update(association.getId(), association);
    }
    
    @Override
    public Associations find(@NotNull String vid,  @NotNull AssociationsRequestParameter parameters)
    {
        if (vid == null) {
            throw new MissingParameterException("vid", "vid is required");
        }
        
       if(verifisService.find(vid) == null){
           throw new VerifisIdNotFoundException("vid not found: " + vid);
       }        
        
        // Page reuest with offset support
        OffsetLimitPageRequest pageable = new OffsetLimitPageRequest(1, 0, null , 0 ,1);
        
        // querydsl constructor
        QAssociation qAssociation = new QAssociation("association");
        Predicate predicate = qAssociation.deleted.eq(Boolean.FALSE)
                .and(qAssociation.status.eq(AssociationStatus.ACTIVE))
                .and(qAssociation.vid.eq(vid));

        String filter = parameters.getFilter();        
        if (filter != null)
        {            
            // get filter parameter in predicate build statement
            predicate = qAssociation.deleted.eq(Boolean.FALSE)
            .and(qAssociation.status.eq(AssociationStatus.ACTIVE))
            .and(qAssociation.vid.eq(vid))
            .and((qAssociation.service.containsIgnoreCase(filter).or(qAssociation.url.containsIgnoreCase(filter))));                              
        }        

        // Create association object        
        Associations associations = new Associations();
        associations.setLimit(-1);
        associations.setStart(-1);
        associations.setAssociations(null);
        List<Association> list = null;
        
        // define sort by creation data desc
        Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
        
        // fetch either all or with filter predicate
        if(parameters.getLimit() != null)
        {
            if (parameters.getLimit() > 0) {
                int offset = parameters.getStart();
                int size = parameters.getLimit();
                associations.setLimit(size);
                associations.setStart(offset);                            
                //pageable = new OffsetLimitPageRequest(size,offset, 0, size);                
                pageable = new OffsetLimitPageRequest(size, offset, sort, 0, size);  
                Page page = repository.findAll(predicate, pageable);
                list = page.getContent();                                    
            } 
        }
        else
        {            
            Iterable<Association> iterable = repository.findAll(predicate, sort);
            list = StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());            
            //list = (List<Association>) repository.findAll(predicate);         
            
        }
        
        // count associations with given filter
        long count = repository.count(predicate);
        associations.setTotalAssocs((int)count);
        associations.setAssociations(list);
        return associations;            
    }
    
    @Override
    public long count(Predicate predicate)
    {
        return repository.count(predicate);        
    }
    
    protected Associations findAssociations(Map<Object, Object> parameters, Pageable pageable)
    {
        // filter: service, url, etc
        // start -> integer pagination offset
        // limit-> pagination limit
        if(parameters.isEmpty() == true){
            throw new MissingParameterException("vid", "vid is required");
        }
        Map<Object,Object> queryMap = new HashMap<>();                
        if(parameters.containsKey("vid")){
            queryMap.put("vid", parameters.get("vid"));                        
        }        
        if(parameters.containsKey("service")){
            queryMap.put("service", parameters.get("service"));
        }        
        if(parameters.containsKey("url")){
            queryMap.put("url", parameters.get("url"));
        }
        if(parameters.containsKey("assocDate")){
            queryMap.put("assocDate", parameters.get("assocDate"));
        }        
        
        // add ACTIVE status and deleted=false query param values
        queryMap.put("status", AssociationStatus.ACTIVE);
        queryMap.put("deleted", false);
        
        List<Association> list;
        Associations associations = new Associations();
        if(pageable != null){
            list = this.find(queryMap, pageable);
            associations.setLimit(pageable.getPageSize());
            associations.setLimit(pageable.getPageNumber());             
        }
        else
        {
            list = this.find(queryMap);
            associations.setLimit(-1);
            associations.setLimit(-1);             
            associations.setTotalAssocs(list.size());            
        }
        
        associations.setAssociations(list); 

        return associations;
    
    }
    
    @Override
    public boolean existsAssociation(String assocId) {
        return this.findSingle(assocId) != null;                   
    }           
    
}
