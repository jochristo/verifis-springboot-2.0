package org.verifis.api.service;

import org.verifis.api.model.serializable.TransferObject;

/**
 *
 * @author ic
 */
public interface ITransferralExtended {
    
    public TransferObject verify(String transferId);
    
    public TransferObject extend(String transferId);    
}
