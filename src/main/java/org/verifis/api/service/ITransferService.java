package org.verifis.api.service;

import org.verifis.api.model.serializable.TransferObject;
import org.verifis.data.mongo.service.IMongoDbService;

/**
 *
 * @author ic
 */
public interface ITransferService extends IMongoDbService<TransferObject>, ITransferralExtended
{

}
