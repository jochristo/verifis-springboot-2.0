package org.verifis.api.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.verifis.api.model.TokenReport;
import org.verifis.api.model.serializable.TransferObject;
import org.verifis.api.model.bind.ActiveTransferParameter;
import org.verifis.api.model.bind.TransferSessionParameter;

/**
 * Defines transfer session component methods.
 * @author ic
 */
public interface ITransferHandler
{
    public TransferObject create(TransferSessionParameter parameter);
    
    public TransferObject verify(String transferId);
    
    public TransferObject extend(String transferId, ActiveTransferParameter parameter);
    
    public ResponseEntity delete(String transferId, ActiveTransferParameter parameter);
    
    public HttpEntity<byte[]> upload( String deviceId, String transferId, byte[] data);
    
    public HttpEntity<byte[]> download(String transferPin, String transferId);
    
    public HttpEntity checkTokenAvailability(String transferId);
    
    public TokenReport getStatus(String transferId);      
    
    @Deprecated
    public HttpEntity createReport(String deviceId, String transferId);
    
    public TokenReport createTokenReport(String deviceId, String transferId);
    
}
