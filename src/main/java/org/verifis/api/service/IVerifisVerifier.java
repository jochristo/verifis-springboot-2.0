package org.verifis.api.service;

import org.verifis.api.domain.Verifis;

/**
 *
 * @author ic
 */
public interface IVerifisVerifier
{
    public Verifis getExplicitly(String vid);
    
    @Deprecated
    public boolean existsInstallation(Verifis verifis, String deviceId);    
    
    public boolean existsInstallation(String vid, String deviceId);    
    
    public Verifis exists(String vid, String deviceId);    
        
    public boolean isValidInstallation(String vid, String deviceId);
    
    public boolean isActive(Verifis verifis);
    
    public boolean deactivate(String deviceId);
    
    public boolean deactivate(String vid, String deviceId);
    
    public boolean isTokenReportSubmitted(Verifis verifis);
    
    public boolean isExpired(String vid);
}
