package org.verifis.api.service;

import com.querydsl.core.types.Predicate;
import java.util.List;
import org.verifis.api.domain.Association;
import org.verifis.api.model.Associations;
import org.verifis.api.model.bind.AssociationsRequestParameter;
import org.verifis.api.model.enumeration.AssociationStatus;
import org.verifis.data.mongo.service.IMongoDbService;

/**
 *
 * @author ic
 */
public interface IAssociationService extends IMongoDbService<Association>
{    
    public List<Association> findAll();          
    
    @Override
    public Association findOne(String assocId);        
    
    public Association findSingle(String assocId);
    
    public List<Association> find(AssociationStatus status);    
    
    public List<Association> find(String vid);    
    
    public List<Association> find(String vid, AssociationStatus status);    
    
    public Association find(String vid, String assocId); 
    
    public void delete(String vid, String assocId);
    
    public Associations find(String vid, AssociationsRequestParameter parameter);
    
    public boolean existsAssociation(String assocId);
    
    public long count(Predicate predicate);
    
    public List<Association> findByVidIncludeDeletedAndInactive(String vid);
}
