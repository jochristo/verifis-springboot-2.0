package org.verifis.data.mongo.listener;

import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterLoadEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.MongoMappingEvent;
import org.verifis.api.domain.Verifis;

/**
 *
 * @author ic
 */
public class VerifisEventListener extends AbstractMongoEventListener<Verifis>
{    

    @Override
    public void onBeforeDelete(BeforeDeleteEvent<Verifis> event) {
        super.onBeforeDelete(event); 
    }

    @Override
    public void onAfterDelete(AfterDeleteEvent<Verifis> event) {
        super.onAfterDelete(event); 
    }

    @Override
    public void onAfterConvert(AfterConvertEvent<Verifis> event) {
        super.onAfterConvert(event); 
    }
        
    @Override
    public void onAfterLoad(AfterLoadEvent<Verifis> event)
    {
    }

    @Override
    public void onAfterSave(AfterSaveEvent<Verifis> event) {
        super.onAfterSave(event); 
    }

    @Override
    public void onBeforeSave(BeforeSaveEvent<Verifis> event)
    {
        
        // hash access pin before persisting
        // generate random and md5 hex hash of verifis id
        //String accessPin = event.getSource().getAccessPin();
        //String sha1Hex = DigestUtils.sha1DigestAsHex(accessPin);
        //event.getSource().setAccessPin(sha1Hex);
        //event.getSource().setVid(md5Hex(UUID.randomUUID().toString()));
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Verifis> event) {
        super.onBeforeConvert(event); 
    }

    @Override
    public void onApplicationEvent(MongoMappingEvent<?> event) {
        super.onApplicationEvent(event); 
    }
    
    
}
