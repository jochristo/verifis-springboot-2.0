package org.verifis.data.mongo.listener;

import org.springframework.data.mongodb.core.mapping.event.AfterConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterLoadEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.MongoMappingEvent;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.verifis.api.domain.Association;

/**
 *
 * @author ic
 */
public class AssociationEventListener extends AbstractMongoEventListener<Association>
{

    @Override
    public void onBeforeDelete(BeforeDeleteEvent<Association> event) {
        super.onBeforeDelete(event); 
    }

    @Override
    public void onAfterDelete(AfterDeleteEvent<Association> event) {
        super.onAfterDelete(event); 
    }

    @Override
    public void onAfterConvert(AfterConvertEvent<Association> event) {
        super.onAfterConvert(event); 
    }

    @Override
    public void onAfterLoad(AfterLoadEvent<Association> event) {
        super.onAfterLoad(event); 
    }

    @Override
    public void onAfterSave(AfterSaveEvent<Association> event) {
        super.onAfterSave(event); 
    }

    @Override
    public void onBeforeSave(BeforeSaveEvent<Association> event) {
        super.onBeforeSave(event); 
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Association> event) {
        super.onBeforeConvert(event); 
    }

    @Override
    public void onApplicationEvent(MongoMappingEvent<?> event) {
        super.onApplicationEvent(event); 
    }
    
}
