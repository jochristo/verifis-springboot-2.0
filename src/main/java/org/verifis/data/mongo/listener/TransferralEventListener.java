package org.verifis.data.mongo.listener;

import java.util.UUID;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterLoadEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.MongoMappingEvent;
import org.verifis.api.model.serializable.TransferObject;

/**
 *
 * @author ic
 */
public class TransferralEventListener extends AbstractMongoEventListener<TransferObject>
{

    @Override
    public void onBeforeDelete(BeforeDeleteEvent<TransferObject> event) {
        super.onBeforeDelete(event); 
    }

    @Override
    public void onAfterDelete(AfterDeleteEvent<TransferObject> event) {
        super.onAfterDelete(event); 
    }

    @Override
    public void onAfterConvert(AfterConvertEvent<TransferObject> event) {
        super.onAfterConvert(event); 
    }

    @Override
    public void onAfterLoad(AfterLoadEvent<TransferObject> event) {
        super.onAfterLoad(event);
    }

    @Override
    public void onAfterSave(AfterSaveEvent<TransferObject> event) {
        super.onAfterSave(event); 
    }

    @Override
    public void onBeforeSave(BeforeSaveEvent<TransferObject> event) {
        event.getSource().setTransferId(DigestUtils.md5Hex(UUID.randomUUID().toString()));
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<TransferObject> event) {
        super.onBeforeConvert(event); 
    }

    @Override
    public void onApplicationEvent(MongoMappingEvent<?> event) {
        super.onApplicationEvent(event); 
    }
    
}
