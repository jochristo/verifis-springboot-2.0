package org.verifis.data.mongo.service;

import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;
import org.verifis.data.mongo.querydsl.PredicateOperation;

/**
 * Defines methods to operate on MongoDb entities-collections.
 * @author ic
 * @param <T> The type of the collection stored.
 */
public interface IMongoDbService<T extends Object>
{
    public List<T> get();
    
    public List<T> get(Pageable pageable);
    
    public T findOne(String id);
    
    public T save(T object);
    
    public T update(String id, T object);
    
    public void delete(T object);
    
    public void insert(T object);        
        
    public T findOne(Object object);
    
    public T findOne(String key, Object object);
    
    public List<T> get(Object object);    
        
    public List<T> find(Map<Object,Object> requestMap);    
    
    public List<T> find(Map<Object,Object> requestMap, Pageable pageable);    
    
    public List<T> find(Map<Object,Object> requestMap, PredicateOperation operation);
    


}
