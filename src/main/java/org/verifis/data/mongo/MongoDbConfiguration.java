package org.verifis.data.mongo;

import com.mongodb.Mongo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoClientFactoryBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.verifis.data.mongo.listener.AssociationEventListener;
import org.verifis.data.mongo.listener.TransferralEventListener;
import org.verifis.data.mongo.listener.VerifisEventListener;

/**
 *
 * @author ic
 */
@Configuration
@EnableMongoRepositories(basePackages = "org.verifis.api.repository")
public class MongoDbConfiguration
{
    @Value("${spring.data.mongodb.database}")
    private String databaseName;    
        
    @Bean
    public MongoTemplate mongoTemplate(Mongo mongo) throws Exception {                
        return new MongoTemplate(mongoFactoryBean().getObject(), databaseName);
    }    
    
    @Bean
    public MongoClientFactoryBean mongoFactoryBean() {        
    return new MongoClientFactoryBean();
    }       
    
    @Bean
    public TransferralEventListener transferralEventListener(){
        return new TransferralEventListener();
    }
    
    @Bean
    public AssociationEventListener associationEventListener(){
        return new AssociationEventListener();
    }

    @Bean
    public VerifisEventListener verifisEvenListener(){
        return new VerifisEventListener();
    }
    
}
