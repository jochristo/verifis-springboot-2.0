package org.verifis.data.redis.service;

/**
 * Defines the caching operations on implementing redis service class.
 * Value and hash methods.
 * @author ic
 */
public interface IRedisService
{
    public void setExpiry(int exp);
    
    public void add(String key, Object value);    
    
    public void add(String key, Object value, long expiration);    
    
    public Object added(String key, Object value);        
    
    public Object added(String key, Object value, long expirationInSeconds);
    
    public void update(String key, Object value);    
    
    public Object updated(String key, Object value);      
    
    public Object updated(String key, Object value, long expirationInSeconds);
    
    boolean hasKey(String key);    
    
    public Object get(String key);
    
    public void delete(String key);
    
    String randomKey();    
    
    long getExpire(String key);        
    
    public void hAdd(String type, String key, Object value);
    
    public void hAdd(String type, String key, Object value, long expiration);
    
    public Object hAdded(String type, String key, Object value);   
    
    boolean hExistsHashKey(String type, String key);
    
    void hPutIfAbsent(String type, String key, Object value);  
    
    public void hPutIfAbsent(String type, String key, Object value, long expirationInSeconds);
    
    public Object hGet(String h, String hashKey);
}
