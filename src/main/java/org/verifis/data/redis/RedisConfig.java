package org.verifis.data.redis;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * Configuration class for redis cache service.
 * @author ic
 */
@EnableCaching
@Configuration
public class RedisConfig //extends CachingConfigurerSupport
{
    private final Logger logger = LoggerFactory.getLogger(RedisConfig.class);
    
    @Value("${spring.redis.host}")
    String redisHost;

    @Value("${spring.redis.port}")
    int redisPort;

    int redisExpiration;
    
    
    @Bean
    JedisConnectionFactory jedisConnectionFactory()
    {
        logger.info("Initializing connection to redis...");
        JedisConnectionFactory factory = new JedisConnectionFactory();
        factory.setHostName(redisHost);
        factory.setPort(redisPort);
        factory.setUsePool(true);
        logger.info("Connection to redis completed");
        return factory;
    }    
    
    @Bean(name = "redisTemplate")
    RedisTemplate<String, Object> redisTemplate()
    {
        logger.info("Creating redis template...");
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        logger.info("Setting redis key and value serializers...");
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new JsonRedisSerializer());
        logger.info("Setting connection factory...");
        redisTemplate.setConnectionFactory(jedisConnectionFactory());   
        return redisTemplate;
    }        
        
    //@Bean(name = "cacheManager")    
    public RedisCacheManager cacheManager()
    {        
        //RedisCacheManager redisManager = new RedisCacheManager(redisTemplate());                
        //logger.info("Initialized RedisCacheManager");  
        //redisManager.setDefaultExpiration(redisExpiration); //1 Day
        //logger.info("Set redis expiration " + redisExpiration);        
        //redisManager.getCacheNames().stream().forEach((item)->{logger.info("Accessing cache name: ", item);});        
        //return redisManager;     
        return null;
    }    
    
    @Bean(name = "redisObjectTemplate")
    RedisTemplate<Object, Object> redisObjectTemplate()
    {
        logger.info("Creating redis object template...");
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        logger.info("Setting redis key and value serializers...");
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new JsonRedisSerializer());
        logger.info("Setting connection factory...");
        redisTemplate.setConnectionFactory(jedisConnectionFactory());   
        return redisTemplate;
    }     
        
    // RedisSerializer
    public static class JsonRedisSerializer implements RedisSerializer<Object> {

        private final ObjectMapper om;

        public JsonRedisSerializer() {
            this.om = new ObjectMapper().enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
        }

        @Override
        public byte[] serialize(Object t) throws SerializationException {
            try {
                return om.writeValueAsBytes(t);
            } catch (JsonProcessingException e) {
                throw new SerializationException(e.getMessage(), e);
            }
        }

        @Override
        public Object deserialize(byte[] bytes) throws SerializationException {

            if (bytes == null) {
                return null;
            }

            try {
                return om.readValue(bytes, Object.class);
            } catch (Exception e) {
                throw new SerializationException(e.getMessage(), e);
            }
        }
    }       
    
}
