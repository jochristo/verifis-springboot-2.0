package org.verifis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Starting point of Verifis Spring Boot application.
 * @author ic
 */
@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
@Configuration
//@EnableScheduling
public class VerifisRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(VerifisRestApplication.class, args);
	}
}
