package org.verifis.configuration;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ErrorsAttributesConfiguration {

    @Bean
    public ErrorAttributes errorAttributes() {
        
        
        //return new DefaultErrorAttributes(true);
        
        DefaultErrorAttributes attributes = new DefaultErrorAttributes(true);        
        return attributes;
        
        /*
        return new DefaultErrorAttributes() {
            @Override
            public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {
                Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace);
                Throwable error = getError(requestAttributes);
                if (error instanceof RestApiException) {
                    errorAttributes.put("title", ((RestApiException) error).getTitle());
                    errorAttributes.put("status", ((RestApiException) error).getHttpStatus());
                    errorAttributes.put("appCode", ((RestApiException) error).getAppCode());
                    errorAttributes.put("details", ((RestApiException) error).getDetails());
                    errorAttributes.put("message", ((RestApiException) error).getMessage());
                }
                return errorAttributes;
            }
        };
        */
    }
}
