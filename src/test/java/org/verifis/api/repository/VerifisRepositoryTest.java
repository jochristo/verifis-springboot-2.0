package org.verifis.api.repository;

import java.util.List;
import java.util.UUID;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.verifis.api.domain.Verifis;
import org.verifis.api.model.enumeration.ActivationStatus;
import static org.verifis.api.model.enumeration.ActivationStatus.ACTIVE;
import static org.verifis.api.model.enumeration.ActivationStatus.INACTIVE;
import org.verifis.api.model.enumeration.MobilePlatform;
import org.verifis.core.utility.Utilities;

/**
 *
 * @author ic
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:app-integration-test.properties")
public class VerifisRepositoryTest
{
    private static final Logger logger = LoggerFactory.getLogger(VerifisRepositoryTest.class); 
    @Autowired private IVerifisRepository repository;
    
    private final Verifis verifisOne;
    private final Verifis verifisTwo;
    private final String deviceOne = UUID.randomUUID().toString(); //"08c76d9c";
    private final String deviceTwo = UUID.randomUUID().toString(); //"c9719dd4";

    public VerifisRepositoryTest()
    {                
        verifisOne = new Verifis();
        verifisTwo = new Verifis();
        
        // init verifis
        initVerifisData();
    }
    
    private void initVerifisData()
    {     
        logger.info("Initializing new verifis id#" + verifisOne.getVid() + " with deviceId: " + deviceOne);
        verifisOne.setAccessPin("1234");
        verifisOne.setDescription("description: " + deviceOne);
        verifisOne.setDeviceId(deviceOne);
        verifisOne.setExpired(false);
        verifisOne.setName(deviceOne);
        verifisOne.setPlatform(MobilePlatform.IOS);
        verifisOne.setStatus(ActivationStatus.ACTIVE);
        verifisOne.setValidUntil(Utilities.currentEpochPlus(3600));
        verifisOne.setVersion("1.0.1");     
        
        logger.info("Initializing new verifis id#" + verifisTwo.getVid() + " with deviceId: " + deviceTwo);
        verifisTwo.setAccessPin("1111");
        verifisTwo.setDescription("description: " + deviceTwo);
        verifisTwo.setDeviceId(deviceTwo);
        verifisTwo.setExpired(false);
        verifisTwo.setName(deviceTwo);
        verifisTwo.setPlatform(MobilePlatform.ANDROID);
        verifisTwo.setStatus(ActivationStatus.ACTIVE);
        verifisTwo.setValidUntil(Utilities.currentEpochPlus(3600));
        verifisTwo.setVersion("1.1.1");                 
    }
    
    @Before
    public void before()
    {
        assertNotNull(repository);
        initVerifisData();        
    }
    
    @After
    public void after()
    {
        //repository.delete(verifisOne);
        //repository.delete(verifisTwo);
    }
    
    @Test
    public void save()
    {
        logger.info("Testing repository save...");
        Verifis v1 = repository.save(verifisOne);
        assertNotNull(v1);
        assertEquals(deviceOne, v1.getDeviceId());       
        
        //clean up
        repository.delete(verifisOne);
    }
    
    @Test
    public void delete()
    {
        logger.info("Testing repository delete...");
        Verifis v2 = repository.save(verifisTwo);
        String vid = v2.getVid();
        assertNotNull(vid);
        repository.delete(v2);
        assertNull(repository.findOne(vid));
    }    
    
    @Test
    public void findAll()
    {
        logger.info("Testing repository findAll...");
        Verifis v1 = repository.save(verifisOne);
        Verifis v2 = repository.save(verifisTwo);        
        
        List<Verifis> list = repository.findAll();
        assertNotNull(list);
        
        /*
        assertEquals(2, list.size());
        list.stream().forEach((v)->{
            assertEquals(false, v.isDeleted());
            assertTrue(v.getDeviceId().equals(deviceOne) || v.getDeviceId().equals(deviceTwo));
        });
        */
        // clean up
        repository.delete(verifisOne);        
        repository.delete(verifisTwo);        
    }     
    
    @Test
    public void findOne()
    {
        logger.info("Testing repository findOne...");
        Verifis v1 = repository.save(verifisOne);
        Verifis vv1 = repository.findOne(v1.getId());
        assertNotNull(vv1);
        assertEquals(deviceOne, vv1.getDeviceId());
        assertEquals(v1, vv1);
        assertEquals(false, vv1.isDeleted());
        
        // clean up
        repository.delete(verifisOne);        
    }      
    
    
    @Test
    public void findByVidAndDeviceId()
    {
        logger.info("Testing repository findByVidAndDeviceId...");
        Verifis v1 = repository.save(verifisOne);
        Verifis v2 = repository.save(verifisTwo);        
        
        Verifis vv1 = repository.findByVidAndDeviceId(v1.getVid(), deviceOne);
        assertNotNull(vv1);
        assertEquals(deviceOne, vv1.getDeviceId());
        assertEquals(false, vv1.isDeleted());
        
        Verifis vv2 = repository.findByVidAndDeviceId(v2.getVid(), deviceTwo);
        assertNotNull(vv1);
        assertEquals(deviceTwo, vv2.getDeviceId());
        assertEquals(false, vv2.isDeleted());        
        
        // clean up
        repository.delete(verifisOne);
        repository.delete(verifisTwo);        
    }         
    
    @Test
    public void findByVid()
    {
        logger.info("Testing repository findByVid...");
        Verifis v1 = repository.save(verifisOne);
        Verifis vv1 = repository.findByVid(v1.getVid());
        assertNotNull(vv1);
        assertEquals(deviceOne, vv1.getDeviceId());
        assertEquals(v1.getVid(), vv1.getVid());
        assertEquals(v1, vv1);
        assertEquals(false, vv1.isDeleted());
        
        // clean up
        repository.delete(verifisOne);        
    }      
        
    @Test
    public void findAllByDeviceId()
    {
        logger.info("Testing repository findAllByDeviceId...");
        repository.save(verifisOne);
        verifisTwo.setDeviceId(deviceOne);
        repository.save(verifisTwo);        
        
        List<Verifis> list = repository.findAllByDeviceId(deviceOne);
        assertNotNull(list);
        /*
        assertEquals(2, list.size());
        list.stream().forEach((v)->{
            assertEquals(false, v.isDeleted());
            assertTrue(v.getDeviceId().equals(deviceOne));
        });     
        */
        // clean up
        repository.delete(verifisOne);
        repository.delete(verifisTwo);        
    }             
    
    @Test
    public void findActiveByVid()
    {
        logger.info("Testing repository findActiveByVid...");
        Verifis v1 = repository.save(verifisOne);
        Verifis vv1 = repository.findActiveByVid(v1.getVid());
        assertNotNull(vv1);
        assertEquals(deviceOne, vv1.getDeviceId());
        assertEquals(v1.getVid(), vv1.getVid());
        assertEquals(v1, vv1);
        assertEquals(ACTIVE, vv1.getStatus());
        
        // clean up
        repository.delete(verifisOne);        
    }     
    
    @Test
    public void findActiveByVidAndDeviceId()
    {
        logger.info("Testing repository findActiveByVidAndDeviceId...");
        Verifis v1 = repository.save(verifisOne);
        Verifis vv1 = repository.findActiveByVidAndDeviceId(v1.getVid(), deviceOne);
        assertNotNull(vv1);
        assertEquals(deviceOne, vv1.getDeviceId());
        assertEquals(v1.getVid(), vv1.getVid());
        assertEquals(v1, vv1);
        assertEquals(ACTIVE, vv1.getStatus());
        
        // clean up
        repository.delete(verifisOne);        
    }      
    
    @Test
    public void findAllActiveByDeviceId()
    {
        logger.info("Testing repository findAllActiveByDeviceId...");
        repository.save(verifisOne);
        verifisTwo.setDeviceId(deviceOne);
        repository.save(verifisTwo);        
        
        List<Verifis> list = repository.findAllActiveByDeviceId(deviceOne);
        assertNotNull(list);
        assertEquals(2, list.size());
        list.stream().forEach((Verifis v)->{
            assertEquals(false, v.isDeleted());
            assertTrue(v.getDeviceId().equals(deviceOne));
            assertEquals(ACTIVE, v.getStatus());
        });     
        
        // clean up
        repository.delete(verifisOne);
        repository.delete(verifisTwo);        
    }     
        
    @Test
    public void findByVidIncludeDeletedAndInactive()
    {
        logger.info("Testing repository findByVidIncludeDeletedAndInactive...");
        Verifis v1 = repository.save(verifisOne);
        verifisTwo.setDeviceId(deviceOne);
        verifisTwo.setDeleted(false);
        verifisTwo.setStatus(ActivationStatus.INACTIVE);
        Verifis v2 = repository.save(verifisTwo);        
        
        Verifis vv1 = repository.findByVidIncludeDeletedAndInactive(v1.getVid());
        assertNotNull(vv1);
        assertEquals(false, vv1.isDeleted());
        assertEquals(ActivationStatus.ACTIVE, vv1.getStatus());
        
        Verifis vv2 = repository.findByVidIncludeDeletedAndInactive(v2.getVid());
        assertNotNull(vv2);    
        assertEquals(false, vv2.isDeleted());
        assertEquals(ActivationStatus.INACTIVE, vv2.getStatus());
        
        // clean up
        repository.delete(verifisOne);
        repository.delete(verifisTwo);        
    }        
    
    @Test
    public void findAllByDeviceIdIncludeDeletedAndInactive()
    {
        logger.info("Testing repository findAllByDeviceIdIncludeDeletedAndInactive...");
        verifisOne.setDeleted(true);        
        repository.save(verifisOne);
        verifisTwo.setDeviceId(deviceOne);        
        verifisTwo.setStatus(ActivationStatus.INACTIVE);
        repository.save(verifisTwo);        
        
        List<Verifis> list = repository.findAllByDeviceIdIncludeDeletedAndInactive(deviceOne);
        assertNotNull(list);
        assertEquals(2, list.size());
        list.stream().forEach((Verifis v)->
        {
            assertTrue(v.getDeviceId().equals(deviceOne));
            assertTrue(v.getVid().equals(verifisOne.getVid()) || v.getVid().equals(verifisTwo.getVid()));
            
            if(v.getVid().equals(verifisOne.getVid())){
                assertEquals(true, v.isDeleted());
                assertTrue(v.getDeviceId().equals(deviceOne));
                assertEquals(ACTIVE, v.getStatus());                
            }
            else if(v.getVid().equals(verifisTwo.getVid())){
                assertEquals(false, v.isDeleted());
                assertTrue(v.getDeviceId().equals(deviceOne));
                assertEquals(INACTIVE, v.getStatus());                
            }            

        });     
        
        // clean up
        repository.delete(verifisOne);
        repository.delete(verifisTwo);        
    }      
}
