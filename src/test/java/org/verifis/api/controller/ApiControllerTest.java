package org.verifis.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mongodb.morphia.utils.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.verifis.VerifisRestApplication;
import org.verifis.api.service.IVerifisService;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.verifis.api.domain.Verifis;
import org.verifis.api.model.serializable.AssociationRequestObject;
import org.verifis.api.model.serializable.VerifisToken;
import org.verifis.api.model.enumeration.ActivationStatus;
import org.verifis.api.model.enumeration.MobilePlatform;
import org.verifis.core.cache.impl.AssociationRequestItemCacheable;
import org.verifis.core.utility.Utilities;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.verifis.api.domain.Association;
import org.verifis.api.model.AssociationRequestData;
import org.verifis.api.model.Associations;
import org.verifis.api.model.BootResponse;
import org.verifis.api.model.serializable.TransferObject;
import org.verifis.api.model.enumeration.AssociationStatus;
import org.verifis.api.model.enumeration.InstallationType;
import org.verifis.api.service.IAssociationService;
import org.verifis.api.service.ITransferReportService;
import org.verifis.core.cache.impl.TransferItemCacheable;

/**
 *
 * @author ic
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = VerifisRestApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:app-integration-test.properties")
public class ApiControllerTest {

    @Autowired private MockMvc mvc;
    @Autowired private IVerifisService verifisService;
    @Autowired private IAssociationService associationService; 
    @Autowired private ITransferReportService transferReportService;
    @Autowired private AssociationRequestItemCacheable aric;    
    @Autowired private TransferItemCacheable tic;    
    private static final Logger logger = LoggerFactory.getLogger(ApiControllerTest.class); 
    
    protected final static String myFirstDeviceId = "54bc36";
    protected final static String mySecondDeviceId = "30cf10";
    protected final static String myThirdDeviceId = "44de96";

    @Before
    public void initData() {
        // create verifis
        Verifis verifis = new Verifis();
        verifis.setAccessPin("1234");
        verifis.setDescription("description");
        verifis.setDeviceId("some device");
        verifis.setExpired(false);
        verifis.setName("name");
        verifis.setPlatform(MobilePlatform.IOS);
        verifis.setStatus(ActivationStatus.ACTIVE);
        verifis.setValidUntil(Utilities.currentEpochPlus(3600));
        verifis.setVersion("1.0.1");
        verifisService.save(verifis);
    }

    @After
    public void afterTest() {
        List<Verifis> verifis = verifisService.findAllByDeviceIdIncludeDeletedAndInactive("some device");
        if (verifis != null) {
            verifis.forEach((item) -> {
                verifisService.delete(item);
            });
        }
    }
    
    protected void createVerifis(String deviceId) {
        // create verifis
        Verifis verifis = new Verifis();
        verifis.setAccessPin("1234");
        verifis.setDescription("description");
        verifis.setDeviceId(deviceId);
        verifis.setExpired(false);
        verifis.setName(deviceId);
        verifis.setPlatform(MobilePlatform.IOS);
        verifis.setStatus(ActivationStatus.ACTIVE);
        verifis.setValidUntil(Utilities.currentEpochPlus(3600));
        verifis.setVersion("2.0.1");
        verifisService.save(verifis);
    }
    
    protected void cleanUpVerifis(String deviceId) {
        List<Verifis> verifis = verifisService.findAllByDeviceIdIncludeDeletedAndInactive(deviceId);
        if (verifis != null) {
            verifis.forEach((item) -> {
                verifisService.delete(item);
            });
        }
    }    
    
    protected void cleanUpAssociation(String vid, AssociationStatus status) {
        List<Association> associations = associationService.findByVidIncludeDeletedAndInactive(vid);        
        if (associations != null) {
            associations.forEach((item) -> {
                associationService.delete(item);
            });
        }
    }     

    @Test
    public void testApiErrorCodes200() throws Exception {
        mvc.perform(get("/codes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    // TEST /boot requests - start
    
    @Test
    public void testBoot200Ok() throws Exception {
        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);                
        
        MvcResult result = mvc.perform(get("/boot").param("vid", verifis.getVid()).param("platform", verifis.getPlatform().getDescription())
            .param("ver", verifis.getVersion()).param("deviceId", verifis.getDeviceId())
                .contentType(MediaType.APPLICATION_JSON))                
                .andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        BootResponse br = mapper.readValue(content, BootResponse.class);
        assertNotNull(br);         
        cleanUpVerifis(myFirstDeviceId);
    }    
    
    @Test
    public void testBoot400MissingParameters() throws Exception {
        mvc.perform(get("/boot").param("vid", "vid").param("platform", "ios").param("ver", "1.0.1")
                .contentType(MediaType.APPLICATION_JSON))                
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"parameter\":\"deviceId\",\"error\":\"deviceId is required\"}"));
    }

    @Test
    public void testBoot400InvalidPlatform() throws Exception {
        mvc.perform(get("/boot").param("vid", "vid").param("platform", "error").param("ver", "1.0.1").param("deviceId", "deviceId")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"parameter\":\"platform\",\"error\":\"INVALID_FORMAT: The format of 'platform' parameter is invalid. Valid values are: ios or android.\"}"));                
    }
    
    @Test
    public void testBoot404VidNotFound() throws Exception {
        mvc.perform(get("/boot").param("vid", "notfound").param("platform", "ios").param("ver", "1.0.1").param("deviceId", "deviceId")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());                
    }    
    
    @Test
    public void testBoot403VidDeviceMismatch() throws Exception {
        String vid = verifisService.findAllByDeviceIdExplicitly("some device").get(0).getVid();
        mvc.perform(get("/boot").param("vid", vid).param("platform", "ios").param("ver", "1.0.1").param("deviceId", "deviceId")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());                
    }     
    
    @Test
    public void testBoot423LockedVidInactive() throws Exception {
        // create verifis
        Verifis verifis = new Verifis();
        verifis.setAccessPin("1234");
        verifis.setDescription("description");
        verifis.setDeviceId("deviceId123");
        verifis.setExpired(false);
        verifis.setName("name");
        verifis.setPlatform(MobilePlatform.IOS);        
        verifis.setValidUntil(Utilities.currentEpochPlus(3600));
        verifis.setVersion("1.0.1");
        verifis.setStatus(ActivationStatus.INACTIVE);
        verifisService.save(verifis);        
        String vid = verifis.getVid();
        mvc.perform(get("/boot").param("vid", vid).param("platform", "ios").param("ver", "1.0.1").param("deviceId", "deviceId123")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isLocked());              
        verifisService.delete(verifis);
    }         
    // TEST /boot requests - end
    
    // TEST /token requests - start    
    @Test
    public void testinitializeToken400MissingDeviceId() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/token")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().json("{\"parameter\":\"deviceId\",\"error\":\"deviceId is required\"}"));
    }    
    
    @Test
    public void testCreatNewVerifisToken200Ok() throws Exception {
        // initialize token request
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/token").param("deviceId", "qe12qe12")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        VerifisToken token = mapper.readValue(content, VerifisToken.class);                
        Assert.isNotNull(token);
        String vid = token.getVid();
        Assert.isNotNull(vid);
        Assert.isNotNull(token.getAccessPin()); 
        // download token request
        mvc.perform(get("/token/"+ vid + "/download")
                .param("accessToken", token.getAccessPin()).param("deviceId", "qe12qe12").param("vid", vid)
                .contentType(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(status().isOk());   
        // final token report request        
        mvc.perform(MockMvcRequestBuilders.post("/token/"+ vid + "/report").param("deviceId", "qe12qe12")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk());     
        // token report status request        
        mvc.perform(get("/token/"+ vid + "/report")                
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json("{\"status\":\"SUCCESS\",\"deviceId\":\"qe12qe12\"}"));    
        //verify verifis installation request
        mvc.perform(get("/token/" + vid).param("deviceId", "qe12qe12"))
            .andExpect(status().isOk());
        
        //clean
        Verifis verifis = verifisService.find(vid, "qe12qe12");
        Assert.isNotNull(verifis);
        Assert.isEqual(vid, verifis.getVid());
        Assert.isEqual("qe12qe12", verifis.getDeviceId());
        verifisService.delete(verifis);
    }     
    
    @Test
    public void testCreatNewVerifisToken400MissingDeviceId() throws Exception {
        String vid = verifisService.findAllByDeviceIdExplicitly("some device").get(0).getVid();
        mvc.perform(get("/token/" + vid).param("deviceId", "some device"))                
                .andExpect(status().isOk());        
    }
    
    @Test
    public void testDownloadUnencrVerifisToken404VerifisIdNotFound() throws Exception {
        String vid = "vid"; //verifisService.findAllByDeviceIdExplicitly("some device").get(0).getVid();
        mvc.perform(get("/token/" + vid + "/download")
                .param("deviceId", "some device").param("accessToken", "1234"))
                .andExpect(status().isNotFound());        
    }
    
    @Test
    public void testDownloadUnencrVerifisToken400InvalidAccessTokenFormat() throws Exception {
        // initialize token request
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/token").param("deviceId", "qe12qe12")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        VerifisToken token = mapper.readValue(content, VerifisToken.class);                
        Assert.isNotNull(token);
        String vid = token.getVid();
        Assert.isNotNull(vid);
        Assert.isNotNull(token.getAccessPin()); 
        // download token request
        mvc.perform(get("/token/"+ vid + "/download")
                .param("accessToken", "invalidToken").param("deviceId", "qe12qe12").param("vid", vid)
                .contentType(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"parameter\":\"accessToken\",\"error\":\"INVALID_FORMAT: The format of 'accessToken' parameter is invalid. Allowed rangeis 1000-9999\"}")); 
    }    
    
    @Test
    public void testDownloadUnencrVerifisToken400AccessTokenMismatch() throws Exception {
        // initialize token request
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/token").param("deviceId", "qe12qe12")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        VerifisToken token = mapper.readValue(content, VerifisToken.class);                
        Assert.isNotNull(token);
        String vid = token.getVid();
        Assert.isNotNull(vid);
        Assert.isNotNull(token.getAccessPin()); 
        // download token request
        mvc.perform(get("/token/"+ vid + "/download")
                .param("accessToken", "1111").param("deviceId", "qe12qe12").param("vid", vid)
                .contentType(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"parameter\":\"accessToken\",\"error\":\"Invalid access token: value is not the same as supplied by the server in POST /token request\"}")); 
    }    
            
    @Test
    public void testGetTokenReport404NotFound() throws Exception {        
         mvc.perform(MockMvcRequestBuilders.get("/token/notfoundvid/report")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }     
    
    @Test
    public void testCreateTokenReport400MissingDeviceId() throws Exception {        
        mvc.perform(MockMvcRequestBuilders.post("/token/vid/report")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().json("{\"parameter\":\"deviceId\",\"error\":\"deviceId is required\"}"));
    }     
    
    @Test
    public void testCreateTokenReport404NotFound() throws Exception {
         mvc.perform(MockMvcRequestBuilders.post("/token/notfoundvid/report").param("deviceId", "qe12qe12")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }           
    
    
    @Test
    public void testCreateTokenReport403Forbidden() throws Exception {
        // initialize token request
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/token").param("deviceId", "qe12qe12")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        VerifisToken token = mapper.readValue(content, VerifisToken.class);                
        Assert.isNotNull(token);
        String vid = token.getVid();
        Assert.isNotNull(vid);
        Assert.isNotNull(token.getAccessPin());         
        mvc.perform(MockMvcRequestBuilders.post("/token/" + vid + "/report").param("deviceId", "error"))
            .andExpect(status().isForbidden());
    }                 
        
    
    @Test
    public void testverifyVerifisInstallation200OK() throws Exception {
        String vid = verifisService.findAllByDeviceIdExplicitly("some device").get(0).getVid();
        mvc.perform(MockMvcRequestBuilders.get("/token/" + vid).param("deviceId", "some device"))                
                .andExpect(status().isOk());
    }        
    
    @Test
    public void testverifyVerifisInstallation400MissingParameters() throws Exception {
        mvc.perform(get("/token/someTokenId")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().json("{\"parameter\":\"deviceId\",\"error\":\"deviceId is required\"}"));
    }

    @Test
    public void testverifyVerifisInstallation404DeviceIdNotFound() throws Exception {
        mvc.perform(get("/token/unknownTokenId").param("deviceId", "somedeviceId")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(content().string(""))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testresetVerifisInstallation400MissingParameters() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/token/someTokenId").param("deviceId", "deviceId")
                .contentType(MediaType.APPLICATION_JSON))                
                .andExpect(status().isBadRequest())                
                .andExpect(content().json("{\"parameter\":\"code\",\"error\":\"code is required\"}"));
    }

    @Test
    public void testresetVerifisInstallation400MissingDeviceIdParameter() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/token/someTokenId").param("code", "123456")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().json("{\"parameter\":\"deviceId\",\"error\":\"deviceId is required\"}"));
    }

    @Test
    public void testresetVerifisInstallation400InvalidCodeFormat() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/token/someTokenId").param("deviceId", "somedeviceId").param("code", "code")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testresetVerifisInstallation404VidNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/token/someTokenId").param("deviceId", "somedeviceId").param("code", "123456")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(content().string(""))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testresetVerifisInstallation403DeviceIdMismatch() throws Exception {
        String vid = verifisService.findAllByDeviceId("some device").get(0).getVid();
        mvc.perform(MockMvcRequestBuilders.delete("/token/" + vid).param("deviceId", "somedeviceId").param("code", "123456")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(content().string(""))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testresetVerifisInstallation200OK() throws Exception {
        String vid = verifisService.findAllByDeviceId("some device").get(0).getVid();
        mvc.perform(MockMvcRequestBuilders.delete("/token/" + vid).param("deviceId", "some device").param("code", "123456")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(content().string(""))
                .andExpect(status().isOk());
        Verifis verifis = verifisService.findByVidIncludeDeletedAndInactive(vid);
        Assert.isTrue(verifis != null);
        Assert.isEqual(verifis.getVid(), vid);
        Assert.isEqual(verifis.isDeleted(), true);
    }

    // TEST /token requests - end
    
    // TEST /association requests - start
    
    @Test
    public void testCreateAssociationRequest200OK() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());            
        this.cleanUpVerifis(myFirstDeviceId);
    }
    
    @Test
    public void testCreateAssociationRequest400InvalidCodeFormat() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "error")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().json("{\"parameter\":\"code\",\"error\":\"INVALID_FORMAT: The format of 'code' parameter is invalid. Please use 6-digit code.\"}"));
        this.cleanUpVerifis(myFirstDeviceId);
    }    
    
    @Test
    public void testCreateAssociationRequest404VidNotFound() throws Exception {                
        mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", "notfound").param("code", "123456")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());        
    }      
    
    @Test
    public void testVerifyAssociationRequest200OK() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        //make the request
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        // verify request id exists
        MvcResult verify = mvc.perform(MockMvcRequestBuilders.get("/association/" + requestObject.getRequestId())
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();           
        content = verify.getResponse().getContentAsString();
        AssociationRequestObject verifyObject = mapper.readValue(content, AssociationRequestObject.class); 
        Assert.isEqual(requestObject.getRequestId(), verifyObject.getRequestId());
        Assert.isEqual(requestObject.getExpires(), verifyObject.getExpires());
        this.cleanUpVerifis(myFirstDeviceId);
    }    
    
    @Test
    public void testVerifyAssociationRequest404NotFound() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        //make the request
        mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
        
        // verify request id exists
        mvc.perform(MockMvcRequestBuilders.get("/association/" + "notfound")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
        this.cleanUpVerifis(myFirstDeviceId);
    } 
    
    @Test
    public void testVerifyAssociationRequest410Gone() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        //make the request
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        
        //expire request id from cache manually
        aric.setExpired(requestObject.getRequestId());
        
        // verify request id exists
        MvcResult verify = mvc.perform(MockMvcRequestBuilders.get("/association/" + requestObject.getRequestId())
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isGone()).andReturn();           
        content = verify.getResponse().getContentAsString();
        AssociationRequestObject verifyObject = mapper.readValue(content, AssociationRequestObject.class); 
        Assert.isEqual(requestObject.getRequestId(), verifyObject.getRequestId());
        Assert.isEqual(0, verifyObject.getExpires());
        this.cleanUpVerifis(myFirstDeviceId);
    }     
    
    @Test
    public void testExtendAssociationRequest200OK() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());            
        
        // simulate delay in requests
        logger.info("Simulating extending association request extension...");
        Thread.sleep(3000);
        logger.info("Simulating extending association request extension... done.");
        
        //perform extend request
        MvcResult extend = mvc.perform(MockMvcRequestBuilders.put("/association/" + requestObject.getRequestId()).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = extend.getResponse().getContentAsString();        
        AssociationRequestObject extendObject = mapper.readValue(content, AssociationRequestObject.class); 
        Assert.isNotNull(extendObject);        
        Assert.isNotNull(extendObject.getRequestId());        
        Assert.isNotNull(extendObject.getExpires());         
        Assert.isEqual(requestObject.getRequestId(), extendObject.getRequestId());

        boolean isGreater = extendObject.getExpires() > requestObject.getExpires();
        assertTrue(isGreater == true);
        this.cleanUpVerifis(myFirstDeviceId);
    }    
    
    @Test
    public void testExtendsAssociationRequest400InvalidCodeFormat() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());            
        
        //perform delete request
        mvc.perform(MockMvcRequestBuilders.put("/association/" + requestObject.getRequestId()).param("code", "error")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest()).andExpect(content().json("{\"parameter\":\"code\",\"error\":\"INVALID_FORMAT: The format of 'code' parameter is invalid. Please use 6-digit code.\"}"));
        this.cleanUpVerifis(myFirstDeviceId);
    }    
        
    @Test
    public void testExtendAssociationRequest404NotFound() throws Exception {        
        
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        
        mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
        
        mvc.perform(MockMvcRequestBuilders.put("/association/" + "notfound").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
        this.cleanUpVerifis(myFirstDeviceId);        
    }     
    
    @Test
    public void testExtendAssociationRequest410Gone() throws Exception {        
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        //make the request
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        
        //set expire on request id from cache manually
        aric.setExpired(requestObject.getRequestId());        
        
        // verify request id exists
        mvc.perform(MockMvcRequestBuilders.put("/association/" + requestObject.getRequestId()).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isGone());           
        
        this.cleanUpVerifis(myFirstDeviceId);               
    }          
        
    @Test
    public void testDeleteAssociationRequest200OK() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());            
        
        //perform delete request
        mvc.perform(MockMvcRequestBuilders.delete("/association/" + requestObject.getRequestId()).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());        
        this.cleanUpVerifis(myFirstDeviceId);
    }    
    
    @Test
    public void testDeleteAssociationRequest400InvalidCodeFormat() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());            
        
        //perform delete request
        mvc.perform(MockMvcRequestBuilders.delete("/association/" + requestObject.getRequestId()).param("code", "error")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest()).andExpect(content().json("{\"parameter\":\"code\",\"error\":\"INVALID_FORMAT: The format of 'code' parameter is invalid. Please use 6-digit code.\"}"));
        this.cleanUpVerifis(myFirstDeviceId);
    }    
    
    @Test
    public void testDeleteAssociationRequest404NotFound() throws Exception {        
        
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        
        mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
        
        mvc.perform(MockMvcRequestBuilders.delete("/association/" + "notfound").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
        this.cleanUpVerifis(myFirstDeviceId);        
    }     
    
    @Test
    public void testDeleteAssociationRequest410Gone() throws Exception {        
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        //make the request
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        
        //set expire on request id from cache manually
        aric.setExpired(requestObject.getRequestId());
        
        // verify request id exists
        mvc.perform(MockMvcRequestBuilders.delete("/association/" + requestObject.getRequestId()).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isGone());           
        
        this.cleanUpVerifis(myFirstDeviceId);               
    }     
    
    @Test
    public void testGetAssociationRequestData200OK() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());            
        
        //perform extend request
        MvcResult extend = mvc.perform(MockMvcRequestBuilders.get("/association/" + requestObject.getRequestId() + "/data")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = extend.getResponse().getContentAsString();        
        AssociationRequestData requestData = mapper.readValue(content, AssociationRequestData.class); 
        Assert.isNotNull(requestData);        
        Assert.isNotNull(requestData.getLogo());        
        Assert.isNotNull(requestData.getUrl());         
        Assert.isNotNull(requestData.getService());                 
        this.cleanUpVerifis(myFirstDeviceId);
    }    
    
    @Test
    public void testGetAssociationRequestData404NotFound() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());
                
        //perform delete request
        mvc.perform(MockMvcRequestBuilders.get("/association/" + "notfound" + "/data")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
        this.cleanUpVerifis(myFirstDeviceId);
    }     
    
    @Test
    public void testGetAssociationRequestData404Expired() throws Exception {        
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        //make the request
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        
        //set expire on request id from cache manually
        aric.setExpired(requestObject.getRequestId());
        
        // verify request id exists
        mvc.perform(MockMvcRequestBuilders.get("/association/" + requestObject.getRequestId() + "/data")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());           
        
        this.cleanUpVerifis(myFirstDeviceId);               
    }    
    
    @Test
    public void testAcceptAssociationRequest200OK() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());            
        
        //perform delete request
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId()+ "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());        
        
        this.cleanUpAssociation(vid, AssociationStatus.ACTIVE);
        this.cleanUpVerifis(myFirstDeviceId);
    }    
    
    @Test
    public void testAcceptAssociationRequest400InvalidCodeFormat() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());            
        
        //perform delete request
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "error")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest()).andExpect(content().json("{\"parameter\":\"code\",\"error\":\"INVALID_FORMAT: The format of 'code' parameter is invalid. Please use 6-digit code.\"}"));        

        this.cleanUpVerifis(myFirstDeviceId);        
    }     
    
    @Test
    public void testAcceptAssociationRequest404NotFound() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());
                
        //perform accept request
        mvc.perform(MockMvcRequestBuilders.post("/association/" + "notfound" + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
        this.cleanUpVerifis(myFirstDeviceId);
    }     
    
    @Test
    public void testRejectAssociationRequest200OK() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());            
        
        //perform delete request
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId()+ "/reject").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());        
                
        this.cleanUpAssociation(vid,AssociationStatus.INACTIVE);
        this.cleanUpVerifis(myFirstDeviceId);
    }    
    
    @Test
    public void testRejectAssociationRequest400InvalidCodeFormat() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());            
        
        //perform delete request
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/reject").param("code", "error")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest()).andExpect(content().json("{\"parameter\":\"code\",\"error\":\"INVALID_FORMAT: The format of 'code' parameter is invalid. Please use 6-digit code.\"}"));        

        this.cleanUpVerifis(myFirstDeviceId);        
    }     
    
    @Test
    public void testRejectAssociationRequest404NotFound() throws Exception {
        
        //create dummy verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);                
        Assert.isNotNull(requestObject);        
        Assert.isNotNull(requestObject.getRequestId());        
        Assert.isNotNull(requestObject.getExpires());
                
        //perform delete request
        mvc.perform(MockMvcRequestBuilders.post("/association/" + "notfound" + "/reject").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
        this.cleanUpVerifis(myFirstDeviceId);
    }        
    // TEST /association requests - end    
    
    // TEST /associations requests - start
    
    @Test
    public void testGetAssociationsByVid200Ok() throws Exception
    {
        // make first request/accept
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept the request - create
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
        
        // seconds association        
        result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = result.getResponse().getContentAsString();        
        requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept the request - create
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());        
        
        // third association
        result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = result.getResponse().getContentAsString();        
        requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept the request - create
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());           
        
        MvcResult response = mvc.perform(MockMvcRequestBuilders.get("/associations/" + vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = response.getResponse().getContentAsString();
        Associations associations = mapper.readValue(content, Associations.class);
        assertNotNull(associations);
        assertTrue(associations.getTotalAssocs() == 3);
        assertTrue(associations.getAssociations() != null);
        assertTrue(associations.getAssociations().size() == 3);
                
        // cleanup
        this.cleanUpAssociation(vid,AssociationStatus.ACTIVE);
        this.cleanUpVerifis(myFirstDeviceId);        
        
    }
    
    @Test
    public void testGetAssociationsByVid400InvalidCodeFormat() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/associations/" + "vid").param("code", "error")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());               
    }    
    
    @Test
    public void testGetAssociationsByVid404NotFound() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/associations/" + "notfound").param("code", "123456")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());               
    }    
    
    @Test
    public void testGetAssociationsByVidPaginated200Ok() throws Exception
    {
        // make first request/accept
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept the request - create
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
        
        // seconds association        
        result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = result.getResponse().getContentAsString();        
        requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept the request - create
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());        
        
        // third association
        result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = result.getResponse().getContentAsString();        
        requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept the request - create
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());           
        
        MvcResult response = mvc.perform(MockMvcRequestBuilders.get("/associations/" + vid).param("code", "123456").param("start", "1").param("limit", "3")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = response.getResponse().getContentAsString();
        Associations associations = mapper.readValue(content, Associations.class);
        assertNotNull(associations);
        assertTrue(associations.getTotalAssocs() == 3);
        assertTrue(associations.getAssociations() != null);
        assertTrue(associations.getAssociations().size() == 2); //starting at element #1
                
        response = mvc.perform(MockMvcRequestBuilders.get("/associations/" + vid).param("code", "123456").param("start", "0").param("limit", "2")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = response.getResponse().getContentAsString();
        associations = mapper.readValue(content, Associations.class);
        assertNotNull(associations);
        assertTrue(associations.getTotalAssocs() == 3); //ignoring paging
        assertTrue(associations.getAssociations() != null);
        assertTrue(associations.getAssociations().size() == 2); //limit = 2 elements!
        
        // cleanup
        this.cleanUpAssociation(vid,AssociationStatus.ACTIVE);
        this.cleanUpVerifis(myFirstDeviceId);                
    }    
    
    @Test
    public void testGetAssociationByVidAndAssocId200Ok() throws Exception
    {
        // make first request/accept
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept association
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());        
        
        // get associations by vid        
        MvcResult response = mvc.perform(MockMvcRequestBuilders.get("/associations/" + vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = response.getResponse().getContentAsString();
        Associations associations = mapper.readValue(content, Associations.class);
        assertNotNull(associations);
        assertTrue(associations.getTotalAssocs() == 1);
        assertTrue(associations.getAssociations() != null);
        assertTrue(associations.getAssociations().size() == 1); //starting at element #1        
        String assocId = associations.getAssociations().get(0).getAssocId();
        
        //make association information request
        response = mvc.perform(MockMvcRequestBuilders.get("/associations/" + vid + "/" + assocId).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = response.getResponse().getContentAsString();        
        Association association = mapper.readValue(content, Association.class);
        assertNotNull(association);
        assertEquals(assocId, association.getAssocId());
        assertEquals(AssociationStatus.ACTIVE, association.getStatus());
        
        // cleanup
        this.cleanUpAssociation(vid,AssociationStatus.ACTIVE);
        this.cleanUpVerifis(myFirstDeviceId);        
        
    }    
    
    @Test
    public void testGetAssociationByVidAndAssocId400InvalidCodeFormat() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/associations/" + "vid" + "/" + "assocId").param("code", "error")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
        .andExpect(content().json("{\"parameter\":\"code\",\"error\":\"INVALID_FORMAT: The format of 'code' parameter is invalid. Please use 6-digit code.\"}"));
    }    
    
    @Test
    public void testGetAssociationByVidAndAssocId404VidNotFound() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/associations/" + "unknownVid" + "/" + "assocId").param("code", "123456")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }    
    
    @Test
    public void testGetAssociationByVidAndAssocId404AssocIdNotFound() throws Exception
    {
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept association
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());         
        mvc.perform(MockMvcRequestBuilders.get("/associations/" + vid + "/" + "unknownAssocId").param("code", "123456")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
        
        // cleanup
        this.cleanUpAssociation(vid,AssociationStatus.ACTIVE);
        this.cleanUpVerifis(myFirstDeviceId);          
    }    
    
    @Test
    public void testGetAssociationByVidAndAssocId404AssocIdAndVidPairNotFound() throws Exception
    {
        //create first verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // make copy
        String vidOne = vid;
        
        // accept first association
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());         
        
        //create second verifis
        this.createVerifis(mySecondDeviceId);        
        vid = verifisService.findAllByDeviceId(mySecondDeviceId).get(0).getVid();
        result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = result.getResponse().getContentAsString();        
        requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept second association
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());          
        
        // get associations by vid        
        MvcResult response = mvc.perform(MockMvcRequestBuilders.get("/associations/" + vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = response.getResponse().getContentAsString();
        Associations associations = mapper.readValue(content, Associations.class);
        assertNotNull(associations);
        assertTrue(associations.getTotalAssocs() == 1);
        assertTrue(associations.getAssociations() != null);
        assertTrue(associations.getAssociations().size() == 1); //starting at element #1        
        String assocId = associations.getAssociations().get(0).getAssocId();        
        
        // provide vidOne: wrong pair of vid & assocId
        mvc.perform(MockMvcRequestBuilders.get("/associations/" + vidOne + "/" + assocId).param("code", "123456")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());        
                
        // cleanup
        this.cleanUpAssociation(vidOne,AssociationStatus.ACTIVE);
        this.cleanUpAssociation(vid,AssociationStatus.ACTIVE);
        this.cleanUpVerifis(myFirstDeviceId);      
        this.cleanUpVerifis(mySecondDeviceId);  
    }    
    
    @Test
    public void testDeleteAssociationByVidAndAssocId200Ok() throws Exception
    {
        // make first request/accept
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept association
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());        
        
        // get associations by vid        
        MvcResult response = mvc.perform(MockMvcRequestBuilders.get("/associations/" + vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = response.getResponse().getContentAsString();
        Associations associations = mapper.readValue(content, Associations.class);
        assertNotNull(associations);
        assertTrue(associations.getTotalAssocs() == 1);
        assertTrue(associations.getAssociations() != null);
        assertTrue(associations.getAssociations().size() == 1); //starting at element #1        
        String assocId = associations.getAssociations().get(0).getAssocId();
        
        //make association information request
        mvc.perform(MockMvcRequestBuilders.delete("/associations/" + vid + "/" + assocId).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
                
        // cleanup
        this.cleanUpAssociation(vid,AssociationStatus.ACTIVE);
        this.cleanUpVerifis(myFirstDeviceId);        
        
    }    
    
    @Test
    public void testDeleteAssociationByVidAndAssocId400InvalidCodeFormat() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.delete("/associations/" + "vid" + "/" + "assocId").param("code", "error")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
        .andExpect(content().json("{\"parameter\":\"code\",\"error\":\"INVALID_FORMAT: The format of 'code' parameter is invalid. Please use 6-digit code.\"}"));
    }     
    
    @Test
    public void testDeleteAssociationByVidAndAssocId404VidNotFound() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.delete("/associations/" + "unknownVid" + "/" + "assocId").param("code", "123456")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }    
    
    @Test
    public void testDeleteAssociationByVidAndAssocId404AssocIdNotFound() throws Exception
    {
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept association
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());         
        mvc.perform(MockMvcRequestBuilders.delete("/associations/" + vid + "/" + "unknownAssocId").param("code", "123456")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
        
        // cleanup
        this.cleanUpAssociation(vid,AssociationStatus.ACTIVE);
        this.cleanUpVerifis(myFirstDeviceId);          
    }    
    
    @Test
    public void testDeleteAssociationByVidAndAssocId404AssocIdAndVidPairNotFound() throws Exception
    {
        //create first verifis
        this.createVerifis(myFirstDeviceId);        
        String vid = verifisService.findAllByDeviceId(myFirstDeviceId).get(0).getVid();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        AssociationRequestObject requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // make copy
        String vidOne = vid;
        
        // accept first association
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());         
        
        //create second verifis
        this.createVerifis(mySecondDeviceId);        
        vid = verifisService.findAllByDeviceId(mySecondDeviceId).get(0).getVid();
        result = mvc.perform(MockMvcRequestBuilders.post("/association").param("vid", vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = result.getResponse().getContentAsString();        
        requestObject = mapper.readValue(content, AssociationRequestObject.class);     
        
        // accept second association
        mvc.perform(MockMvcRequestBuilders.post("/association/" + requestObject.getRequestId() + "/accept").param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());          
        
        // get associations by vid        
        MvcResult response = mvc.perform(MockMvcRequestBuilders.get("/associations/" + vid).param("code", "123456")
            .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        content = response.getResponse().getContentAsString();
        Associations associations = mapper.readValue(content, Associations.class);
        assertNotNull(associations);
        assertTrue(associations.getTotalAssocs() == 1);
        assertTrue(associations.getAssociations() != null);
        assertTrue(associations.getAssociations().size() == 1); //starting at element #1        
        String assocId = associations.getAssociations().get(0).getAssocId();        
        
        // provide vidOne: wrong pair of vid & assocId
        mvc.perform(MockMvcRequestBuilders.delete("/associations/" + vidOne + "/" + assocId).param("code", "123456")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());        
                
        // cleanup
        this.cleanUpAssociation(vidOne,AssociationStatus.ACTIVE);
        this.cleanUpAssociation(vid,AssociationStatus.ACTIVE);
        this.cleanUpVerifis(myFirstDeviceId);      
        this.cleanUpVerifis(mySecondDeviceId);  
    }          
    // TEST /associations requests - end
    
    
    // TEST /associations requests - start
    
    @Test
    public void testCreateTransfer200Ok() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        assertNotNull(transferObject);
        assertTrue(transferObject.getExpires() > 0);        
        this.cleanUpVerifis(myFirstDeviceId);  
    }
    
    @Test
    public void testCreateTransfer400InvalidCodeFormat() throws Exception
    {        
        mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", "vid").param("code", "error").param("deviceId", "deviceId").param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
        .andExpect(content().json("{\"parameter\":\"code\",\"error\":\"INVALID_FORMAT: The format of 'code' parameter is invalid. Please use 6-digit code.\"}"));  
    }    
    
    @Test
    public void testCreateTransfer400InvalidTransferPinFormat() throws Exception
    {        
        mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", "vid").param("code", "123456").param("deviceId", "deviceId").param("transferPin", "error")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
        .andExpect(content().json("{\"parameter\":\"transferPin\",\"error\":\"INVALID_FORMAT: The format of 'transferPin' parameter is invalid. Please use 4-digit code.\"}"));  
    }      
    
    @Test
    public void testCreateTransfer403ForbiddenDeviceIdMismatch() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = "mimatch";
        mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isForbidden());        
        this.cleanUpVerifis(myFirstDeviceId);  
    }    

    @Test
    public void testUploadEncryptedToken200Ok() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        // upload token request
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + transferObject.getTransferId() + "/token")
            .param("deviceId", deviceId)            
            .contentType(MediaType.APPLICATION_OCTET_STREAM).content(content.getBytes())).andExpect(status().isOk());        
        this.cleanUpVerifis(myFirstDeviceId);  
    }      
    
    @Test
    public void testUploadEncryptedToken400EmptyTokenBody() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        // upload token request
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + transferObject.getTransferId() + "/token")
            .param("deviceId", deviceId))            
            .andExpect(status().isBadRequest());        
        this.cleanUpVerifis(myFirstDeviceId);  
    }     
    
    @Test
    public void testUploadEncryptedToken403ForbiddenDeviceIdMismatch() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        // upload token request
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + transferObject.getTransferId() + "/token")
            .param("deviceId", "mismatch")            
            .contentType(MediaType.APPLICATION_OCTET_STREAM).content(content.getBytes())).andExpect(status().isForbidden());        
        this.cleanUpVerifis(myFirstDeviceId);  
    }         
    
    @Test
    public void testUploadEncryptedToken400MissingDeviceId() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + "transferId" + "/token")
                .contentType(MediaType.APPLICATION_OCTET_STREAM).content("john".getBytes()))
                .andExpect(status().isBadRequest()).andExpect(content().json("{\"parameter\":\"deviceId\",\"error\":\"deviceId is required\"}"));
    }      
    
    @Test
    public void testCheckEncryptedToken200Ok() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        // upload token request
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + transferObject.getTransferId() + "/token")
            .param("deviceId", deviceId)            
            .contentType(MediaType.APPLICATION_OCTET_STREAM).content(content.getBytes())).andExpect(status().isOk());        
        
        //check request
        mvc.perform(MockMvcRequestBuilders.head("/transfer/" + transferObject.getTransferId() + "/token")
            .param("deviceId", deviceId)).andExpect(status().isOk());              
        this.cleanUpVerifis(myFirstDeviceId);  
    }    
    
    @Test
    public void testCheckEncryptedToken404TransferIdNotfound() throws Exception
    {                      
        //check request
        mvc.perform(MockMvcRequestBuilders.head("/transfer/" + "notfound" + "/token")
            .param("deviceId", "deviceId")).andExpect(status().isNotFound());              
    }     
    
    @Test
    public void testCheckEncryptedToken410Expired() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        //set transfer as expired manually
        tic.setExpired(transferObject.getTransferId());
        
        //check request
        mvc.perform(MockMvcRequestBuilders.head("/transfer/" + transferObject.getTransferId() + "/token")
            .param("deviceId", deviceId)).andExpect(status().isGone());              
        this.cleanUpVerifis(myFirstDeviceId);  
    }     
    
    @Test
    public void testCheckEncryptedToken404TokenUnavailable() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        //check request
        mvc.perform(MockMvcRequestBuilders.head("/transfer/" + transferObject.getTransferId() + "/token")
            .param("deviceId", deviceId)).andExpect(status().isNotFound());              
        this.cleanUpVerifis(myFirstDeviceId);  
    }      
    
    @Test
    public void testCreateTransferReport200Ok() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        String myFirstDeviceIdTransferred = "myFirstDeviceIdTransferred";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        // upload token request
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + transferObject.getTransferId() + "/token")
            .param("deviceId", deviceId)            
            .contentType(MediaType.APPLICATION_OCTET_STREAM).content(content.getBytes())).andExpect(status().isOk());        
        
        //report request
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + transferObject.getTransferId() + "/report")
            .param("deviceId", myFirstDeviceIdTransferred)).andExpect(status().isOk());
        Verifis ver = verifisService.findAllByDeviceId(myFirstDeviceIdTransferred).get(0);
        assertNotNull(ver);
        assertEquals(myFirstDeviceIdTransferred, ver.getDeviceId());
        assertEquals(vid, ver.getVid());
        assertEquals(InstallationType.TRANSFER, ver.getType());
        this.transferReportService.delete(transferReportService.find(transferObject.getTransferId(), myFirstDeviceIdTransferred));
        this.cleanUpVerifis(myFirstDeviceIdTransferred);  
    }    
    
    @Test
    public void testCreateTransferReport404TransferIdNotfound()throws Exception
    {                  
        //report request
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + "notfound" + "/report")
            .param("deviceId", "deviceId")).andExpect(status().isNotFound()); 
    }     
    
    @Test
    public void testCreateTransferReport410Expired() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        //set transfer as expired manually
        tic.setExpired(transferObject.getTransferId());
        
        //check request
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + transferObject.getTransferId() + "/report")
            .param("deviceId", deviceId)).andExpect(status().isGone());              
        this.cleanUpVerifis(myFirstDeviceId);  
    }     
    
    @Test
    public void testCreateTransferReport404TokenUnavailable() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        //report request
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + transferObject.getTransferId() + "/report")
            .param("deviceId", deviceId)).andExpect(status().isNotFound());              
        this.cleanUpVerifis(myFirstDeviceId);  
    }    
        
    @Test
    public void testCreateTransferReport400MissingDeviceId() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + "trasnferId" + "/report")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().json("{\"parameter\":\"deviceId\",\"error\":\"deviceId is required\"}"));
    }     
  
    @Test
    public void testGetReportStatus200Ok() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        // upload token request
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + transferObject.getTransferId() + "/token")
            .param("deviceId", deviceId)            
            .contentType(MediaType.APPLICATION_OCTET_STREAM).content(content.getBytes())).andExpect(status().isOk());        
        
        //report request
        String newDeviceId = "newDeviceId";
        mvc.perform(MockMvcRequestBuilders.post("/transfer/" + transferObject.getTransferId() + "/report")
            .param("deviceId", newDeviceId)).andExpect(status().isOk());
        
        // make report status request
        mvc.perform(MockMvcRequestBuilders.get("/transfer/" + transferObject.getTransferId() + "/report")            
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
        .andExpect(content().json("{\"status\":\"SUCCESS\",\"deviceId\":\"newDeviceId\"}"));           
        
        this.transferReportService.delete(transferReportService.find(transferObject.getTransferId(), newDeviceId));
        this.cleanUpVerifis(newDeviceId);  
    }    
    
    @Test
    public void testGetReportStatus404TransferIdNotfound() throws Exception
    {                      
        //check request
        mvc.perform(MockMvcRequestBuilders.get("/transfer/" + "notfound" + "/report")
            .param("deviceId", "deviceId")).andExpect(status().isNotFound());              
    }     
    
    /*
    @Test
    public void testGetReportStatus410Expired() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        //set transfer as expired manually
        tic.setExpired(transferObject.getTransferId());
        
        //check request
        mvc.perform(MockMvcRequestBuilders.get("/transfer/" + "notfound" + "/report")).andExpect(status().isGone());              
        this.cleanUpVerifis(myFirstDeviceId);  
    }     
    */
    
    /*
    @Test
    public void testGetReportStatus404TokenUnavailable() throws Exception
    {        
        this.createVerifis(myFirstDeviceId);        
        Verifis verifis = verifisService.findAllByDeviceId(myFirstDeviceId).get(0);
        String vid = verifis.getVid();
        String deviceId = verifis.getDeviceId();
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/transfer")
            .param("vid", vid).param("code", "123456").param("deviceId", deviceId).param("transferPin", "1234")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TransferObject transferObject = mapper.readValue(content, TransferObject.class);   
        
        //check request
        mvc.perform(MockMvcRequestBuilders.head("/transfer/" + transferObject.getTransferId() + "/token")
            .param("deviceId", deviceId)).andExpect(status().isNotFound());              
        this.cleanUpVerifis(myFirstDeviceId);  
    }      
    */
    // TEST /associations requests - end    
        
}
