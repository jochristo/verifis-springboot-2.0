package org.verifis.api.service;

import java.util.List;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.verifis.api.model.bind.TransferSessionParameter;
import org.verifis.api.model.serializable.TransferItem;
import org.verifis.core.cache.impl.TransferItemCacheable;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.DigestUtils;
import org.verifis.api.Constants.Cache.KeyPrefix;
import org.verifis.api.service.impl.TransferHandlerImpl;
import org.verifis.api.domain.TransferReport;
import org.verifis.api.domain.Verifis;
import org.verifis.api.exception.InvalidTransferPinException;
import org.verifis.api.exception.TokenUnavailableException;
import org.verifis.api.exception.TransferIdExpiredException;
import org.verifis.api.exception.TransferIdNotFoundException;
import org.verifis.api.exception.VerifisDeviceMismatchException;
import org.verifis.api.exception.VerifisReportNotYetSubmittedException;
import org.verifis.api.model.TokenReport;
import org.verifis.api.model.bind.ActiveTransferParameter;
import org.verifis.api.model.enumeration.ActivationStatus;
import org.verifis.api.model.enumeration.MobilePlatform;
import org.verifis.api.model.enumeration.OperationStatus;
import org.verifis.api.model.serializable.TransferObject;
import org.verifis.core.utility.Utilities;

/**
 * Unit test of component TransferHandlerImpl.
 * Injected mock objects: TransferItemCacheable, ITransferReportService, IVerifisService, IVerifisVerifier
 * @author Admin
 */
@RunWith(MockitoJUnitRunner.class)
public class TransferHandlerImplUnitTest {
    
    @InjectMocks private TransferHandlerImpl component;
    @Mock private TransferItemCacheable cache;
    @Mock private ITransferReportService transferReportService;    
    @Mock private IVerifisService verifisService;     
    @Mock private IVerifisVerifier verifier;
    
    private TransferSessionParameter tsr = null;
    private Verifis verifis = null;
    private TransferItem _transferItem = null;
    private ActiveTransferParameter activeTransferParameter = null;
    
    @Before
    public void before()
    {         
        createMockVerifis("deviceId");
        
        tsr = new TransferSessionParameter();
        tsr.setCode("123456");
        tsr.setDeviceId(verifis.getDeviceId());
        tsr.setTransferPin("1234");
        tsr.setVid(verifis.getVid());         
        
        _transferItem = new TransferItem();
        _transferItem.setForwardingDeviceId(tsr.getDeviceId());            
        _transferItem.setVid(tsr.getVid());
        _transferItem.setTransferPin(DigestUtils.md5DigestAsHex(tsr.getTransferPin().getBytes()));  
        
        activeTransferParameter = new ActiveTransferParameter();        
        activeTransferParameter.setCode("123456");
        activeTransferParameter.setDeviceId(_transferItem.getForwardingDeviceId());          
    }
    
    protected void createMockVerifis(String deviceId) {
        // create verifis
        verifis = new Verifis();        
        verifis.setAccessPin("1234");
        verifis.setDescription("description");
        verifis.setDeviceId(deviceId);
        verifis.setExpired(false);
        verifis.setName(deviceId);
        verifis.setPlatform(MobilePlatform.IOS);
        verifis.setStatus(ActivationStatus.ACTIVE);
        verifis.setValidUntil(Utilities.currentEpochPlus(3600));
        verifis.setVersion("2.0.1");
        //verifisService.save(verifis);
    }
    
    protected void cleanUpMockVerifis(String deviceId) {
        List<Verifis> verifis = verifisService.findAllByDeviceIdIncludeDeletedAndInactive(deviceId);
        if (verifis != null) {
            verifis.forEach((item) -> {
                verifisService.delete(item);
            });
        }
    }     
    
    @After
    public void after()
    {
        
    }
    
    @Test
    public void mockCreateVerifyTransferOK()
    { 
        Mockito.when(verifisService.findByVidAndDeviceIdExplicitly(verifis.getVid(), verifis.getDeviceId())).thenReturn(verifis);      
        TransferObject transferObject = component.create(tsr);
        _transferItem.setTransferId(transferObject.getTransferId());
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());         
        TransferObject verify = component.verify(_transferItem.getTransferId());
        assertEquals(transferObject.getTransferId(), verify.getTransferId());        
    }    
    
    @Test(expected = VerifisDeviceMismatchException.class)
    public void mockCreateTransferVerifisDeviceMismatchException()
    { 
        Mockito.when(verifisService.findByVidAndDeviceIdExplicitly(verifis.getVid(), verifis.getDeviceId())).thenThrow(new VerifisDeviceMismatchException("deviceId does not match active installation's deviceId"));        
        component.create(tsr);        
    }     
    
    @Test(expected = TransferIdNotFoundException.class)
    public void mockVerifyTransferNotFound()
    { 
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenThrow(new TransferIdNotFoundException("transferId not found: " + _transferItem.getTransferId()));              
        component.verify(_transferItem.getTransferId());        
    }

    @Test(expected = TransferIdExpiredException.class)
    public void mockVerifyTransferExpired()
    { 
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());
        Mockito.when(cache.get(_transferItem.getTransferId())).thenThrow(new TransferIdExpiredException("transferId has expired: " + _transferItem.getTransferId()));              
        component.verify(_transferItem.getTransferId());       
    }    
    
    @Test
    public void mockExtendTransferOK()
    {                 
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());                   
        TransferObject verify = component.extend(_transferItem.getTransferId(),activeTransferParameter);
        assertEquals(_transferItem.getTransferId(), verify.getTransferId());        
    }      
    
    @Test(expected = TransferIdNotFoundException.class)
    public void mockExtendTransferNotFound()
    {              
        //Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId()))
            .thenThrow(new TransferIdNotFoundException("transferId not found: " + _transferItem.getTransferId()));       
        component.extend(_transferItem.getTransferId(),activeTransferParameter);                        
    }    
    
    @Test(expected = TransferIdExpiredException.class)
    public void mockExtendTransferExpired()
    {         
        Mockito.when(cache.get(_transferItem.getTransferId())).thenThrow(new TransferIdExpiredException("transferId has expired: " + _transferItem.getTransferId()));        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());       
        component.extend(_transferItem.getTransferId(),activeTransferParameter);                        
    }    
    
    @Test(expected = VerifisDeviceMismatchException.class)
    public void mockExtendTransferDeviceMismatchException()
    { 
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());                                       
        activeTransferParameter.setDeviceId("mismatch");
        component.extend(_transferItem.getTransferId(), activeTransferParameter);        
    }      
    
    @Test
    public void mockDeleteTransferOK()
    {             
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());       
        ResponseEntity response = component.delete(_transferItem.getTransferId(), activeTransferParameter);        
        assertEquals(200, response.getStatusCodeValue());        
    }    
    
    @Test(expected = TransferIdNotFoundException.class)
    public void mockDeleteTransferNotFound()
    {              
        //Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId()))
            .thenThrow(new TransferIdNotFoundException("transferId not found: " + _transferItem.getTransferId()));       
        component.delete(_transferItem.getTransferId(),activeTransferParameter);                        
    }    
    
    @Test(expected = TransferIdExpiredException.class)
    public void mockDeleteTransferExpired()
    {         
        Mockito.when(cache.get(_transferItem.getTransferId())).thenThrow(new TransferIdExpiredException("transferId has expired: " + _transferItem.getTransferId()));        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());       
        component.delete(_transferItem.getTransferId(),activeTransferParameter);                        
    }      
    
    @Test
    public void mockUploadOk()
    {
        byte[] data = "data".getBytes();        
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());    
        //Mockito.when(verifisService.findByVidAndDeviceIdExplicitly(verifis.getVid(), verifis.getDeviceId())).thenReturn(verifis);   
        component.upload(_transferItem.getForwardingDeviceId(), _transferItem.getTransferId(), data);        
        assertEquals(_transferItem.getToken(), data);
    }
    
    @Test(expected = TransferIdNotFoundException.class)
    public void mockUploadNotFound()
    {
        byte[] data = "data".getBytes();        
        //Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);                
        //Mockito.when(verifisService.findByVidAndDeviceIdExplicitly(verifis.getVid(), verifis.getDeviceId())).thenReturn(verifis);   
        component.upload(_transferItem.getForwardingDeviceId(), _transferItem.getTransferId(), data);        
        
    }

    @Test(expected = TransferIdExpiredException.class)
    public void mockUploadExpired()
    {
        byte[] data = "data".getBytes();                
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());    
        //Mockito.when(verifisService.findByVidAndDeviceIdExplicitly(verifis.getVid(), verifis.getDeviceId())).thenReturn(verifis);   
        component.upload(_transferItem.getForwardingDeviceId(), _transferItem.getTransferId(), data);                
    }

    @Test(expected = VerifisDeviceMismatchException.class)
    public void mockUploadVerifisDeviceMismatchException()
    {
        byte[] data = "data".getBytes();        
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());                    
        component.upload("mismatch", _transferItem.getTransferId(), data);                
    }    
    
    @Test
    public void mockDownloadOk()
    {
        byte[] data = "data".getBytes();        
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());    
        //Mockito.when(verifisService.findByVidAndDeviceIdExplicitly(verifis.getVid(), verifis.getDeviceId())).thenReturn(verifis);   
        component.upload(_transferItem.getForwardingDeviceId(), _transferItem.getTransferId(), data);   
        HttpEntity<byte[]> bytes = component.download(tsr.getTransferPin(), _transferItem.getTransferId());        
        assertEquals(data.hashCode(), bytes.getBody().hashCode());
    }
    
    @Test(expected = TransferIdNotFoundException.class)
    public void mockDownloadNotFound()
    {     
        //Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);                
        //Mockito.when(verifisService.findByVidAndDeviceIdExplicitly(verifis.getVid(), verifis.getDeviceId())).thenReturn(verifis);   
        component.download(tsr.getTransferPin(), "notfound");     
        
    }

    @Test(expected = TransferIdExpiredException.class)
    public void mockDownloadExpired()
    {        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());            
        component.download(tsr.getTransferPin(), _transferItem.getTransferId());               
    }
    
    @Test(expected = InvalidTransferPinException.class)
    public void mockDownloadInvalidTransferPinException()
    {        
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());                    
        component.download("invalidPin", _transferItem.getTransferId());             
    }    
    
    @Test(expected = TokenUnavailableException.class)
    public void mockDownloadTokenUnavailableException()
    {        
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());                    
        component.download(tsr.getTransferPin(), _transferItem.getTransferId());             
    }      

    @Test
    public void mockCheckTokenAvailabilityOk()
    {
        byte[] data = "data".getBytes();        
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());    
        //Mockito.when(verifisService.findByVidAndDeviceIdExplicitly(verifis.getVid(), verifis.getDeviceId())).thenReturn(verifis);   
        component.upload(_transferItem.getForwardingDeviceId(), _transferItem.getTransferId(), data);   
        ResponseEntity response = component.checkTokenAvailability(_transferItem.getTransferId());        
        assertEquals(200, response.getStatusCodeValue());
    }  
    
    @Test(expected = TransferIdNotFoundException.class)
    public void mockCheckTokenAvailabilityNotFound()
    {             
        component.checkTokenAvailability(_transferItem.getTransferId());           
    }

    @Test(expected = TransferIdExpiredException.class)
    public void mockCheckTokenAvailabilityExpired()
    {        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());            
        component.checkTokenAvailability(_transferItem.getTransferId());                
    }
    
    @Test(expected = TokenUnavailableException.class)
    public void mockCheckTokenAvailabilityITokenUnavailableException()
    {        
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());                    
        component.checkTokenAvailability(_transferItem.getTransferId());           
    }      
    
    @Test
    public void mockGetStatusOk()
    {
        TransferReport transferReport = new TransferReport();
        transferReport.setStatus(OperationStatus.SUCCESS);
        transferReport.setForwardingDeviceId(_transferItem.getRecipientDeviceId());               
        transferReport.setTransferId(_transferItem.getTransferId());
        transferReport.setVid(_transferItem.getVid());
        Mockito.when(this.transferReportService.findSuccessful(_transferItem.getTransferId())).thenReturn(transferReport);
        TokenReport tokenReport = component.getStatus(_transferItem.getTransferId());                     
        assertEquals(_transferItem.getVid(), tokenReport.getVid());
        assertEquals(_transferItem.getRecipientDeviceId(), tokenReport.getDeviceId());
    }     
    
    @Test(expected = VerifisReportNotYetSubmittedException.class)
    public void mockGetStatusVerifisReportNotYetSubmittedException()
    {
        TransferReport transferReport = new TransferReport();
        transferReport.setStatus(OperationStatus.SUCCESS);
        transferReport.setForwardingDeviceId(_transferItem.getRecipientDeviceId());               
        transferReport.setTransferId(_transferItem.getTransferId());
        transferReport.setVid(_transferItem.getVid());
        //Mockito.when(this.transferReportService.findSuccessful(_transferItem.getTransferId())).thenReturn(transferReport);
        component.getStatus("unknown");                             
    }   
    
    @Test
    public void mockCreateTokenReportOk()
    {
        byte[] data = "data".getBytes(); 
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId()); 
        Mockito.when(verifisService.findByVidAndDeviceIdExplicitly(verifis.getVid(), verifis.getDeviceId())).thenReturn(verifis);   
        component.upload(_transferItem.getForwardingDeviceId(), _transferItem.getTransferId(), data);  
        
        Mockito.when(verifisService.update(verifis.getId(), verifis)).thenReturn(verifis); 
        Mockito.when(verifier.deactivate(verifis.getVid(), (_transferItem.getForwardingDeviceId()))).thenReturn(true);
        TokenReport tokenReport = component.createTokenReport(_transferItem.getForwardingDeviceId(),  _transferItem.getTransferId());                 
        assertEquals(_transferItem.getVid(), tokenReport.getVid());
        assertEquals(_transferItem.getRecipientDeviceId(), tokenReport.getDeviceId());
        assertEquals(OperationStatus.SUCCESS.getDescription(), tokenReport.getStatus());
    }
    
    @Test(expected = TransferIdNotFoundException.class)
    public void mockCreateTokenReporNotFound()
    {             
        component.createTokenReport(_transferItem.getForwardingDeviceId(),  _transferItem.getTransferId());             
    }

    @Test(expected = TransferIdExpiredException.class)
    public void mockCreateTokenReporExpired()
    {        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());            
        component.createTokenReport(_transferItem.getForwardingDeviceId(),  _transferItem.getTransferId());                  
    }
    
    @Test(expected = TokenUnavailableException.class)
    public void mockCreateTokenReporTokenUnavailableException()
    {        
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());                    
        component.createTokenReport(_transferItem.getForwardingDeviceId(),  _transferItem.getTransferId());             
    }   
    
    @Test(expected = Exception.class)
    public void mockCreateTokenReporFailsWithExceptionStatusFailure()
    {        
        byte[] data = "data".getBytes(); 
        Mockito.when(cache.get(_transferItem.getTransferId())).thenReturn(_transferItem);        
        Mockito.when(cache.get(KeyPrefix.TRANSFER_REQUEST_ID_EXISTENCE, _transferItem.getTransferId())).thenReturn(_transferItem.getTransferId());                
        component.upload(_transferItem.getForwardingDeviceId(), _transferItem.getTransferId(), data);
        //Mockito.when(verifisService.findByVidAndDeviceIdExplicitly(verifis.getVid(), verifis.getDeviceId())).thenReturn(verifis).thenThrow(new Exception("mpla mpla"));                 
        Mockito.doThrow(Exception.class).when(verifier).deactivate(verifis.getVid(), verifis.getDeviceId()); // exception in persistence layer for instance
        TokenReport tokenReport = component.createTokenReport("newDeviceId",  _transferItem.getTransferId());             
        assertEquals(_transferItem.getVid(), tokenReport.getVid());
        assertEquals(_transferItem.getRecipientDeviceId(), tokenReport.getDeviceId());
        assertEquals(OperationStatus.FAILURE.getDescription(), tokenReport.getStatus());        
    }     
        
}
