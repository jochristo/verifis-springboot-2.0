package org.verifis.api.service;

import com.querydsl.core.types.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.verifis.api.domain.Verifis;
import org.verifis.api.model.enumeration.ActivationStatus;
import org.verifis.api.model.enumeration.MobilePlatform;
import org.verifis.api.repository.IVerifisRepository;
import org.verifis.api.service.impl.VerifisServiceImpl;
import org.verifis.core.utility.Utilities;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.verifis.api.domain.QVerifis;
import org.verifis.api.exception.AccessTokenMismatchException;
import org.verifis.api.exception.VerifisDeviceMismatchException;
import org.verifis.api.exception.VerifisIdNotActiveException;
import org.verifis.api.exception.VerifisIdNotFoundException;
import org.verifis.api.model.BootResponse;
import org.verifis.api.model.TokenReport;
import org.verifis.api.model.bind.BootRequestParameter;
import org.verifis.api.model.bind.DeviceIdParameter;
import org.verifis.api.model.bind.VerifisTokenParameter;
import org.verifis.api.model.enumeration.OperationStatus;
import org.verifis.api.model.serializable.UnencryptedTokenItem;
import org.verifis.api.model.serializable.VerifisToken;
import org.verifis.core.cache.impl.UnencryptedTokenItemCacheable;

/**
 * Unit test of component TransferComponentImpl.
 * Injected mock objects: IVerifisRepository, IVerifisVerifier
 * @author Admin
 */
@RunWith(MockitoJUnitRunner.class)
public class VerifisServiceImplUnitTest
{    
    @InjectMocks private VerifisServiceImpl service;
    @Mock private IVerifisVerifier verifier;
    @Mock private IVerifisRepository repository;
    @Mock private UnencryptedTokenItemCacheable cache;
    private static final Logger logger = LoggerFactory.getLogger(VerifisServiceImplUnitTest.class);     
    
    private Verifis verifis = null;    
    private List<Verifis> list = null;
    
    @Before
    public void before()
    {         
        list = new ArrayList();
        createMockVerifis("deviceId");        
    }
    
    @After
    public void after()
    {
        
    }    

    protected void createMockVerifis(String deviceId)
    {
        verifis = new Verifis();
        
        verifis.setAccessPin("1234");
        verifis.setDescription("description");
        verifis.setDeviceId(deviceId);
        verifis.setExpired(false);
        verifis.setName(deviceId);
        verifis.setPlatform(MobilePlatform.IOS);
        verifis.setStatus(ActivationStatus.ACTIVE);
        verifis.setValidUntil(Utilities.currentEpochPlus(3600));
        verifis.setVersion("2.0.1");
        verifis.setId(UUID.randomUUID().toString());                
        list.add(verifis);
    }
    
    protected void cleanUpMockVerifis(String deviceId) {
        List<Verifis> verifis = service.findAllByDeviceIdIncludeDeletedAndInactive(deviceId);
        if (verifis != null) {
            verifis.forEach((item) -> {
                service.delete(item);
            });
        }
    }        
    
    @Test
    public void mockGet()
    { 
        Mockito.when(repository.findAll()).thenReturn(list);
        List<Verifis> data = service.get();
        assertNotNull(list);
        assertEquals(1, data.size());
        assertEquals(verifis.getVid(), data.get(0).getVid());
    }     
    
    @Test
    public void mockFindOne()
    { 
        Mockito.when(repository.findOne(verifis.getId())).thenReturn(verifis);
        Verifis ver = service.findOne(verifis.getId());
        assertNotNull(ver);
        assertEquals(verifis.getVid(), ver.getVid());        
    }  

    @Test
    public void mockSave()
    { 
        Mockito.when(repository.save(verifis)).thenReturn(verifis);
        service.save(verifis);
        Mockito.when(repository.findAll()).thenReturn(list);
        List<Verifis> data = service.get();
        assertNotNull(list);
        assertEquals(1, data.size());
        assertEquals(verifis.getVid(), data.get(0).getVid());
    }      
    
    @Test
    public void mockUpdate()
    { 
        Mockito.when(repository.findOne(verifis.getId())).thenReturn(verifis);        
        Verifis updated = new Verifis();        
        updated.setAccessPin("1234");
        updated.setDescription("description1");
        updated.setDeviceId("de56");
        updated.setExpired(false);
        updated.setName("name");
        updated.setPlatform(MobilePlatform.ANDROID);
        updated.setStatus(ActivationStatus.ACTIVE);
        updated.setValidUntil(Utilities.currentEpochPlus(3600));
        updated.setVersion("1.0.1");        
        updated.setDeleted(true);
        Mockito.when(repository.save(verifis)).thenReturn(updated);
        verifis = service.update(verifis.getId(), updated);
        assertNotNull(verifis);        
        assertEquals(true, verifis.isDeleted());        
        assertEquals("de56", verifis.getDeviceId());        
        assertEquals("description1", verifis.getDescription());        
        assertEquals(MobilePlatform.ANDROID, verifis.getPlatform());        
        assertEquals(ActivationStatus.ACTIVE, verifis.getStatus());        
    }      
    
    @Test
    public void mockDelete()
    { 
        Mockito.doNothing().when(repository).delete(verifis);
        service.delete(verifis);
        Mockito.verify(repository, Mockito.times(1)).delete(verifis);
        
    }      
    
    @Test
    public void mockInsert()
    { 
        Mockito.doReturn(verifis).when(repository).save(verifis);               
        service.insert(verifis);
        Mockito.verify(repository, Mockito.times(1)).save(verifis);
    }     
    
    @Test
    public void mockFindByVid()
    { 
        Mockito.when(repository.findByVid(verifis.getId())).thenReturn(verifis);
        Verifis ver = service.find(verifis.getId());
        assertNotNull(ver);
        assertEquals(verifis.getVid(), ver.getVid());                
    }    

    @Test
    public void mockFindByVidAndDeviceId()
    {
        Mockito.when(repository.findByVidAndDeviceId(verifis.getId(), verifis.getDeviceId())).thenReturn(verifis);
        Verifis ver = service.find(verifis.getId(), verifis.getDeviceId());
        assertNotNull(ver);
        assertEquals(verifis.getVid(), ver.getVid());           
        assertEquals(verifis.getDeviceId(), ver.getDeviceId());           
        assertEquals("2.0.1", ver.getVersion());
    }
    
    @Test
    public void mockIsVidAndDeviceIdMatch()
    {
        Mockito.when(repository.findByVidAndDeviceId(verifis.getId(), verifis.getDeviceId())).thenReturn(verifis);
        boolean result = service.isVidAndDeviceIdMatch(verifis.getId(), verifis.getDeviceId());
        assertEquals(true, result);                
    }    
    
    @Test
    public void mockFindAllByDeviceId() throws InterruptedException
    {
        // create more verifis with the same "deviceId"
        Thread.sleep(2000);
        createMockVerifis("deviceId");
        Thread.sleep(2000);
        createMockVerifis("deviceId");        
        //Mockito.when(repository.findAllByDeviceId(verifis.getDeviceId())).thenReturn(list);
        assertNotNull(list);
        assertEquals(3, list.size());
        list.forEach((item)->{assertTrue(item.getDeviceId().equals("deviceId"));});        
    }
    
    @Test
    public void mockFindAllByDeviceIdOrderedByCreation() throws InterruptedException
    {
        // create more verifis with the same "deviceId"        
        logger.info("Simulating creating second verifis...");
        Thread.sleep(2000);
        createMockVerifis("deviceId");
        logger.info("Simulating creating third verifis...");
        Thread.sleep(2000);
        createMockVerifis("deviceId");        
        QVerifis verifis = new QVerifis("verifis");
        Predicate predicate = verifis.deviceId.eq("deviceId").and(verifis.status.eq(ActivationStatus.ACTIVE));        
        Sort sort = new Sort(Sort.Direction.DESC, "createdAt");        
        //Mockito.when(repository.findAll(predicate, sort)).thenReturn(list);
        assertNotNull(list);
        assertEquals(3, list.size());
        list.forEach((item)->{assertTrue(item.getDeviceId().equals("deviceId"));});       
        assertTrue(list.get(0).getCreatedAt().before(list.get(1).getCreatedAt()));
        assertTrue(list.get(0).getCreatedAt().before(list.get(2).getCreatedAt()));
        assertTrue(list.get(1).getCreatedAt().before(list.get(2).getCreatedAt()));
    }    
    
    @Test
    public void mockInitializeVerifis()
    {
        BootRequestParameter brp = new BootRequestParameter();
        brp.setDeviceId(verifis.getDeviceId());
        brp.setPlatform(MobilePlatform.IOS);
        brp.setVer(verifis.getVersion());
        brp.setVid(verifis.getVid());
        verifis.setStatus(ActivationStatus.INACTIVE);
        Mockito.when(verifier.exists(verifis.getVid(), verifis.getDeviceId())).thenReturn(verifis);
        Mockito.when(verifier.isActive(verifis)).thenReturn(Boolean.TRUE);
        BootResponse br = service.initializeVerifis(brp);
        assertNotNull(br);        
        assertEquals(MobilePlatform.IOS.getDescription(), br.getPlatform());
        assertEquals(false, br.getForceUpdate());
    }    
    
    @Test(expected = VerifisIdNotActiveException.class)
    public void mockInitializeVerifisThrowsVerifisIdNotActiveException()
    {
        BootRequestParameter brp = new BootRequestParameter();
        brp.setDeviceId(verifis.getDeviceId());
        brp.setPlatform(MobilePlatform.IOS);
        brp.setVer(verifis.getVersion());
        brp.setVid(verifis.getVid());
        verifis.setStatus(ActivationStatus.INACTIVE);
        Mockito.when(verifier.exists(verifis.getVid(), verifis.getDeviceId())).thenReturn(verifis);
        Mockito.when(verifier.isActive(verifis)).thenThrow(new VerifisIdNotActiveException("Verifis ID is not active"));
        BootResponse br = service.initializeVerifis(brp);
        assertNull(br);        
    }
    
    @Test
    public void mockInitializeToken()
    {        
        DeviceIdParameter dp = new DeviceIdParameter();
        dp.setDeviceId(verifis.getDeviceId());                        
        VerifisToken token  = service.initializeToken(dp);
        assertNotNull(token);       
        assertNotNull(token.getAccessPin());   
        assertNotNull(token.getValidUntil());   
        assertNotNull(token.getVid());   
    }     
    
    @Test
    public void mockGetUnencryptedToken()
    {        
        VerifisTokenParameter vtp = new VerifisTokenParameter();
        vtp.setAccessToken("1234");
        vtp.setDeviceId(verifis.getDeviceId());
        UnencryptedTokenItem tokenItem = new UnencryptedTokenItem();
        tokenItem.setAccessToken(vtp.getAccessToken());
        tokenItem.setDeviceId(verifis.getDeviceId());
        tokenItem.setVid(verifis.getVid());        
        Mockito.when(cache.get(verifis.getVid())).thenReturn(tokenItem);
        HttpEntity<byte[]> response = service.getUnencryptedToken(verifis.getVid(), vtp);
        byte[] data = service.getUnencryptedToken(verifis.getVid(), vtp).getBody();
        assertNotNull(data);               
        assertEquals(MediaType.APPLICATION_OCTET_STREAM, response.getHeaders().getContentType());
        assertEquals(data.length, response.getHeaders().getContentLength());
    }       
    
    @Test(expected = VerifisIdNotFoundException.class)
    public void mockGetUnencryptedTokenThrowsVerifisIdNotFoundException()
    {        
        VerifisTokenParameter vtp = new VerifisTokenParameter();
        vtp.setAccessToken("1234");
        vtp.setDeviceId(verifis.getDeviceId());
        HttpEntity<byte[]> response = service.getUnencryptedToken(verifis.getVid(), vtp);        
        assertNull(response);                       
    }     
    
    @Test(expected = AccessTokenMismatchException.class)
    public void mockGetUnencryptedTokenThrowsAccessTokenMismatchException()
    {        
        VerifisTokenParameter vtp = new VerifisTokenParameter();
        vtp.setAccessToken("1234");
        vtp.setDeviceId(verifis.getDeviceId());
        UnencryptedTokenItem tokenItem = new UnencryptedTokenItem();
        tokenItem.setAccessToken("1111"); // invalid access pin
        tokenItem.setDeviceId(verifis.getDeviceId());
        tokenItem.setVid(verifis.getVid());        
        Mockito.when(cache.get(verifis.getVid())).thenReturn(tokenItem);
        HttpEntity<byte[]> response = service.getUnencryptedToken(verifis.getVid(), vtp);        
        assertNull(response);  
    }       
    
    @Test
    public void mockCreateTokenReport()
    {        
        VerifisTokenParameter vtp = new VerifisTokenParameter();
        vtp.setAccessToken("1234");
        vtp.setDeviceId(verifis.getDeviceId());
        UnencryptedTokenItem tokenItem = new UnencryptedTokenItem();
        tokenItem.setAccessToken(vtp.getAccessToken());
        tokenItem.setDeviceId(verifis.getDeviceId());
        tokenItem.setVid(verifis.getVid());        
        tokenItem.setVerifis(verifis);
        Mockito.when(cache.get(verifis.getVid())).thenReturn(tokenItem);
        Mockito.doReturn(true).when(verifier).deactivate(verifis.getVid(), verifis.getDeviceId());
        Mockito.doReturn(verifis).when(repository).save(verifis);                        
        boolean response = service.createTokenReport(verifis.getVid(), verifis.getDeviceId());        
        Mockito.verify(repository, Mockito.times(1)).save(verifis);  
        Mockito.verify(verifier, Mockito.times(1)).deactivate(verifis.getVid(), verifis.getDeviceId());         
        assertNotNull(response);               
        assertEquals(true, response);        
    }       
    
    @Test(expected = VerifisIdNotFoundException.class)
    public void mockCreateTokenReportThrowsVerifisIdNotFoundException()
    {        
        VerifisTokenParameter vtp = new VerifisTokenParameter();
        vtp.setAccessToken("1234");
        vtp.setDeviceId(verifis.getDeviceId());
        UnencryptedTokenItem tokenItem = new UnencryptedTokenItem();
        tokenItem.setAccessToken(vtp.getAccessToken());
        tokenItem.setDeviceId(verifis.getDeviceId());
        tokenItem.setVid(verifis.getVid());        
        tokenItem.setVerifis(verifis);
        //Mockito.doReturn(tokenItem).when(cache).get("notfound");
        //Mockito.doReturn(true).when(verifier).deactivate(verifis.getVid(), verifis.getDeviceId());
        //Mockito.doReturn(verifis).when(repository).save(verifis);                        
        boolean response = service.createTokenReport(verifis.getVid(), verifis.getDeviceId());      
        Mockito.verify(cache, Mockito.times(1)).get(verifis.getVid());
        Mockito.verify(repository, Mockito.times(0)).save(verifis);  
        Mockito.verify(verifier, Mockito.times(0)).deactivate(verifis.getVid(), verifis.getDeviceId());         
        assertNull(response);       
    } 

    @Test(expected = VerifisDeviceMismatchException.class)
    public void mockCreateTokenReportThrowsVerifisDeviceMismatchException()
    {        
        VerifisTokenParameter vtp = new VerifisTokenParameter();
        vtp.setAccessToken("1234");
        vtp.setDeviceId(verifis.getDeviceId());
        UnencryptedTokenItem tokenItem = new UnencryptedTokenItem();
        tokenItem.setAccessToken(vtp.getAccessToken());
        tokenItem.setDeviceId(verifis.getDeviceId());
        tokenItem.setVid(verifis.getVid());        
        tokenItem.setVerifis(verifis);
        Mockito.doReturn(tokenItem).when(cache).get(verifis.getVid());
        //Mockito.doReturn(true).when(verifier).deactivate(verifis.getVid(), verifis.getDeviceId());
        //Mockito.doReturn(verifis).when(repository).save(verifis);                        
        boolean response = service.createTokenReport(verifis.getVid(), "mismatch");      
        Mockito.verify(cache, Mockito.times(1)).get(verifis.getVid());
        Mockito.verify(repository, Mockito.times(0)).save(verifis);  
        Mockito.verify(verifier, Mockito.times(0)).deactivate(verifis.getVid(), verifis.getDeviceId());         
        assertNull(response);   
    }     
    
    public void mockGetTokenReport()
    {        
        Mockito.doReturn(verifis).when(verifier).getExplicitly(verifis.getVid());
        Mockito.doReturn(true).when(verifier).isTokenReportSubmitted(verifis);       
        TokenReport tr = service.getTokenReport(verifis.getVid());
        Mockito.verify(verifier, Mockito.times(1)).getExplicitly(verifis.getVid());
        Mockito.verify(verifier, Mockito.times(1)).isTokenReportSubmitted(verifis);               
        assertNotNull(tr);
        assertEquals(verifis.getDeviceId(), tr.getDeviceId());   
        assertEquals(OperationStatus.SUCCESS.getDescription(), tr.getStatus());                   
    }
    
    public void mockVerifyInstallation()
    {                
        Mockito.doReturn(true).when(verifier).isValidInstallation(verifis.getVid(), verifis.getDeviceId());       
        ResponseEntity response = service.verifyInstallation(verifis.getVid(), verifis.getDeviceId());
        Mockito.verify(verifier, Mockito.times(1)).isValidInstallation(verifis.getVid(), verifis.getDeviceId());        
        assertNotNull(response);        
        assertEquals(200, response.getStatusCodeValue());
    }    
    
    public void mockResetInstallation()
    {                
        Mockito.doReturn(verifis).when(verifier).exists(verifis.getVid(), verifis.getDeviceId());       
        Mockito.doReturn(verifis).when(repository).save(verifis);   
        ResponseEntity response = service.resetInstallation(verifis.getVid(), verifis.getDeviceId());
        Mockito.verify(verifier, Mockito.times(1)).exists(verifis.getVid(), verifis.getDeviceId());        
        Mockito.verify(service, Mockito.times(1)).save(verifis);  
        assertNotNull(response);        
        assertEquals(200, response.getStatusCodeValue());
        assertEquals(false, verifis.isExpired());
        assertEquals(ActivationStatus.INACTIVE, verifis.getStatus());
    }       
    
}

