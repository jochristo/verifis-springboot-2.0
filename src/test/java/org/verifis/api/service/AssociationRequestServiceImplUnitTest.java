package org.verifis.api.service;

import com.google.common.io.Resources;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.verifis.api.Constants;
import org.verifis.api.component.LogoComponent;
import org.verifis.api.domain.Association;
import org.verifis.api.exception.AssociationRequestExpiredException;
import org.verifis.api.exception.RequestIdNotFoundException;
import org.verifis.api.model.AssociationProvider;
import org.verifis.api.model.AssociationRequestData;
import org.verifis.api.model.serializable.AssociationRequestItem;
import org.verifis.api.model.serializable.AssociationRequestObject;
import org.verifis.api.service.impl.AssociationRequestServiceImpl;
import org.verifis.core.cache.impl.AssociationRequestItemCacheable;
import org.verifis.core.utility.Utilities;

/**
 * Unit test of component AssociationRequestServiceImp.
 * Injected mock objects: IAssociationService, AssociationRequestServiceImpl, LogoComponent
 * @author Admin
 */
@RunWith(MockitoJUnitRunner.class)
public class AssociationRequestServiceImplUnitTest
{
    private static final Logger logger = LoggerFactory.getLogger(AssociationRequestServiceImplUnitTest.class);      
    @InjectMocks private AssociationRequestServiceImpl component;        
    @Mock private IAssociationService service;
    @Mock private AssociationRequestItemCacheable cache;
    @Mock private LogoComponent logoComponent;
    
    // dummy data objects
    private AssociationRequestItem item;        
    private final Association associationPaypalOne;
    private final Association associationPaypalTwo;    

    public AssociationRequestServiceImplUnitTest() throws URISyntaxException, IOException
    {
        // read json association resources into Association objects
        Gson gson = new Gson();
        URL url = Resources.getResource("associationPaypalOne.json");
        File file = new File(url.toURI());
        String contents = FileUtils.readFileToString(file, "UTF-8");               
        associationPaypalOne = gson.fromJson(contents, Association.class);
        logger.info("Initializing new association id#" + associationPaypalOne.getAssocId()+ " with vid: " + associationPaypalOne.getVid());
        
        url = Resources.getResource("associationPaypalTwo.json");
        file = new File(url.toURI());
        contents = FileUtils.readFileToString(file, "UTF-8");               
        associationPaypalTwo = gson.fromJson(contents, Association.class);        
        logger.info("Initializing new association id#" + associationPaypalTwo.getAssocId()+ " with vid: " + associationPaypalTwo.getVid());
    }
        
    
    @Before
    public void before()
    {
        assertNotNull(associationPaypalOne);
        assertNotNull(associationPaypalTwo);
        
        int validUntil = Utilities.currentEpochPlus(Constants.Cache.ASSOCIATION_REQUEST_EXPIRY_SECONDS); 
        item = new AssociationRequestItem();
        item.setVid(associationPaypalOne.getVid());        
        item.setExpires(validUntil);                   
    }
    
    @Test
    public void mockCreate()
    { 
        AssociationRequestObject object = component.create(associationPaypalOne.getVid());
        assertNotNull(object);        
        assertNotNull(object.getExpires());        
        assertNotNull(object.getRequestId());        
        Date now = new Date();
        Date date = new Date(object.getExpires());
        assertTrue(date.before(now));
    }      
    
    @Test
    public void mockVerify() throws InterruptedException
    {         
        Mockito.doReturn(item.getRequestId()).when(cache).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.doReturn(item).when(cache).get(item.getRequestId());               
        AssociationRequestObject object = component.verify(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.verify(cache, Mockito.times(1)).get(item.getRequestId());                     
        assertNotNull(object);        
        assertNotNull(object.getExpires());        
        assertNotNull(object.getRequestId());      
        assertEquals(item.getExpires(), object.getExpires());
        assertEquals(item.getRequestId(), object.getRequestId());        
    }    
    
    @Test(expected = RequestIdNotFoundException.class)
    public void mockVerifyThrowsRequestIdNotFoundException() throws InterruptedException
    {                 
        AssociationRequestObject object = component.verify(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());    
        Mockito.verify(cache, Mockito.times(0)).get(item.getRequestId());     
        assertNull(object);                
    }  

    @Test(expected = AssociationRequestExpiredException.class)
    public void mockVerifyThrowsAssociationRequestExpiredException() throws InterruptedException
    {         
        Mockito.doReturn(item.getRequestId()).when(cache).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());        
        AssociationRequestObject object = component.verify(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.verify(cache, Mockito.times(1)).get(item.getRequestId());                     
        assertNull(object);           
    }      
    
    @Test
    public void mockExtend()
    {         
        item.setExpires(Utilities.currentEpochPlus(200));
        Mockito.doReturn(item.getRequestId()).when(cache).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.doReturn(item).when(cache).get(item.getRequestId());
        AssociationRequestObject object = component.extend(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.verify(cache, Mockito.times(1)).get(item.getRequestId());                     
        assertNotNull(object);        
        assertNotNull(object.getExpires());        
        assertNotNull(object.getRequestId());              
        assertEquals(item.getRequestId(), object.getRequestId());        
    }  
    
    @Test(expected = RequestIdNotFoundException.class)
    public void mockExtendThrowsRequestIdNotFoundException() throws InterruptedException
    {                 
        AssociationRequestObject object = component.extend(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());    
        Mockito.verify(cache, Mockito.times(0)).get(item.getRequestId());     
        assertNull(object);                
    }  

    @Test(expected = AssociationRequestExpiredException.class)
    public void mockExtendThrowsAssociationRequestExpiredException() throws InterruptedException
    {         
        Mockito.doReturn(item.getRequestId()).when(cache).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());        
        AssociationRequestObject object = component.extend(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.verify(cache, Mockito.times(1)).get(item.getRequestId());                     
        assertNull(object);           
    }     

    @Test
    public void mockDelete()
    { 
        Mockito.doReturn(item.getRequestId()).when(cache).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.doReturn(item).when(cache).get(item.getRequestId());
        ResponseEntity object = component.delete(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.verify(cache, Mockito.times(1)).get(item.getRequestId());             
        Mockito.verify(cache, Mockito.times(1)).setExpired(item.getRequestId());
        Mockito.verify(cache, Mockito.times(1)).setExpired(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());        
        assertNotNull(object);        
        assertEquals(200, object.getStatusCodeValue());                   
    }      
    
    @Test(expected = RequestIdNotFoundException.class)
    public void mockDeleteThrowsRequestIdNotFoundException() throws InterruptedException
    {                 
        ResponseEntity object = component.delete(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());    
        Mockito.verify(cache, Mockito.times(0)).get(item.getRequestId());     
        assertNull(object);                
    }  

    @Test(expected = AssociationRequestExpiredException.class)
    public void mockDeleteThrowsAssociationRequestExpiredException() throws InterruptedException
    {         
        Mockito.doReturn(item.getRequestId()).when(cache).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());        
        ResponseEntity object = component.delete(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.verify(cache, Mockito.times(1)).get(item.getRequestId());                     
        assertNull(object);           
    }     
    
    @Test
    public void mockGet()
    {         
        Mockito.doReturn(item.getRequestId()).when(cache).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.doReturn(item).when(cache).get(item.getRequestId());
        AssociationProvider provider = new AssociationProvider();
        provider.setLogo("logo");
        provider.setService("service");
        provider.setUrl("url");
        Mockito.doReturn(provider).when(logoComponent).getRandomProvider();
        AssociationRequestData object = component.get(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.verify(cache, Mockito.times(1)).get(item.getRequestId());             
        Mockito.verify(logoComponent, Mockito.times(1)).getRandomProvider();            
        assertNotNull(object);        
        assertNotNull(object.getLogo());        
        assertNotNull(object.getUrl());        
        assertNotNull(object.getService());
        assertEquals(provider.getLogo(), object.getLogo());  
        assertEquals(provider.getUrl(), object.getUrl());  
        assertEquals(provider.getService(), object.getService());    
    }  
    
    @Test(expected = RequestIdNotFoundException.class)
    public void mockGetFailsNotExistsWithRequestIdNotFoundException() throws InterruptedException
    {                 
        AssociationRequestData object = component.get(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());    
        Mockito.verify(cache, Mockito.times(0)).get(item.getRequestId());     
        assertNull(object);                
    }  

    @Test(expected = RequestIdNotFoundException.class)
    public void mockGetFailsExpiredWithRequestIdNotFoundException() throws InterruptedException
    {         
        Mockito.doReturn(item.getRequestId()).when(cache).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());        
        AssociationRequestData object = component.get(item.getRequestId());        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());
        Mockito.verify(cache, Mockito.times(1)).get(item.getRequestId());                     
        assertNull(object);           
    }     

    @Test
    public void mockAccept()
    { 
        String code = "123456";
        Mockito.doReturn(item).when(cache).get(item.getRequestId());
        AssociationProvider provider = new AssociationProvider();
        provider.setLogo("logo");
        provider.setService("service");
        provider.setUrl("url");
        Mockito.doReturn(provider).when(logoComponent).getRandomProvider();                       
        ResponseEntity object = component.accept(item.getRequestId(), code);                
        Mockito.verify(cache, Mockito.times(1)).get(item.getRequestId());   
        Mockito.verify(logoComponent, Mockito.times(1)).getRandomProvider();                
        assertNotNull(object);                
        assertEquals(200, object.getStatusCodeValue());         
    }  
    
    @Test(expected = RequestIdNotFoundException.class)
    public void mockAcceptThrowsRequestIdNotFoundException() throws InterruptedException
    {                
        String code = "123456";
        ResponseEntity object = component.accept(item.getRequestId(), code);        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());            
        assertNull(object);                
    }      

    @Test
    public void mockReject()
    { 
        String code = "123456";
        Mockito.doReturn(item).when(cache).get(item.getRequestId());
        AssociationProvider provider = new AssociationProvider();
        provider.setLogo("logo");
        provider.setService("service");
        provider.setUrl("url");
        Mockito.doReturn(provider).when(logoComponent).getRandomProvider();                       
        ResponseEntity object = component.reject(item.getRequestId(), code);                
        Mockito.verify(cache, Mockito.times(1)).get(item.getRequestId());   
        Mockito.verify(logoComponent, Mockito.times(1)).getRandomProvider();                
        assertNotNull(object);                
        assertEquals(200, object.getStatusCodeValue());             
    }      
    
    @Test(expected = RequestIdNotFoundException.class)
    public void mockRejectThrowsRequestIdNotFoundException() throws InterruptedException
    {                 
        String code = "123456";
        ResponseEntity object = component.reject(item.getRequestId(), code);        
        Mockito.verify(cache, Mockito.times(1)).get(Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE, item.getRequestId());            
        assertNull(object);                
    }      
    
}
