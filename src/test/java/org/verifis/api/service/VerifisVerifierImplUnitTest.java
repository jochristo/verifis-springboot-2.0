package org.verifis.api.service;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.verifis.api.domain.Verifis;
import org.verifis.api.exception.InvalidVerifisInstallationException;
import org.verifis.api.exception.VerifisIdNotActiveException;
import org.verifis.api.exception.VerifisIdNotFoundException;
import org.verifis.api.exception.VerifisReportNotYetSubmittedException;
import org.verifis.api.model.enumeration.ActivationStatus;
import org.verifis.api.model.enumeration.MobilePlatform;
import org.verifis.api.service.impl.VerifisVerifierImpl;
import org.verifis.core.utility.Utilities;

/**
 * Unit test of component VerifisVerifierImp.
 * Injected mock objects: IVerifisService
 * @author Admin
 */
@RunWith(MockitoJUnitRunner.class)
public class VerifisVerifierImplUnitTest
{
    @InjectMocks private VerifisVerifierImpl verifier;
    @Mock private IVerifisService service;      
    private Verifis verifis = null;    
    private List<Verifis> list = null;    
    private static final Logger logger = LoggerFactory.getLogger(VerifisServiceImplUnitTest.class);       
    
    @Before
    public void before()
    {         
        list = new ArrayList();        
        createMockVerifis("deviceId");        
    }
    
    @After
    public void after()
    {
        
    }    

    protected void createMockVerifis(String deviceId)
    {
        verifis = new Verifis();
        verifis.setAccessPin("1234");
        verifis.setDescription("description");
        verifis.setDeviceId(deviceId);
        verifis.setExpired(false);
        verifis.setName(deviceId);
        verifis.setPlatform(MobilePlatform.IOS);
        verifis.setStatus(ActivationStatus.ACTIVE);
        verifis.setValidUntil(Utilities.currentEpochPlus(3600));
        verifis.setVersion("2.0.1");
        
        list.add(verifis);
    }          
    
    @Test
    public void mockGetExplicitly()
    { 
        Mockito.doReturn(verifis).when(service).find(verifis.getVid());
        Verifis ver = verifier.getExplicitly(verifis.getVid());
        Mockito.verify(service, Mockito.times(1)).find(verifis.getVid());     
        assertNotNull(ver);        
        assertEquals(verifis.getVid(), ver.getVid());     
    }       
    
    @Test(expected = VerifisIdNotFoundException.class)
    public void mockGetExplicitlyThrowsVerifisIdNotFoundException()
    { 
        //Mockito.doReturn(verifis).when(service).find(verifis.getVid());
        Verifis ver = verifier.getExplicitly("notfound");
        Mockito.verify(service, Mockito.times(1)).find("notfound");     
        assertNull(ver);                
    }    
    
    @Test
    public void mockExistsInstallation()
    { 
        Mockito.doReturn(verifis).when(service).find(verifis.getVid());
        boolean result = verifier.existsInstallation(verifis.getVid(), verifis.getDeviceId());        
        Mockito.verify(service, Mockito.times(1)).find(verifis.getVid());         
        assertNotNull(result);        
        assertEquals(true, result);     
    }       
    
    @Test(expected = VerifisIdNotFoundException.class)
    public void mockExistsInstallationThrowsVerifisIdNotFoundException()
    { 
        //Mockito.doReturn(verifis).when(service).find(verifis.getVid());
        boolean result = verifier.existsInstallation("notfound", verifis.getDeviceId());        
        Mockito.verify(service, Mockito.times(1)).find("notfound");   
        assertNull(result);                
    }    
    
    @Test
    public void mockExists()
    { 
        Mockito.doReturn(verifis).when(service).find(verifis.getVid());
        Verifis result = verifier.exists(verifis.getVid(), verifis.getDeviceId());        
        Mockito.verify(service, Mockito.times(1)).find(verifis.getVid());         
        assertNotNull(result);        
        assertEquals(verifis.getVid(), result.getVid());     
        assertEquals(verifis.getDeviceId(), result.getDeviceId());     
    }       
    
    @Test(expected = VerifisIdNotFoundException.class)
    public void mockExistsThrowsVerifisIdNotFoundException()
    { 
        //Mockito.doReturn(verifis).when(service).find(verifis.getVid());
        Verifis result = verifier.exists("notfound", verifis.getDeviceId());        
        Mockito.verify(service, Mockito.times(1)).find("notfound");   
        assertNull(result);                
    }       
    
    @Test
    public void mockIsActive()
    {         
        boolean result = verifier.isActive(verifis);                
        assertNotNull(result);        
        assertEquals(true, result);     
    }       
    
    @Test(expected = VerifisIdNotActiveException.class)
    public void mockIsActiveThrowsVerifisIdNotActiveException()
    {         
        verifis.setStatus(ActivationStatus.INACTIVE);
        boolean result = verifier.isActive(verifis);                 
        assertNull(result);                
    }     
    
    @Test
    public void mockDeactivate() throws InterruptedException
    { 
        // create more verifis with the same "deviceId"        
        logger.info("Simulating creating second verifis...");
        Thread.sleep(2000);
        createMockVerifis("deviceId");
        logger.info("Simulating creating third verifis...");
        Thread.sleep(2000);
        createMockVerifis("deviceId");        
        Mockito.doReturn(list).when(service).findAllByDeviceId("deviceId");        
        boolean result = verifier.deactivate("deviceId");
        Mockito.verify(service, Mockito.times(1)).findAllByDeviceId("deviceId");    
        assertNotNull(result);        
        assertEquals(true, result);     
        list.forEach((element)->{ 
            assertTrue(element.getStatus().equals(ActivationStatus.INACTIVE)); 
            Mockito.verify(service, Mockito.times(1)).update(element.getId(), element);
        });        
    }       
    
    @Test
    public void mockDeactivateByVidAndDeviceId() throws InterruptedException
    { 
        // create more verifis with the same "deviceId"        
        logger.info("Simulating creating second verifis...");
        Thread.sleep(2000);
        createMockVerifis("deviceId");
        logger.info("Simulating creating third verifis...");
        Thread.sleep(2000);
        createMockVerifis("deviceId");        
        Mockito.doReturn(list).when(service).findAllByDeviceIdOrderedByCreation("deviceId");        
        boolean result = verifier.deactivate(verifis.getVid(), "deviceId");
        Mockito.verify(service, Mockito.times(1)).findAllByDeviceIdOrderedByCreation("deviceId");    
        assertNotNull(result);        
        assertEquals(true, result);     
        list.forEach((element)->{ 
            if(element.getVid().equals(verifis.getVid()) == false){
                assertTrue(element.getStatus().equals(ActivationStatus.INACTIVE));             
                Mockito.verify(service, Mockito.times(1)).update(element.getId(), element);
            }
        });        
        Mockito.verify(service, Mockito.times(0)).update(verifis.getVid(), verifis); // existing installation is not updated: true
    }    
    
    @Test
    public void mockIsTokenReportSubmitted()
    {         
        boolean result = verifier.isTokenReportSubmitted(verifis);                
        assertNotNull(result);        
        assertEquals(true, result);     
    }     
    
    @Test(expected = VerifisReportNotYetSubmittedException.class)
    public void mockIsTokenReportSubmittedThrowsVerifisReportNotYetSubmittedException()
    {         
        verifis.setStatus(ActivationStatus.INACTIVE);
        boolean result = verifier.isTokenReportSubmitted(verifis);                
        assertNull(result);                
    }       
    
    @Test
    public void mockIsExpired()
    {
        Mockito.doReturn(verifis).when(service).find(verifis.getVid());
        boolean result = verifier.isExpired(verifis.getVid());
        Mockito.verify(service, Mockito.times(1)).find(verifis.getVid());
        assertNotNull(result);        
        assertEquals(false, result);          
    }
    
    @Test
    public void mockIsValidInstallation()
    {         
        Mockito.doReturn(verifis).when(service).find(verifis.getVid(), verifis.getDeviceId());
        boolean result = verifier.isValidInstallation(verifis.getVid(), verifis.getDeviceId());               
        assertNotNull(result);        
        assertEquals(true, result);     
    }     
    
    @Test(expected = InvalidVerifisInstallationException.class)
    public void mockIsValidInstallationThrowsInvalidVerifisInstallationException() 
    {         
        //Mockito.doReturn(verifis).when(service).find(verifis.getVid(), verifis.getDeviceId());                
        boolean result = verifier.isValidInstallation(verifis.getVid(), "unknown");
        assertNull(result);     
        
        Mockito.doReturn(verifis).when(service).find(verifis.getVid(), verifis.getDeviceId());        
        result = verifier.isValidInstallation("unknown", verifis.getDeviceId());
        assertNull(result);         
    }     
}
