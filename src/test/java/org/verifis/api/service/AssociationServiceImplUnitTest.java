package org.verifis.api.service;

import com.google.common.io.Resources;
import com.google.gson.Gson;
import com.querydsl.core.types.Predicate;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
//import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.verifis.api.domain.Association;
import org.verifis.api.domain.QAssociation;
import org.verifis.api.domain.Verifis;
import org.verifis.api.exception.AssociationVerifisMismatchException;
import org.verifis.api.exception.ResourceNotFoundException;
import org.verifis.api.exception.VerifisIdNotFoundException;
import org.verifis.api.model.Associations;
import org.verifis.api.model.bind.AssociationsRequestParameter;
import org.verifis.api.model.enumeration.AssociationStatus;
import org.verifis.api.repository.IAssociationRepository;
import org.verifis.api.repository.extension.OffsetLimitPageRequest;
import org.verifis.api.service.impl.AssociationServiceImpl;

/**
 * Unit test of component AssociationServiceImpl.
 * Injected mock objects: IVerifisService, IAssociationRepository, AssociationRequestItemCacheable, LogoComponent
 * @author Admin
 */
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class AssociationServiceImplUnitTest
{
    private static final Logger logger = LoggerFactory.getLogger(AssociationServiceImplUnitTest.class);  
    @InjectMocks private AssociationServiceImpl service;
    @Mock private IVerifisService verifisService;      
    @Mock private IAssociationRepository repository;          
    private List<Association> list = null;        

    //resource data
    private final Association associationOne;
    private final Association associationTwo;
    private final Association associationThree;
    private final Association associationOne6e9bfc;
    private final Association associationTwo6e9bfc;
    private final Association associationInactiveDeleted;
    private final Association associationPaypalOne;
    private final Association associationPaypalTwo;
    
    public AssociationServiceImplUnitTest() throws URISyntaxException, IOException
    {
        // read json association resources into Association objects
        Gson gson = new Gson();
        URL url = Resources.getResource("associationOne.json");
        File file = new File(url.toURI());
        String contents = FileUtils.readFileToString(file, "UTF-8");               
        associationOne = gson.fromJson(contents, Association.class);
        logger.info("Initializing new association id#" + associationOne.getAssocId()+ " with vid: " + associationOne.getVid());
        
        url = Resources.getResource("associationTwo.json");
        file = new File(url.toURI());
        contents = FileUtils.readFileToString(file, "UTF-8");               
        associationTwo = gson.fromJson(contents, Association.class);
        logger.info("Initializing new association id#" + associationTwo.getAssocId()+ " with vid: " + associationTwo.getVid());
        
        url = Resources.getResource("associationThree.json");
        file = new File(url.toURI());
        contents = FileUtils.readFileToString(file, "UTF-8");               
        associationThree = gson.fromJson(contents, Association.class);
        logger.info("Initializing new association id#" + associationThree.getAssocId()+ " with vid: " + associationThree.getVid());
        
        url = Resources.getResource("associationOne6e9bfc.json");
        file = new File(url.toURI());
        contents = FileUtils.readFileToString(file, "UTF-8");               
        associationOne6e9bfc = gson.fromJson(contents, Association.class);
        logger.info("Initializing new association id#" + associationOne6e9bfc.getAssocId()+ " with vid: " + associationOne6e9bfc.getVid());
        
        url = Resources.getResource("associationTwo6e9bfc.json");
        file = new File(url.toURI());
        contents = FileUtils.readFileToString(file, "UTF-8");               
        associationTwo6e9bfc = gson.fromJson(contents, Association.class);
        logger.info("Initializing new association id#" + associationTwo6e9bfc.getAssocId()+ " with vid: " + associationTwo6e9bfc.getVid());
        
        url = Resources.getResource("associationInactiveDeleted.json");
        file = new File(url.toURI());
        contents = FileUtils.readFileToString(file, "UTF-8");               
        associationInactiveDeleted = gson.fromJson(contents, Association.class);       
        logger.info("Initializing new association id#" + associationInactiveDeleted.getAssocId()+ " with vid: " + associationInactiveDeleted.getVid());
        
        url = Resources.getResource("associationPaypalOne.json");
        file = new File(url.toURI());
        contents = FileUtils.readFileToString(file, "UTF-8");               
        associationPaypalOne = gson.fromJson(contents, Association.class);
        logger.info("Initializing new association id#" + associationPaypalOne.getAssocId()+ " with vid: " + associationPaypalOne.getVid());

        url = Resources.getResource("associationPaypalTwo.json");
        file = new File(url.toURI());
        contents = FileUtils.readFileToString(file, "UTF-8");               
        associationPaypalTwo = gson.fromJson(contents, Association.class);        
        logger.info("Initializing new association id#" + associationPaypalTwo.getAssocId()+ " with vid: " + associationPaypalTwo.getVid());
    }
        
    
    @Before
    public void before() throws IOException, URISyntaxException
    {   
        list = new ArrayList();
        
        assertNotNull(associationOne);
        assertNotNull(associationTwo);
        assertNotNull(associationThree);
        assertNotNull(associationOne6e9bfc);
        assertNotNull(associationTwo6e9bfc);
        
        // add to list
        list.add(associationOne);
        list.add(associationTwo);
        list.add(associationThree);
        list.add(associationOne6e9bfc);
        list.add(associationTwo6e9bfc);        
    }
    
    @After
    public void after()
    {
        
    } 
    
    @Test
    public void mockGet()
    { 
        Mockito.doReturn(list).when(repository).findAll();
        List<Association> data = service.get();
        Mockito.verify(repository, Mockito.times(1)).findAll();
        assertNotNull(data);
        assertEquals(5, data.size());        
    }     
    
    @Test
    public void mockFindOne()
    { 
        Mockito.doReturn(associationOne).when(repository).findOne(associationOne.getId());
        Association data = service.findOne(associationOne.getId());
        Mockito.verify(repository, Mockito.times(1)).findOne(associationOne.getId());
        assertNotNull(data);
        assertEquals(associationOne.getAssocId(), data.getAssocId());     
    }  

    @Test
    public void mockFindSingle()
    { 
        String assocId = "9eb3de3b8c40b2264db21a35821a24d7"; //associationOne.getAssocId()
        Mockito.doReturn(associationOne).when(repository).findByAssocId(assocId);
        Association data = service.findSingle(assocId);
        Mockito.verify(repository, Mockito.times(1)).findByAssocId(assocId);
        assertNotNull(data);
        assertEquals(assocId, data.getAssocId()); 
    }    
    
    @Test
    public void mockSave()
    { 
        String assocId = "9eb3de3b8c40b2264db21a35821a24d7"; //associationOne.getAssocId()
        Mockito.doReturn(associationOne).when(repository).save(associationOne);
        Association data = service.save(associationOne);
        Mockito.verify(repository, Mockito.times(1)).save(associationOne);
        assertNotNull(data);
        assertEquals(assocId, data.getAssocId()); 
        assertEquals(associationOne.getVid(), data.getVid());         
    }      
    
    @Test
    public void mockUpdate() throws InterruptedException
    { 
        associationOne.setUpdatedAt(new Date());
        Thread.sleep(1000); // to simulate different updates
        String assocId = "9eb3de3b8c40b2264db21a35821a24d7"; //associationOne.getAssocId()        
        Mockito.doReturn(associationOne).when(repository).findOne(associationOne.getId());
        Mockito.doReturn(associationTwo).when(repository).save(associationOne);        
        associationTwo.setStatus(AssociationStatus.INACTIVE);    
        associationTwo.setDeleted(true);
        Association data = service.update(associationOne.getId(), associationTwo);
        Mockito.verify(repository, Mockito.times(1)).findOne(associationOne.getId());
        Mockito.verify(repository, Mockito.times(1)).save(associationOne);
        assertNotNull(data);
        assertEquals(assocId, data.getAssocId()); 
        assertEquals(associationTwo.getVid(), data.getVid());     
        assertEquals(AssociationStatus.INACTIVE, data.getStatus());     
        assertTrue(data.isDeleted() == true);        
    }     
    
    @Test(expected = ResourceNotFoundException.class)
    public void mockUpdateThrowsResourceNotFoundException() throws InterruptedException
    { 
        
        String assocId = "notfound"; //associationOne.getAssocId()        
        //Mockito.doReturn(associationOne).when(repository).findOne(associationOne.getId());        
        Association data = service.update(assocId, associationTwo);
        Mockito.verify(repository, Mockito.times(1)).findOne(associationOne.getId());        
        assertNull(data);        
    }     
    
    @Test
    public void mockDelete() throws InterruptedException
    {                         
        Mockito.doNothing().when(repository).delete(associationThree);                
        service.delete(associationThree);        
        Mockito.verify(repository, Mockito.times(1)).delete(associationThree);        
    }     
    
    @Test
    public void mockInsert() throws InterruptedException
    {                         
        Mockito.doReturn(associationThree).when(repository).save(associationThree);                
        service.insert(associationThree);        
        Mockito.verify(repository, Mockito.times(1)).save(associationThree);              
    }      
    
    @Test
    public void mockFindAll()
    { 
        Mockito.doReturn(list).when(repository).findAll();
        List<Association> data = service.findAll();
        Mockito.verify(repository, Mockito.times(1)).findAll();
        assertNotNull(data);
        assertEquals(5, data.size());        
    }    
    
    @Test
    public void mockFindByStatus() throws InterruptedException
    {              
        //active
        Mockito.doReturn(list).when(repository).findByStatus(AssociationStatus.ACTIVE);                
        List<Association> data = service.find(AssociationStatus.ACTIVE);        
        Mockito.verify(repository, Mockito.times(1)).findByStatus(AssociationStatus.ACTIVE); 
        assertNotNull(data);
        assertEquals(5, data.size());
        
        // inactive
        list = new ArrayList();
        list.add(associationInactiveDeleted);
        Mockito.doReturn(list).when(repository).findByStatus(AssociationStatus.INACTIVE);                
        data = service.find(AssociationStatus.INACTIVE);        
        Mockito.verify(repository, Mockito.times(1)).findByStatus(AssociationStatus.INACTIVE); 
        assertNotNull(data);
        assertEquals(1, data.size());        
        assertTrue(data.get(0).isDeleted() == true);
        assertTrue(data.get(0).getStatus() == AssociationStatus.INACTIVE);
    }     
    
    @Test
    public void mockFindByVid() throws InterruptedException
    {              
        //active
        String vid = associationOne6e9bfc.getVid();
        list.clear();
        list.add(associationOne6e9bfc);
        list.add(associationTwo6e9bfc);
        
        Mockito.doReturn(list).when(repository).findByVid(vid);                
        List<Association> data = service.find(vid);        
        Mockito.verify(repository, Mockito.times(1)).findByVid(vid);  
        assertNotNull(data);
        assertEquals(2, data.size());                
        assertTrue(data.get(0).getVid().equals(vid));
        assertTrue(data.get(1).getVid().equals(vid));
    }     
    
    @Test
    public void mockFindByVidIncludeDeletedAndInactive() throws InterruptedException
    {              
        //active
        String vid = associationOne6e9bfc.getVid();
        list.clear();
        list.add(associationInactiveDeleted);                
        Mockito.doReturn(list).when(repository).findByVidIncludeDeletedAndInactive(vid);                
        List<Association> data = service.findByVidIncludeDeletedAndInactive(vid);        
        Mockito.verify(repository, Mockito.times(1)).findByVidIncludeDeletedAndInactive(vid);  
        assertNotNull(data);
        assertEquals(1, data.size());                
        assertTrue(data.get(0).getVid().equals(vid));
        assertTrue(data.get(0).getStatus().equals(AssociationStatus.INACTIVE));
        assertTrue(data.get(0).isDeleted());        
    }      
    
    @Test
    public void mockfindByVidAndStatus() throws InterruptedException
    {              
        //active
        String vid = associationOne6e9bfc.getVid();
        list.clear();
        list.add(associationOne6e9bfc);                
        list.add(associationTwo6e9bfc);                
        Mockito.doReturn(list).when(repository).findByVidAndStatus(vid, AssociationStatus.ACTIVE);                
        List<Association> data = service.find(vid, AssociationStatus.ACTIVE);        
        Mockito.verify(repository, Mockito.times(1)).findByVidAndStatus(vid, AssociationStatus.ACTIVE);  
        assertNotNull(data);
        assertEquals(2, data.size());                
        assertTrue(data.get(0).getVid().equals(vid));
        assertTrue(data.get(0).getStatus().equals(AssociationStatus.ACTIVE));
        assertTrue(data.get(0).isDeleted() == false);       
        assertTrue(data.get(1).getVid().equals(vid));
        assertTrue(data.get(1).getStatus().equals(AssociationStatus.ACTIVE));
        assertTrue(data.get(1).isDeleted() == false);             
    }     
    
    @Test
    public void mockfindByVidAndAssocId() throws InterruptedException
    {              
        //active
        String vid = associationOne6e9bfc.getVid();
        String assocId = associationOne6e9bfc.getAssocId();                   
        
        // create stubs
        Verifis verifis = new Verifis();
        verifis.setVid(vid);
        Mockito.doReturn(verifis).when(verifisService).find(vid); 
        Mockito.doReturn(associationOne6e9bfc).when(repository).findByAssocId(assocId);                
        Mockito.doReturn(associationOne6e9bfc).when(repository).findByVidAndAssocId(vid, assocId);                
        
        Association data = service.find(vid, assocId);        
        Mockito.verify(repository, Mockito.times(1)).findByAssocId(assocId);  
        Mockito.verify(repository, Mockito.times(1)).findByVidAndAssocId(vid, assocId); 
        assertNotNull(data);        
        assertTrue(data.getVid().equals(vid));
        assertTrue(data.getStatus().equals(AssociationStatus.ACTIVE));
        assertTrue(data.isDeleted() == false);       
        assertTrue(data.getAssocId().equals(assocId));        
    }    
    
    @Test(expected = VerifisIdNotFoundException.class)
    public void mockfindByVidAndAssocIdThrowsVerifisIdNotFoundException() throws InterruptedException
    {              
        //active
        String vid = associationOne6e9bfc.getVid();
        String assocId = associationOne6e9bfc.getAssocId();                   
        
        // create stubs
        Verifis verifis = new Verifis();
        verifis.setVid(vid);
        //Mockito.doReturn(verifis).when(verifisService).find(vid);                      
        Association data = service.find("notfound", assocId);        
        Mockito.verify(verifisService, Mockito.times(1)).find("notfound");           
        assertNull(data);                
    }        
    
    @Test(expected = AssociationVerifisMismatchException.class)
    public void mockfindByVidAndAssocIdThrowsAssociationVerifisMismatchExceptionUnknownAssocId() throws InterruptedException
    {                      
        String vid = associationOne6e9bfc.getVid();
        String assocId = associationOne6e9bfc.getAssocId();

        // create stubs
        Verifis verifis = new Verifis();
        verifis.setVid(vid);
        Mockito.doReturn(verifis).when(verifisService).find(vid);
        //Mockito.doReturn(associationOne6e9bfc).when(repository).findByAssocId(assocId);
        Association data = service.find(vid, "mismatch");
        Mockito.verify(verifisService, Mockito.times(1)).find(vid);
        Mockito.verify(repository, Mockito.times(1)).findByAssocId("mismatch");
        assertNull(data);              
    }     
    
    @Test(expected = AssociationVerifisMismatchException.class)
    public void mockfindByVidAndAssocIdThrowsAssociationVerifisMismatchExceptionUnknownVid() throws InterruptedException
    {                      
        String vid = associationOne6e9bfc.getVid();
        String assocId = associationOne6e9bfc.getAssocId();

        // create stubs
        Verifis verifis = new Verifis();        
        Mockito.doReturn(verifis).when(verifisService).find(verifis.getVid());
        Mockito.doReturn(associationOne6e9bfc).when(repository).findByAssocId(assocId);
        Mockito.doReturn(null).when(repository).findByVidAndAssocId(verifis.getVid(), assocId);  
        Association data = service.find(verifis.getVid(), assocId);
        Mockito.verify(verifisService, Mockito.times(1)).find(vid);
        Mockito.verify(repository, Mockito.times(1)).findByAssocId(assocId);  
        Mockito.verify(repository, Mockito.times(1)).findByVidAndAssocId(verifis.getVid(), assocId);         
        assertNull(data);              
    }     
    
    @Test
    public void mockDeleteAssocationSetFlags() throws InterruptedException
    {                      
        String vid = associationTwo6e9bfc.getVid();
        String assocId = associationTwo6e9bfc.getAssocId();                
        Verifis verifis = new Verifis();        
        verifis.setVid(vid);       
        Mockito.doReturn(associationTwo6e9bfc).when(repository).findByVidAndAssocId(verifis.getVid(), assocId);       
        Mockito.doReturn(associationTwo6e9bfc).when(repository).save(associationTwo6e9bfc); 
        service.delete(vid, assocId);        
        Mockito.verify(repository, Mockito.times(1)).findByVidAndAssocId(verifis.getVid(), assocId);           
        Mockito.verify(repository, Mockito.times(1)).save(associationTwo6e9bfc);                            
    }      
    
    @Test(expected = AssociationVerifisMismatchException.class)
    public void mockDeleteAssocationSetFlagsThrowsAssociationVerifisMismatchException() throws InterruptedException
    {                      
        String vid = associationTwo6e9bfc.getVid();
        String assocId = associationTwo6e9bfc.getAssocId();                
        Verifis verifis = new Verifis();        
        verifis.setVid(vid);       
        //Mockito.doReturn(associationTwo6e9bfc).when(repository).findByVidAndAssocId(verifis.getVid(), assocId);               
        service.delete("mismatch", assocId);        
        Mockito.verify(repository, Mockito.times(1)).findByVidAndAssocId(verifis.getVid(), assocId);           
        Mockito.verify(repository, Mockito.times(0)).save(associationTwo6e9bfc);                            
    }     
    
    @Test
    public void mockFindAssociationsByVidNoPaging()
    {
        list.clear();
        list.add(associationOne);
        list.add(associationTwo);
        list.add(associationThree);        
        Associations associations = new Associations();
        associations.setAssociations(list);
        associations.setTotalAssocs(list.size());
        AssociationsRequestParameter parameters = new AssociationsRequestParameter();
        parameters.setCode("123456");
        String vid = associationOne.getVid();
        Verifis verifis = new Verifis();        
        verifis.setVid(vid);         
        Mockito.when(verifisService.find(vid)).thenReturn(verifis);
        QAssociation qAssociation = new QAssociation("association");
        Predicate predicate = qAssociation.deleted.eq(Boolean.FALSE).and(qAssociation.status.eq(AssociationStatus.ACTIVE)).and(qAssociation.vid.eq(vid));
        Sort sort = new Sort(Sort.Direction.DESC, "createdAt");        
        Mockito.when(repository.count(predicate)).thenReturn(new Long(String.valueOf(list.size())));
        Mockito.when(repository.findAll(predicate, sort)).thenReturn(list);
        Associations data = service.find(vid, parameters);
        Mockito.verify(verifisService, Mockito.times(1)).find(vid);           
        Mockito.verify(repository, Mockito.times(1)).count(predicate);       
        Mockito.verify(repository, Mockito.times(1)).findAll(predicate, sort);
        assertNotNull(data);
        assertNotNull(data.getAssociations());
        assertEquals(3, data.getAssociations().size());
        assertEquals(-1, data.getLimit().intValue());
        assertEquals(-1, data.getStart().intValue());
        assertEquals(3, data.getTotalAssocs().intValue());               
    }
    
    //@Test
    public void mockFindAssociationsByVidWithPaging()
    {
        list.clear();
        list.add(associationPaypalOne);
        list.add(associationPaypalTwo);
        Associations associations = new Associations();
        associations.setAssociations(list);
        associations.setTotalAssocs(list.size());
        
        // query params
        AssociationsRequestParameter parameters = new AssociationsRequestParameter();
        String filter = "Paypal";
        parameters.setCode("123456");        
        parameters.setFilter(filter);
        parameters.setLimit(2);
        parameters.setStart(0);
        
        String vid = associationPaypalOne.getVid();
        Verifis verifis = new Verifis();        
        verifis.setVid(vid);         
        Mockito.when(verifisService.find(vid)).thenReturn(verifis);
        
        // query pageable parameters stubs
        QAssociation qAssociation = new QAssociation("association");
        Predicate predicate = qAssociation.deleted.eq(Boolean.FALSE).and(qAssociation.status.eq(AssociationStatus.ACTIVE)).and(qAssociation.vid.eq(vid));
        Sort sort = new Sort(Sort.Direction.DESC, "createdAt");   
        OffsetLimitPageRequest pageable = new OffsetLimitPageRequest(parameters.getLimit(), parameters.getStart(), sort, 0, parameters.getLimit());         
        Page page = new PageImpl(list, pageable, 2);                
        Mockito.when(repository.findAll(predicate, pageable)).thenReturn(page);        
        
        Mockito.when(repository.findAll(predicate, pageable).getContent()).then(new Answer<List>() {
            @Override
            public List answer(InvocationOnMock invocation) throws Throwable {
                return list;
            }
        });
        Mockito.when(repository.count(predicate)).thenReturn(new Long(String.valueOf(list.size())));        
        Associations data = service.find(vid, parameters);
        Mockito.verify(verifisService, Mockito.times(1)).find(vid);           
        Mockito.verify(repository, Mockito.times(1)).count(predicate);       
        Mockito.verify(repository, Mockito.times(1)).findAll(predicate, pageable).getContent();
        assertNotNull(data);
        assertNotNull(data.getAssociations());
        assertEquals(list.size(), data.getAssociations().size());
        assertEquals(parameters.getLimit(), data.getLimit());
        assertEquals(parameters.getStart(), data.getStart());
        assertEquals(list.size(), data.getTotalAssocs().intValue());               
        
        // set different paging params...
        
    }    
    
    @Test
    public void mockCountWithPredicate()
    {
        list.clear();
        list.add(associationPaypalOne);
        list.add(associationPaypalTwo);
        Associations associations = new Associations();
        associations.setAssociations(list);
        associations.setTotalAssocs(list.size());
        
        // query params
        AssociationsRequestParameter parameters = new AssociationsRequestParameter();
        parameters.setCode("123456");        
        associations.setAssociations(list);
        associations.setTotalAssocs(list.size());        
        
        String vid = associationOne.getVid();
        Verifis verifis = new Verifis();        
        verifis.setVid(vid);         
        //Mockito.when(verifisService.find(vid)).thenReturn(verifis);
        QAssociation qAssociation = new QAssociation("association");
        Predicate predicate = qAssociation.deleted.eq(Boolean.FALSE).and(qAssociation.status.eq(AssociationStatus.ACTIVE)).and(qAssociation.vid.eq(vid));        
        Mockito.when(repository.count(predicate)).thenReturn(new Long(String.valueOf(list.size())));        
        long data = service.count(predicate);        
        Mockito.verify(repository, Mockito.times(1)).count(predicate);               
        assertNotNull(data);        
        assertEquals(2, data);               
    }    
    
    @Test
    public void mockExistsAssociation() throws InterruptedException
    {                             
        String assocId = associationOne6e9bfc.getAssocId();                                        
        Mockito.doReturn(associationOne6e9bfc).when(repository).findByAssocId(assocId);                            
        boolean data = service.existsAssociation(assocId);        
        Mockito.verify(repository, Mockito.times(1)).findByAssocId(assocId);          
        assertNotNull(data);        
        assertTrue(data);        
    }     
    
}
