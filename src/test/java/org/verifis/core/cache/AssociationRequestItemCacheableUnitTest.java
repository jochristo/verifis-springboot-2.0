package org.verifis.core.cache;

import java.util.UUID;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.verifis.api.Constants;
import org.verifis.api.model.serializable.AssociationRequestItem;
import org.verifis.core.cache.impl.AssociationRequestItemCacheable;
import org.verifis.core.utility.Utilities;
import org.verifis.data.redis.service.IRedisService;

/**
 *
 * @author ic
 */
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class AssociationRequestItemCacheableUnitTest
{
    private static final Logger logger = LoggerFactory.getLogger(AssociationRequestItemCacheableUnitTest.class);   
    @InjectMocks private AssociationRequestItemCacheable cache;    
    @Mock private IRedisService redis;    
    private final AssociationRequestItem itemOne;
    private final AssociationRequestItem itemTwo;

    public AssociationRequestItemCacheableUnitTest()
    {
        itemOne = new AssociationRequestItem();
        itemTwo = new AssociationRequestItem();
        
        initializeItems();
    }
    
    private void initializeItems()
    {
        itemOne.setVid(UUID.randomUUID().toString());
        itemOne.setExpires(Utilities.currentEpochPlus(60));
        
        itemTwo.setVid(UUID.randomUUID().toString());
        itemTwo.setExpires(Utilities.currentEpochPlus(60));
    }
    
    @Before
    public void before()
    {                   
        assertNotNull(itemOne);
        assertNotNull(itemTwo);

    }
    
    @After
    public void after()
    {
        
    }    
    
    @Test
    public void set()
    {
        logger.info("Testing AbstractRedisCacheable<AssociationRequestItem> set...");
        String key = UUID.randomUUID().toString();
        String keyprefix = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID;
        String _key = keyprefix + key;
        
        AssociationRequestItem nullable = cache.set("unknownkey", itemOne);
        assertNull(nullable);        
        
        // stub method to set and return redis item
        Mockito.when(redis.added(_key, itemOne, Constants.Cache.ASSOCIATION_REQUEST_EXPIRY_SECONDS)).thenReturn(itemOne);        
        AssociationRequestItem item = cache.set(key, itemOne);
        assertNotNull(item);
        assertEquals(itemOne.getVid(), item.getVid());
    }
    
    @Test
    public void get()
    {
        logger.info("Testing AbstractRedisCacheable<AssociationRequestItem> get...");
        String key = UUID.randomUUID().toString();
        String keyprefix = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID;
        String _key = keyprefix + key;
        
        AssociationRequestItem nullable = cache.get("unknownkey");
        assertNull(nullable);          
        
        // stub method to set and return redis item
        Mockito.when(redis.get(_key)).thenReturn(itemOne);        
        AssociationRequestItem item = cache.get(key);
        assertNotNull(item);
        assertEquals(itemOne.getVid(), item.getVid());
    }    
    
    @Test
    public void isExpired()
    {
        logger.info("Testing AbstractRedisCacheable<AssociationRequestItem> isExpired...");
        String key = UUID.randomUUID().toString();
        String keyprefix = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID;
        String _key = keyprefix + key;             
                       
        // stub method to set and return redis item
        Mockito.when(redis.hasKey(_key)).thenReturn(true);        
        boolean result = cache.isExpired(key);        
        assertEquals(false, result);
    } 

    @Test
    public void setExpired()
    {
        logger.info("Testing AbstractRedisCacheable<AssociationRequestItem> setExpired...");
        String key = UUID.randomUUID().toString();
        String keyprefix = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID;
        String _key = keyprefix + key;
                
        // stub method to set and return redis item
        //Mockito.when(redis.hasKey(_key)).thenReturn(true);       
        boolean result = cache.setExpired(key);        
        assertEquals(true, result);
    } 
    
    @Test
    public void setWithKeyPrefix()
    {
        logger.info("Testing AbstractRedisCacheable<AssociationRequestItem> setWithKeyPrefix...");
        String key = UUID.randomUUID().toString();
        String keyprefix = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE;
        String _key = keyprefix + key;
        
        Object nullable = cache.set(keyprefix, "unknownkey", itemOne);
        assertNull(nullable);        
        
        // stub method to set and return redis item
        Mockito.when(redis.added(_key, key, Constants.Cache.REQUEST_ID_EXIST_EXPIRY_SECONDS)).thenReturn(key);        
        Object item = cache.set(keyprefix, key, key);
        assertNotNull(item);
        assertEquals(key, item);
    }
    
    @Test
    public void getWithKeyPrefix()
    {
        logger.info("Testing AbstractRedisCacheable<AssociationRequestItem> getWithKeyPrefix...");
        String key = UUID.randomUUID().toString();
        String keyprefix = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE;
        String _key = keyprefix + key;
        
        Object nullable = cache.get(keyprefix, "unknownkey");
        assertNull(nullable);           
        
        // stub method to set and return redis item
        Mockito.when(redis.get(_key)).thenReturn(key);        
        Object item = cache.get(keyprefix, key);
        assertNotNull(item);
        assertEquals(key, item);
    }    
    
    @Test
    public void isExpiredWithKeyPrefix()
    {
        logger.info("Testing AbstractRedisCacheable<AssociationRequestItem> isExpiredWithKeyPrefix...");
        String key = UUID.randomUUID().toString();
        String keyprefix = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE;
        String _key = keyprefix + key;             
                       
        // stub method to set and return redis item
        Mockito.when(redis.hasKey(_key)).thenReturn(true);        
        boolean result = cache.isExpired(keyprefix, key);        
        assertEquals(false, result);
    } 

    @Test
    public void setExpiredWithKeyPrefix()
    {
        logger.info("Testing AbstractRedisCacheable<AssociationRequestItem> setExpiredWithKeyPrefix...");
        String key = UUID.randomUUID().toString();
        String keyprefix = Constants.Cache.KeyPrefix.ASSOCIATION_REQUEST_ID_EXISTENCE;
        String _key = keyprefix + key;
                
        // stub method to set and return redis item
        Mockito.when(redis.hasKey(_key)).thenReturn(false);       
        boolean result = cache.setExpired(keyprefix, key);        
        assertEquals(true, result);
    }     
    
}
