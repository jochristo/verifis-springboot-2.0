package org.verifis.data.redis.service;

import com.google.common.io.Resources;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.UUID;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.verifis.api.domain.Association;
import org.verifis.api.domain.Verifis;

/**
 *
 * @author ic
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:app-integration-test.properties")
public class IRedisServiceUnitTest
{
    private static final Logger logger = LoggerFactory.getLogger(IRedisServiceUnitTest.class); 
    @Autowired private IRedisService redis;   
    private final String _key = "association:id#" + "fbe24f"; // some random key
    private Object object = new Verifis();
    private final Association associationOne;
    private final Association associationTwo;    

    public IRedisServiceUnitTest() throws URISyntaxException, IOException
    {
        // read json association resources into Association objects
        Gson gson = new Gson();
        URL url = Resources.getResource("associationOne.json");
        File file = new File(url.toURI());
        String contents = FileUtils.readFileToString(file, "UTF-8");               
        associationOne = gson.fromJson(contents, Association.class);
        logger.info("Initializing new association id#" + associationOne.getAssocId()+ " with vid: " + associationOne.getVid());
        
        url = Resources.getResource("associationTwo.json");
        file = new File(url.toURI());
        contents = FileUtils.readFileToString(file, "UTF-8");               
        associationTwo = gson.fromJson(contents, Association.class);        
        logger.info("Initializing new association id#" + associationTwo.getAssocId()+ " with vid: " + associationTwo.getVid());    
    }    
        
    @Before
    public void before()
    {
        assertNotNull(redis);
        assertNotNull(associationOne);
        assertNotNull(associationTwo);
    }
    
    @After
    public void after()
    {        
        
    }

    @Test
    public void add()
    {
        logger.info("Testing redis add...");
        redis.add(associationOne.getAssocId(), associationOne);        
        assertNotNull(redis.get(associationOne.getAssocId()));
        Association ass1 = (Association) redis.get(associationOne.getAssocId());
        assertEquals(associationOne.getAssocId(), ass1.getAssocId());
        redis.delete(associationOne.getAssocId());
        assertTrue(redis.get(associationOne.getAssocId()) == null);
    }
    
    @Test
    public void addWithExpiry() throws InterruptedException
    {
        logger.info("Testing redis addWithExpiry...");
        String key = UUID.randomUUID().toString();
        redis.add(key, associationOne, 1);        
        assertNotNull(redis.get(key));
        Thread.sleep(2000);
        assertTrue(redis.get(key) == null);
    }    
    
    @Test
    public void added()
    {
        logger.info("Testing redis added...");
        String key = UUID.randomUUID().toString();
        Object added = redis.added(key, associationOne);        
        assertNotNull(added);            
        redis.delete(key);
        assertTrue(redis.get(key) == null);
    }    
    
    @Test
    public void addedWithExpiry() throws InterruptedException
    {
        logger.info("Testing redis addedWithExpiry...");
        String key = UUID.randomUUID().toString();
        Object added = redis.added(key, associationOne, 1);        
        assertNotNull(added);                
        Thread.sleep(2000);
        assertTrue(redis.get(key) == null);
    }    
    
    @Test
    public void get()
    {
        logger.info("Testing redis get...");
        assertNull(redis.get("notfound"));            
        String key = UUID.randomUUID().toString();
        redis.add(key, associationOne);        
        assertNotNull(redis.get(key));            
        redis.delete(key);
        assertTrue(redis.get(key) == null);
    }     
    
    @Test
    public void hasKey()
    {
        logger.info("Testing redis hasKey...");
        assertEquals(false,redis.hasKey("notfound"));            
        String key = UUID.randomUUID().toString();
        redis.add(key, associationOne);        
        assertEquals(true,redis.hasKey(key));           
        redis.delete(key);
        assertEquals(false,redis.hasKey(key)); 
    }     
    
    @Test
    public void getExpire() throws InterruptedException
    {
        logger.info("Testing redis getExpire...");
        String key = UUID.randomUUID().toString();
        Object added = redis.added(key, associationOne, 1);        
        assertNotNull(added);               
        assertEquals(1, redis.getExpire(key));
        Thread.sleep(2000);
        assertEquals(false,redis.hasKey(key));
        assertTrue(redis.get(key) == null);
    }     
    
}
